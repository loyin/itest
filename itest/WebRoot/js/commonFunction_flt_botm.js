    // ids：定义Grid.attachHeader方法加入的DIV id多个ID用逗号隔开
	// indexs：定义要支持过滤的列（列号从0开始）的列号，多个列之间用逗号隔开
	//childNames 预先在页面上定义的 flt_box div id 多个之间用逗号隔开
    function attachHead(ids,indexs,childNames,GridObj){
    	var indexArr = indexs.split(",");
   		var idArr = ids.split(",");
   		var fltHeadStr = "";
   		var idsIndex = 0;
   		chdNameArr = childNames.split(",");
   		var colCount = GridObj.getColumnsNum();
   		for(var i=0; i<colCount; i++){
   			if(indexs.indexOf(i)>=0){
   				fltHeadStr = fltHeadStr +" ,<div id='"+idArr[idsIndex]+"' style='width:95%;font-size:11px;font-family:Tahoma;'></div>";
   				idsIndex++;
   			}else{
   				fltHeadStr = fltHeadStr +",&nbsp;";
   			}
   		}
   		fltHeadStr = fltHeadStr.substr(1);
    	GridObj.attachHeader(fltHeadStr);
    	for(var l=0;l<idArr.length; l++){
    		document.getElementById(idArr[l]).appendChild(document.getElementById(chdNameArr[l]).childNodes[0]);
    	}
		GridObj.hdr.rows[2].style.display = "none";
		
    }

	//页面过滤 ids为在pmGrid.attachHeader方法加入的DIV id多个ID用逗号隔开
	//indexs为用逗号隔开的列号
   function filterBy(ids,indexs,GridObj){
   		var indexArr = indexs.split(",");
   		var idArr = ids.split(",");
   		var allItems = GridObj.getAllItemIds();
   		var items = allItems.split(',');
   		var fltVals = new Object() ;//存放在各个过输入框的值
   		for(var l=0 ; l<idArr.length; l++){
   			fltVals[l] = document.getElementById(idArr[l]).childNodes[0].value.toLowerCase();
   		}
		for(var i=0; i< items.length; i++){
			var isDis = false;
			//每一行记录必须所有支持过滤的列，都满足过滤条件，才显示
			for(var col=0; col<indexArr.length; col++){
				var tStr = GridObj.cells(items[i],indexArr[col]).getValue().toString().toLowerCase();
				if(fltVals[col]=="" || tStr.indexOf(fltVals[col])!=-1){
					isDis = true;
				}else{
					isDis = false;
					break;
				}				
			}
			if(isDis){
				GridObj.getRowById(items[i]).style.display = ""
			}else{
				GridObj.getRowById(items[i]).style.display = "none"
			}
		}
		GridObj._fixAlterCss();
		GridObj.setSizes();
	}
