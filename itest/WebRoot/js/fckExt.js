
//把fck editor 的内容还原成纯文本
function html2PlainText(iniText){
	var oEditor = FCKeditorAPI.GetInstance('MyTextarea') ;
	 oEditor.EnterMode = 'br' ;
	var iniText = oEditor.GetXHTML(true);
	 var textArr = iniText.split(">");
	 var restText = "";
	 for(var i=0; i<textArr.length; i++){
	 	var tmpText = textArr[i];
 		if(tmpText.indexOf("</p")>=0||tmpText.indexOf("</tr")>=0||tmpText.indexOf("<br /")>=0 ){
 			restText = restText +"<br>";
 		}
	 	if(textArr[i].indexOf("<")==0){
	 		if(textArr[i].indexOf("<img")==0){
	 			restText += "(图略...)";
	 		}
	 		continue;
	 	}
	 	var pic="";
	 	if(tmpText.indexOf("<")>0){
	 		if(tmpText.indexOf("<img")>=0){
 				pic = "(图略...)";
 			}
	 		tmpText = tmpText.substring(0,tmpText.indexOf("<"));
	 		tmpText = tmpText.replace("&nbsp;","") ;
	 		tmpText = tmpText.replace("&gt;","") ;
	 		tmpText = tmpText.replace("&lt;","") ;
	 		tmpText = tmpText.replace(/\s+$|^\s+/g,"");
	 		restText =restText + tmpText.replace("&nbsp;","") +" " ;
	 		if(pic !=""){
	 			restText+=pic;
	 		}
	 	}else{
	 		tmpText = tmpText.replace(/\s+$|^\s+/g,"");
	 		restText+=tmpText;
	 	}
	 }
	 return restText;
}