	function isAllNull(str){
    	return isWhitespace(str);
    }
	String.prototype.trim = function(){
	     var _argument = arguments[0]==undefined ? " ":arguments[0];
	     if(typeof(_argument)=="string"){
	        return this.replace(_argument == " "?/(^s*)|(s*$)/g : new RegExp("(^"+_argument+"*)|("+_argument+"*$)","g"),"");
	     }else if(typeof(_argument)=="object"){
	        return this.replace(_argument,"")
	     }else if(typeof(_argument)=="number" && arguments.length>=1){
	        return arguments.length==1? this.substring(arguments[0]) : this.substring(arguments[0],this.length-arguments[1]);
	     }
	};
	 String.prototype.endWith=function(str){  
		 if(str==null||str==""||this.length==0||str.length>this.length){  
		   return false;  
		 }if(this.substring(this.length-str.length)==str){ 
		   return true;  
		 }else{  
		   return false;
		 }
		 return true;  
	};
    function checkHasChi(str){
        re = /[\u4E00-\u9FA0]/;//汉字
        if (re.test(str)){
            return 1;
        } else{
            return 0;
        }
    }
    function checkIsOverLong(str,longtest){
        var len=0;
        for(l=0;l<str.length;l++){
            if(checkHasChi(str.charAt(l))==1){
                len+=2;
            } else{
                len+=1;
            }
            if(len>parseInt(longtest)){
                return true;
            }
        }
        return false;
    }
 	function adjustTable(tableId,startClass){
		var ctable=$(tableId);
		var count = 1;
		for(var i=0;i<ctable.rows.length;i++){
		   if(ctable.rows[i].style.display==""){
			   if((count%2)==0){
				   	if(typeof(startClass)!="undefined"){
			   	   		ctable.rows[i].className="ev_mypm";
			   	   	}else{
				   		ctable.rows[i].className="odd_mypm";
				   	}
			   }else{
				   	if(typeof(startClass)!="undefined"){
			   	   		ctable.rows[i].className="odd_mypm";
			   	   	}else{
				   		ctable.rows[i].className="ev_mypm";
				   	}
			   }
			   count++;
			  }
		}
	}
	var griJs ="/dhtmlx/grid/codebase/dhtmlxgrid.js";
	var griCelJs ="/dhtmlx/grid/codebase/dhtmlxgridcell.js";
	var winJs = "/dhtmlx/windows/codebase/dhtmlxwindows.js";
	var fckJs ="/pmEditor/fckeditor.js";
	var winCss ="/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css";
	function importWinJs(){
		//loadCSS(winCss);
		return importJs(winJs);
	}
	function importGriJs(){
		importJs(griJs);
		return importJs(griCelJs);
	}
	function importFckJs(){
		return importJs(fckJs);
	}
	function postSub(url,formId){
		var ajaxResut = dhtmlxAjax.postSync(url,formId).xmlDoc.responseText;
		return 	ajaxResut;
	}
	function getBroType(){
		return _isIE;
	}
	function html2PlainText(edInstId){
		var oEditor = FCKeditorAPI.GetInstance(edInstId) ;
		oEditor.EnterMode = 'br' ;
		var iniText = oEditor.GetXHTML(true);
		 var textArr = iniText.split(">");
		 var restText = "";
		 for(var i=0; i<textArr.length; i++){
		 	var tmpText = textArr[i];
		 	if(textArr[i].indexOf("<")==0){
		 		if(textArr[i].indexOf("<img")==0){
		 			restText += "(图略...)";
		 		}
		 		continue;
		 	}
		 	var pic="";
		 	if(tmpText.indexOf("<")>0){
		 		if(tmpText.indexOf("<img")>=0){
	 				pic = "(图略...)";
	 			}
		 		tmpText = tmpText.substring(0,tmpText.indexOf("<"));
		 		tmpText = tmpText.replace("&nbsp;","") ;
		 		tmpText = tmpText.replace("&gt;","") ;
		 		tmpText = tmpText.replace("&lt;","") ;
		 		tmpText = tmpText.replace(/\s+$|^\s+/g,"");
		 		restText =restText + tmpText.replace("&nbsp;","") +" " ;
		 		if(pic !=""){
		 			restText+=pic;
		 		}
		 	}else{
		 		tmpText = tmpText.replace(/\s+$|^\s+/g,"");
		 		restText+=tmpText;
		 	}
		 }
		 return restText;
	}
	var selectAllFlag = false;
	var deleteFlag = 0;
	var pageBreakUrl = "";
	var pageBreakForm = "findForm";
	var pageNo = 1;
	var pageSize =20;
	var selectItems, cufmsW, cuW_ch, mW_ch, dW_ch , suW_ch, fW_ch,cW_ch,ptW_ch,secuW_ch;
	var pAdto = "dto";
	var pageSizec=1,actFlg = false;
	var popGrid ,pmGrid ,selGrid,seledGrid,pmBar,jsons,pageCount;
function ajaxForm(g) {
	if (g == "") {
		return "";
	}
	var b = document.getElementById(g);
	var a = b.elements;
	var e;
	var f;
	var c = "";
	for (f = 0; f < a.length; ++f) {
		var e = a[f];
		if (e.type == "text" || e.type == "textarea" || e.type == "hidden") {
			e.value = jsonProtect(e.value);
			c += encodeURIComponent(e.name) + "=" + encodeURIComponent(e.value) + "&";
		} else {
			if (e.type == "select-one" || e.type == "select-multiple") {
				var k = e.options, d, h;
				for (d = 0; d < k.length; ++d) {
					h = k[d];
					if (h.selected) {
						c += encodeURIComponent(e.name) + "=" + encodeURIComponent(h.value) + "&";
					}
				}
			} else {
				if (e.type == "checkbox" || e.type == "radio") {
					if (e.checked) {
						c += encodeURIComponent(e.name) + "=" + encodeURIComponent(e.value) + "&";
					}
				} else {
					if (e.type != "file") {
						if (e.type != "password") {
							e.value = jsonProtect(e.value);
						}
						c += encodeURIComponent(e.name) + "=" + encodeURIComponent(e.value) + "&";
					} else {
					}
				}
			}
		}
	}
	if (c.lastIndexOf("&") == c.length - 1) {
		c = c.substr(0, c.lastIndexOf("&"));
	}
	return c;
}

