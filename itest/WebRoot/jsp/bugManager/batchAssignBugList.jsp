
<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
		<TITLE>BUG管理</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page.css'>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>			
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>			
	</HEAD>
	<BODY style="overflow-x:hidden;">
		<ww:include value="/jsp/common/dialog.jsp"></ww:include>
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top" style="padding-right: 3px;">
					<div id="toolbarObj"></div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<div id="gridbox"></div>
				</td>
			</tr>
		</table>
		<input type="hidden" id="listStr" name="listStr" value="${dto.listStr}"/>
		<ww:hidden id="relCaseSwitch" name="dto.relCaseSwitch"></ww:hidden>
		<ww:hidden id="customSprHomeTaskId" name="dto.taskId" ></ww:hidden>
	<script type="text/javascript">
	var taskOpts = Array(Array('00000000000000000000000000000000', 'obj', '请选择任务'));
	var ontLineState = "${dto.outLineState}";
	ininPage("toolbarObj", "gridbox", 800);
    pmBar = new dhtmlXToolbarObject("toolbarObj");
	pmBar.setIconsPath(conextPath+"/dhtmlx/toolbar/images/");
	pmBar.addButton("allChk", 0, "", "ch_false.gif");
	pmBar.setItemToolTip("allChk", "全选");
	pmBar.addButton("assignPer",1 , "", "noReport.gif");
	pmBar.setItemToolTip("assignPer", "分配问题");
	pmBar.addButton("loadBugBoard",2 , "", "chart.gif");
	pmBar.setItemToolTip("loadBugBoard", "项目成员BUG面板");
	pmBar.addButton("find",3 , "", "search.gif");
	pmBar.setItemToolTip("find", "查询");
	pmBar.addButton("reFreshP",4 , "", "page_refresh.gif");
	pmBar.setItemToolTip("reFreshP", "刷新页面");
	pmBar.addButton("back",5 , "", "back.gif");
	pmBar.setItemToolTip("back", "返回");
	pmBar.addButton("first",6 , "", "first.gif", "first.gif");
	pmBar.setItemToolTip("first", "第一页");
	pmBar.addButton("pervious",7, "", "pervious.gif", "pervious.gif");
	pmBar.setItemToolTip("pervious", "上一页");
	pmBar.addSlider("slider",8, 80, 1, 30, 1, "", "", "%v");
	pmBar.setItemToolTip("slider", "滚动条翻页");
	pmBar.addButton("next",9, "", "next.gif", "next.gif");
	pmBar.setItemToolTip("next", "下一页");
	pmBar.addButton("last",10, "", "last.gif", "last.gif");
	pmBar.setItemToolTip("last", "末页");
	pmBar.addInput("page",11, "", 25);
	pmBar.addText("pageMessage",12, "");
	pmBar.addText("pageSizeText",13, "每页");
	var opts = Array(Array('id1', 'obj', '10'), Array('id2', 'obj', '15'), Array('id3', 'obj', '20'), Array('id4', 'obj', '25'));
	pmBar.addButtonSelect("pageP",14, "page", opts);
	pmBar.setItemToolTip("pageP", "每页记录数");
	pmBar.addText("pageSizeTextEnd",15, "条");
	pmBar.setListOptionSelected('pageP','id1');
	pmBar.addSeparator("sep_swTask3",16); 
	pmBar.addButton("sw2Task",17, "", "");
	var taskOpts = Array(Array('allTask', 'obj', '所有项目BUG列表'), Array('sw2TaskAction', 'obj', '其他项目BUG列表'), Array('sw2AssignAction', 'obj', '其他项目批量分配列表'));
	pmBar.addButtonSelect('selTaskPage',18,'所有', taskOpts);
	pmBar.addSeparator("sep_swTask2",19); 
	pmBar.addText("qSearch",20, "编号快速查询");
	pmBar.addInput("qSearchIpt",21, "", 80);
	pmBar.addSeparator("sep_swTask2",22);
	pmBar.addButton("currPosition",23, "当前位置: 批量分配列表", "hand.gif", "hand.gif"); 
	pmBar.attachEvent("onClick", function(id) {
		if(id =="reFreshP"){
	   		pageAction(pageNo, pageSize);
		}
	});
	var bugBoard_wh;
	pmBar.attachEvent("onClick", function(id) {
		if(id =="back"){
	   		location=conextPath+ "/bugManager/bugManagerAction!loadAllMyBug.action?dto.allTestTask=true";
		}else if(id=="loadBugBoard"){
			var url = conextPath+ "/bugManager/bugManagerAction!loadBugBoard.action?dto.loadType=0";
			bugBoard_wh = initW_ch(bugBoard_wh, "", true, 400, 380,'bugBoard_wh');
			bugBoard_wh.attachURL(url);
			bugBoard_wh.setText("项目成员BUG面板---点击待处理BUG数目查看相关待处理BUG");	
		    bugBoard_wh.show();
		    bugBoard_wh.bringToTop();
		    bugBoard_wh.setModal(true);
		}
	});


	pmBar.setItemText("selTaskPage", "切换列表");
	pmBar.attachEvent("onClick", function(id) {
		if(id=="custHome"){
			dhtmlxAjax.post(conextPath+"/commonAction!setBugAsHome.action");
			parent.setCheckboxUnchk('sprHome');
		}else if(id=="sw2TaskAction"){
			openSwTaskList();
		}else if(id=="allTask"){
			var url = conextPath+ "/bugManager/bugManagerAction!sw2AllMyBug.action?dto.allTestTask=true&dto.defBug=1";
			window.location=url;
		}else if(id=="sw2AssignAction"){
			openSwTaskList("assignBug");
		}
	});

	var flwStr ="${dto.testFlow}";
	var roleStr ="${dto.roleInTask}";
	var myId = "${session.currentUser.userInfo.id}";
	var swWin;
	function openSwTaskList(cusOpera){
		if(typeof(swWin)!="undefined" &&swWin.isHidden()){
	    	swWin.show();
	    	swWin.bringToTop();
	    	swWin.setModal(true);
	    	return;			
		}
		var operCmd = "myBugList";
		if(typeof(cusOpera)!="undefined")
			operCmd = cusOpera;
		swWin = initW_ch(swWin, "", true, 890, 500,'swWin');
		swWin.attachURL(conextPath+"/singleTestTask/singleTestTaskAction!swTestTaskList.action?dto.operCmd="+operCmd);
		swWin.setText("请选择测试项目");	
    	swWin.show();
    	swWin.bringToTop();
    	swWin.setModal(true);			
	}
	if($("customSprHomeTaskId").value==""){
		openSwTaskList();
	}
	var outLineCom = true;
	if(ontLineState!="1"&&$("customSprHomeTaskId").value!=""){
		openSwTaskList();
		hintMsg("所选|当前项目未提交测试需求,请切换到其他项目");
		outLineCom = false;	
	}	
	</script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/jsp/bugManager/batchBase.js"></script> 
	<script type="text/javascript"src="<%=request.getContextPath()%>/jsp/bugManager/batchGrid.js"></script>
		<div id="createDiv" class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:hidden id="typeSelStr" name="dto.bug.typeSelStr" ></ww:hidden>
			<ww:hidden id="gradeSelStr" name="dto.bug.gradeSelStr" ></ww:hidden>
			<ww:hidden id="freqSelStr" name="dto.bug.freqSelStr" ></ww:hidden>
			<ww:hidden id="occaSelStr" name="dto.bug.occaSelStr" ></ww:hidden>
			<ww:hidden id="geCaseSelStr" name="dto.bug.geCaseSelStr" ></ww:hidden>
			<ww:hidden id="sourceSelStr" name="dto.bug.sourceSelStr" ></ww:hidden>
			<ww:hidden id="plantFormSelStr" name="dto.bug.plantFormSelStr"></ww:hidden>
			<ww:hidden id="genePhaseSelStr" name="dto.bug.genePhaseSelStr"></ww:hidden>
			<ww:hidden id="priSelStr" name="dto.bug.priSelStr"></ww:hidden>
			<input type="hidden" id="verSelStr" name="verSelStr" value=""/>
			<input type="hidden" id="testSelStr" name="testSelStr" value=""/>
			<input type="hidden" id="devStr" name="devStr" value=""/>
			<input type="hidden" id="assignSelStr" name="assignSelStr" value=""/>
			<input type="hidden" id="analySelStr" name="analySelStr" value=""/>
			<input type="hidden" id="interCesSelStr" name="interCesSelStr"value=""/>
			<input type="hidden" id="devChkIdSelStr" name="devChkIdSelStr" value=""/>
			<input type="hidden" id="testChkIdSelStr" name="testChkIdSelStr" value=""/>
			<input type="hidden" id="testLdIdSelStr" name="testLdIdSelStr"value=""/>
			<input type="hidden" id="roleInTask" name="roleInTask" value=""/>
			<input type="hidden" id="testFlow" name="testFlow" value=""/>
			<input type="hidden" id="loadType" name="loadType" value=""/>
			<input type="hidden" id="querySelStr" name="querySelStr" value=""/>
			<input type="hidden" id="queryCount" name="queryCount" value=""/>
			<ww:hidden id="testPhase" name="dto.bug.testPhase"></ww:hidden>
			<input type="hidden" id="nodeDataStr" name="nodeDataStr" value=""/>
			<ww:form theme="simple" method="post" id="createForm" name="createForm" namespace="" action="">
				<ww:hidden id="bugId" name="dto.bug.bugId"></ww:hidden>
				<ww:hidden id="reProTxt" name="dto.bug.reProTxt"></ww:hidden>
				<ww:hidden id="currStateId" name="dto.bug.currStateId"></ww:hidden>
				<ww:hidden id="nextFlowCd" name="dto.bug.nextFlowCd"></ww:hidden>
				<ww:hidden id="currFlowCd" name="dto.bug.currFlowCd"></ww:hidden>
				<ww:hidden id="attachUrl" name="dto.bug.attachUrl"></ww:hidden>
				<table border="0" id="createTable" class="obj row20px" cellspacing="0" align="center" cellpadding="0" border="0" width="100%">
					<tr>
						<td colspan="6" class="tdtxt" align="center" width="720" style="border-right:0">
							<div id="cUMTxt" align="center"
								style="color: Blue"></div>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right" style="color:red;border-right:0">
							测试需求:
						</td>
						<td class="tdtxt" align="left" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="moduleId" name="dto.bug.moduleId"></ww:hidden>
							<ww:textfield id="moduleName" name="moduleName"
								cssStyle="width:640;padding:2 0 0 4;" cssClass="text_c"
								  onfocus="loadTree()"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td width="80"  class="rightM_center" align="right" style="border-right:0">
							状态:
						</td>
						<td class="tdtxt" align="left" colspan="3" width="400"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="stateName" name="stateName" readonly="true" cssClass="text_c"
								cssStyle="width:160;padding:2 0 0 4;"></ww:textfield>
						</td>
						<td width="80"  class="rightM_center" align="right" style="color:red;border-right:0">
							发现版本:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0" nowrap>
							<ww:hidden id="bugReptVer" name="dto.bug.bugReptVer"
								onkeypress="javascript:return;"></ww:hidden>
							<ww:textfield id="bugReptVerName" name="dto.bug.reptVersion.versionNum"
								cssStyle="width:160;padding:2 0 0 4;" cssClass="text_c"
								  readonly="true"
								onfocus="popSelWin('bugReptVer','bugReptVerName','verSelStr','发现版本')"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							Bug描述:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="bugDesc" name="dto.bug.bugDesc" cssClass="text_c"
								cssStyle="width:640;padding:2 0 0 4;"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							类型:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0" nowrap>
							<ww:hidden id="bugTypeId" name="dto.bug.bugTypeId"
								onkeypress="javascript:return;"></ww:hidden>
							<ww:textfield id="bugTypeName" name="dto.bug.dtoHelper.bugType.typeName"
								cssStyle="width:160;padding:2 0 0 4;" cssClass="text_c"
								  readonly="true"
								onfocus="popSelWin('bugTypeId','bugTypeName','typeSelStr','类型')"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							等级:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="bugGradeId" name="dto.bug.bugGradeId"></ww:hidden>
							<ww:textfield id="bugGradeName" name="dto.bug.dtoHelper.bugGrade.typeName"
								cssStyle="width:160;padding:2 0 0 4;"
								  readonly="true" cssClass="text_c"
								onfocus="popSelWin('bugGradeId','bugGradeName','gradeSelStr','等级')"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							平台:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="platformId" name="dto.bug.platformId"></ww:hidden>
							<ww:textfield id="pltfomName" name="dto.bug.dtoHelper.occurPlant.typeName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('platformId','pltfomName','plantFormSelStr','发生平台')"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							来源:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="sourceId" name="dto.bug.sourceId"></ww:hidden>
							<ww:textfield id="sourceName" name="dto.bug.dtoHelper.bugSource.typeName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('sourceId','sourceName','sourceSelStr','来源')"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							发现时机:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="bugOccaId" name="dto.bug.bugOccaId"></ww:hidden>
							<ww:textfield id="occaName" name="dto.bug.dtoHelper.bugOpotunity.typeName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('bugOccaId','occaName','occaSelStr','发现时机')"></ww:textfield>
						</td>
						<td width="80"  class="rightM_center" align="right" class="rightM_center" 
							style="color:red;border-right:0">
							<div id="geneCauseTd" style="display: none;">测试时机:</div>
						</td>
						<td class="tdtxt"  width="160"
							style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="geneCauseId" name="dto.bug.geneCauseId"></ww:hidden>
							<div id="geneCauseF" style="display: none" >
							<ww:textfield id="geneCaseName" name="dto.bug.dtoHelper.geneCause.typeName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('geneCauseId','geneCaseName','geCaseSelStr','测试时机')"></ww:textfield>
						    </div>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							优先级:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="priId" name="dto.bug.priId"></ww:hidden>
							<ww:textfield id="priName" name="dto.bug.dtoHelper.bugPri.typeName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('priId','priName','priSelStr','优先级')"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							频率:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="bugFreqId" name="dto.bug.bugFreqId"></ww:hidden>
							<ww:textfield id="bugFreqName" name="dto.bug.dtoHelper.bugFreq.typeName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('bugFreqId','bugFreqName','freqSelStr','频率')"></ww:textfield>
						</td>
						<td width="80" id="repropTd" class="rightM_center" align="right"
							 class="rightM_center" style="color:red;display: none;border-right:0">
							再现比例:
						</td>
						<td id="reprop" class="tdtxt" align="left"
							style="padding: 2 0 0 4; width: 160; display: none;border-right:0">
							<ww:textfield id="reproPersent" name="dto.bug.reproPersent" cssClass="text_c"
								cssStyle="width:160;padding:2 0 0 4;"></ww:textfield>
						</td>
					</tr>
					<tr style="display:none">
						<td id="impPhaTdtxt" width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;display: none;border-right:0">
							引入原因:
						</td>
						<td id="impPhaTd" class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4; display: none;border-right:0">
							<ww:hidden id="genePhaseId" name="dto.bug.genePhaseId"></ww:hidden>
							<ww:textfield id="genPhName" name="genPhName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('genePhaseId','genPhName','genePhaseSelStr','引入原因')"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0" nowrap>
							再现步骤:
						</td>
						<td class="tdtxt" colspan="5"
							style="background-color: #f7f7f7; hight: 200; width: 640;border-right:0">
							<textarea name="dto.bug.reProStep" id="reProStep" cols="50"
								rows="30" style="width:640;hight:200;padding:2 0 0 4;">
   			        		  <ww:property value="dto.bug.reProStep" escape="false" />
   			                </textarea>
						</td>
					</tr>
					<tr id="testOwnerTr" style="display: none;">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							转互验人:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="testOwnerId" name="dto.bug.testOwnerId"></ww:hidden>
							<ww:textfield id="testOwnName" name="testOwnName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('testOwnerId','testOwnName','testSelStr','互验人')"></ww:textfield>
						</td>
						<td class="tdtxt" align="left" width="480" colspan="4" style="display: none;border-right:0">
							<a class="graybtn" href="javascript:void(0);" onclick="getMdPerson()"><span>从测试需求开发人员中指派</span> </a>
						</td>
					</tr>
					<tr id="analOwnerTr" style="display: none;">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							转分析人 :
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="analyseOwnerId" name="dto.bug.analyseOwnerId"></ww:hidden>
							<ww:textfield id="analOwnerName" name="analOwnerName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('analyseOwnerId','analOwnerName','analySelStr','分析人')"></ww:textfield>
						</td>
						<td class="tdtxt" align="left" width="480" colspan="4" style="display: none;border-right:0">
							<a class="graybtn" href="javascript:void(0);" onclick="getMdPerson()"><span>从测试需求开发人员中指派</span> </a>
						</td>
					</tr>
					<tr id="anasnOwnerTr" style="display: none;">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							转分配人:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="assinOwnerId" name="dto.bug.assinOwnerId"></ww:hidden>
							<ww:textfield id="anasnOwnerName" name="anasnOwnerName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('assinOwnerId','anasnOwnerName','assignSelStr','分配人')"></ww:textfield>
						</td>
						<td class="tdtxt" align="left" width="480" colspan="4" style="display: none;border-right:0">
							<a class="graybtn" href="javascript:void(0);" onclick="getMdPerson()"><span>从测试需求开发人员中指派</span> </a>
						</td>
					</tr>
					<tr id="devOwnerTr" style="display: none;">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							转修改人 :
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="devOwnerId" name="dto.bug.devOwnerId"></ww:hidden>
							<input type="hidden" id="moduleDevStr" name="moduleDevStr"
								value="" />
							<ww:textfield id="devOwnerName" name="dto.bug.dtoHelper.devOwner.uniqueName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('devOwnerId','devOwnerName','devStr','修改人')"></ww:textfield>
						</td>
						<td class="tdtxt" align="left" id="assFromMdTd" width="480" colspan="4" style="display: none;border-right:0">
							<a class="graybtn" href="javascript:void(0);" onclick="getMdPerson()"><span>从测试需求开发人员中指派</span> </a>
						</td>
					</tr>
					<tr id="intecsOwnerTr" style="display: none;">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							转仲裁人 :
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="intercessOwnerId" name="dto.bug.intercessOwnerId"></ww:hidden>
							<ww:textfield id="intecsOwnerName" name="intecsOwnerName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('intercessOwnerId','intecsOwnerName','devStr','仲裁')"></ww:textfield>
						</td>
						<td class="tdtxt" align="left" width="480" colspan="4" style="display: none;border-right:0" >
							<a class="graybtn" href="javascript:void(0);" onclick="getMdPerson()"><span>从测试需求开发人员中指派</span> </a>
						</td>
					</tr>
					</ww:form>
				    <ww:form enctype="multipart/form-data" theme="simple" name="fileform" id="fileform" action="" method="post" target="target_upload">
					  <tr class="odd_mypm">
					  	 <td class="rightM_center" width="90" id="attachTd" style="border-right:0">
					  	 附件/插图片:
					  	 </td>
					    <td class="dataM_left"  colspan="5" width="630" style="border-right:0">
							<input name="currUpFile" id="currUpFile" type="file" style="padding:2 0 0 4;width:210" Class="text_c">
							<img src="<%=request.getContextPath()%>/images/button/attach.gif" style="display:none"id="currAttach" alt="当前附件" title="打开当前附件" onclick="openAtta()" />
							<img src="<%=request.getContextPath()%>/dhtmlx/toolbar/images/img_insert.png" id="insertImg" alt="当前位置插入图片" title="当前位置插入图片" onclick="upLoadAndSub('newSub','addSubCheck',1,oEditor);" />
					    </td>
					  </tr>	
					  <tr class="ev_mypm">
					    <td width="90" align="right"  style="border-right:0">&nbsp;</td>
						<td width="630" style="width:200;padding:2 0 0 4;border-right:0" colspan="5"><div id="upStatusBar"></div></td>		  
					  </tr>		
				  </ww:form>	
 		  	      <iframe id="target_upload" name="target_upload" src="" frameborder="0" scrolling="no" width="0" height="0"></iframe>
					<tr>
					<td class="tdtxt" align="center" width="720" colspan="6" style="border-right:0">
					  <a class="bluebtn" href="javascript:void(0);"onclick="eraseAttach('eraseAllImg');closeMain();"style="margin-left: 6px"><span> 返回</span> </a>
					  <a class="bluebtn" href="javascript:void(0);" id="saveBtnc" onclick="saveBtnId='saveBtnc';contin='1';upLoadAndSub('newSub','addSubCheck',0,oEditor);" style="margin-left: 6px"><span>确定并继续</span> </a>
					  <a class="bluebtn" href="javascript:void(0);" id="saveBtn" onclick="contin='';upLoadAndSub('newSub','addSubCheck',0,oEditor);"style="margin-left: 6px"><span>确定</span> </a>
					  <a class="bluebtn" href="javascript:void(0);" id="reSetAttaBtn" onclick="$('currUpFile').value='';if(_isIE)$('currUpFile').outerHTML=$('currUpFile').outerHTML" ><span>取消选取的文件</span> </a>
					<td>
					</tr>
					<tr>
						<td class="tdtxt" align="left" width="720" colspan="6" style="padding: 2 0 0 4;border-right:0">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td class="tdtxt" align="left" width="720" colspan="6" style="padding: 2 0 0 4;border-right:0">
							&nbsp;
						</td>
					</tr>
				</table>
		</div>
		</div>
		<div id="findDiv"class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="findForm" name="findForm"
				namespace="" action="">
				<ww:hidden id="currTaskIdF" name="dto.taskId" ></ww:hidden>
				<table border="0" id="findTable" class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td colspan="8" class="tdtxt" align="center" width="720" style="border-right:0">
							<div id="fMTxt" align="center" style="color: Blue;"></div>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td width="100" class="rightM_center" align="right" style="border-right:0">
							查询器:
						</td>
						<td width="360" class="tdtxt" align="left" colspan="4" style="border-right:0">
						   <ww:hidden id="currQueryId" name="currQueryId"></ww:hidden>
							<ww:hidden id="queryId" name="dto.queryId"></ww:hidden>
							<ww:textfield id="queryNameF" name="queryNameF"
								cssStyle="width:360;padding:2 0 0 4;"
								  readonly="true" cssClass="text_c"
								onfocus="popSelWin('currQueryId','queryNameF','querySelStr','我的查询器')"></ww:textfield>
						</td>
						<td width="270" class="rightM_center" align="left" style="border-right:0"colspan="3">
						 <div id="queryBtnTd" style="display: none">
							<a class="graybtn" href="javascript:void(0);" onclick="queryDtal()"><span>
									明细</span> </a>
							<a class="graybtn" style="margin-left: 3px"
								href="javascript:void(0);" onclick="findByquery()"><span> 查询</span> </a>
							<a class="graybtn" style="margin-left: 3px;display:none" id="upQuBtn"
								href="javascript:void(0);" onclick="saveQuery('up')"><span> 更新</span> </a>
							<a class="graybtn" style="margin-left: 3px"
								href="javascript:void(0);" onclick="clearQueryTd()"><span>清空</span>
							</a>
							<a class="graybtn" style="margin-left: 3px"
								href="javascript:void(0);" onclick="delQuery()"><span>删除</span>
							</a>
						 </div>
						</td>
					</tr>
					<tr>
						<td width="100" class="rightM_center" align="right" style="border-right:0">
							测试需求:
						</td>
						<td class="tdtxt" align="left" colspan="4" width="360" style="border-right:0">
							<ww:hidden id="moduleIdF" name="dto.bug.moduleId"></ww:hidden>
							<ww:textfield id="moduleNameF" name="moduleNameF"
								cssStyle="width:360;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								  onfocus="loadTree()"></ww:textfield>
						</td>
						<td width="80" class="tdtxt" align="left"  style="border-right:0">
						 <div id="mdBtn" style="display:none;">
							<a class="graybtn" href="javascript:void(0);"
								onclick="javascript:$('moduleNameF').value='';$('moduleIdF').value='';"><span>清空</span> </a>
						</div>
						</td>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							待处理人:
						</td>
						<td class="tdtxt" width="100" style="border-right:0">
							<ww:hidden id="nextOwnerIdF" name="dto.bug.nextOwnerId"></ww:hidden>
							<ww:textfield id="nextOwnerIdFName" name="nextOwnerIdFName" cssClass="text_c" onclick="importJs('/jsp/common/testTaskSelSinglePersion.js');popTeamMEMselWin('nextOwnerIdF','nextOwnerIdFName',true)"
								readonly="true" cssStyle="width:100;padding:2 0 0 4;"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td width="100" class="rightM_center" align="right" style="border-right:0">
							描述/再现步骤:
						</td>
						<td class="tdtxt" align="left" colspan="4" width="360" style="border-right:0">
							<ww:textfield id="bugDescF" name="dto.bug.bugDesc" cssClass="text_c"
								cssStyle="width:360;padding:2 0 0 4;"></ww:textfield>
						</td>
						<td style="border-right:0">&nbsp;</td>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							发现版本:
						</td>
						<td class="tdtxt" width="100" style="border-right:0">
							<ww:hidden id="bugReptVerF" name="dto.bug.bugReptVer"></ww:hidden>
							<ww:textfield id="bugReptVerNameF" name="bugReptVerNameF" cssClass="text_c"
								cssStyle="width:100;padding:2 0 0 4;" readonly="true"
								onfocus="popSelWin('bugReptVerF','bugReptVerNameF','verSelStr','发现版本')"></ww:textfield>
						</td>
						
					</tr>
					<tr>
						<td width="100" class="rightM_center" align="right" style="border-right:0">
							状态:
						</td>
						<td width="100" class="tdtxt" align="left" style="border-right:0">
							<ww:select id="currStateIdF" name="dto.bug.currStateId" cssClass="text_c"
								list="#{-1:'分配及重分配'}" headerValue="-1" 
								cssStyle="width:100;padding:2 0 0 4;"></ww:select>
						</td>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							测试所属:
						</td>
						<td class="tdtxt" width="100" style="border-right:0">
							<ww:hidden id="testOwnerIdF" name="dto.bug.testOwnerId"></ww:hidden>
							<ww:textfield id="testOwnNameF" name="testOwnNameF" cssClass="text_c"
								cssStyle="width:100;padding:2 0 0 4;" readonly="true"
								onfocus="popSelWin('testOwnerIdF','testOwnNameF','testSelStr','测试人员')"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							开发所属:
						</td>
						<td class="tdtxt" width="100" style="border-right:0">
							<ww:hidden id="devOwnerIdF" name="dto.bug.devOwnerId"></ww:hidden>
							<ww:textfield id="devOwnerNameF" name="devOwnerNameF" cssClass="text_c"
								cssStyle="width:100;padding:2 0 0 4;" readonly="true"
								onfocus="popSelWin('devOwnerIdF','devOwnerNameF','devStr','修改人')"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							发现日期:
						</td>
						<td class="tdtxt" width="100" style="border-right:0">
							<ww:textfield id="reptDateF" name="dto.bug.reptDate" cssClass="text_c" onclick="showCalendar(this);"
								readonly="true" cssStyle="width:100;padding:2 0 0 4;"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td width="100" class="rightM_center" align="right" style="border-right:0">
							类型:
						</td>
						<td class="tdtxt" width="100" style="border-right:0">
							<ww:hidden id="bugTypeIdF" name="dto.bug.bugTypeId"
								onkeypress="javascript:return;"></ww:hidden>
							<ww:textfield id="bugTypeNameF" name="bugTypeNameF" readonly="true"
								cssStyle="width:100;padding:2 0 0 4;" cssClass="text_c"
								onfocus="popSelWin('bugTypeIdF','bugTypeNameF','typeSelStr','类型')"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							等级:
						</td>
						<td class="tdtxt" width="100" style="border-right:0">
							<ww:hidden id="bugGradeIdF" name="dto.bug.bugGradeId"></ww:hidden>
							<ww:textfield id="bugGradeNameF" name="bugGradeNameF" readonly="true"
								cssStyle="width:100;padding:2 0 0 4;" cssClass="text_c"
								onfocus="popSelWin('bugGradeIdF','bugGradeNameF','gradeSelStr','等级')"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							平台:
						</td>
						<td class="tdtxt" width="100" style="border-right:0">
							<ww:hidden id="platformIdF" name="dto.bug.platformId"></ww:hidden>
							<ww:textfield id="pltfomNameF" name="pltfomNameF" readonly="true"
								cssStyle="width:100;padding:2 0 0 4;" cssClass="text_c"
								onfocus="popSelWin('platformIdF','pltfomNameF','plantFormSelStr','发生平台')"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							来源:
						</td>
						<td class="tdtxt" width="100" style="border-right:0">
							<ww:hidden id="sourceIdF" name="dto.bug.sourceId"></ww:hidden>
							<ww:textfield id="sourceNameF" name="sourceNameF"
								cssStyle="width:100;padding:2 0 0 4;" cssClass="text_c" readonly="true"
								onfocus="popSelWin('sourceIdF','sourceNameF','sourceSelStr','来源')"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td width="100" class="rightM_center" align="right" style="border-right:0">
							发现时机:
						</td>
						<td class="tdtxt" width="100" style="border-right:0">
							<ww:hidden id="bugOccaIdF" name="dto.bug.bugOccaId"></ww:hidden>
							<ww:textfield id="occaNameF" name="occaNameF"
								cssStyle="width:100;padding:2 0 0 4;"
								  readonly="true" cssClass="text_c"
								onfocus="popSelWin('bugOccaIdF','occaNameF','occaSelStr','发现时机')"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							测试时机:
						</td>
						<td class="tdtxt" width="100" style="border-right:0">
							<ww:hidden id="geneCauseIdF" name="dto.bug.geneCauseId"></ww:hidden>
							<ww:textfield id="geneCaseNameF" name="geneCaseNameF"
								cssStyle="width:100;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('geneCauseIdF','geneCaseNameF','geCaseSelStr','测试时机')"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							优先级:
						</td>
						<td class="tdtxt" width="100" style="border-right:0">
							<ww:hidden id="priIdF" name="dto.bug.priId"></ww:hidden>
							<ww:textfield id="priNameF" name="priNameF"
								cssStyle="width:100;padding:2 0 0 4;"
								  readonly="true" cssClass="text_c"
								onfocus="popSelWin('priIdF','priNameF','priSelStr','优先级')"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							频率:
						</td>
						<td class="tdtxt" width="100" style="border-right:0">
							<ww:hidden id="bugFreqIdF" name="dto.bug.bugFreqId"></ww:hidden>
							<ww:textfield id="bugFreqNameF" name="bugFreqNameF"
								cssStyle="width:100;padding:2 0 0 4;"
								 readonly="true" cssClass="text_c"
								onfocus="popSelWin('bugFreqIdF','bugFreqNameF','freqSelStr','频率')"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td id="impPhaTdtxtF" width="100" class="rightM_center" align="right" style="border-right:0">
							引入原因:
						</td>
						<td id="impPhaTdF" class="tdtxt" width="100" align="left" style="border-right:0">
							<ww:hidden id="genePhaseIdF" name="dto.bug.genePhaseId"></ww:hidden>
							<ww:textfield id="genPhNameF" name="genPhNameF"
								cssStyle="width:100;padding:2 0 0 4;" cssClass="text_c" readonly="true"
								onfocus="popSelWin('genePhaseIdF','genPhNameF','genePhaseSelStr','引入原因')"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							关联用例:
						</td>
						<td width="100" class="tdtxt" align="left" style="border-right:0">
							<ww:select id="relaCaseFlagF" name="dto.bug.relaCaseFlag" cssClass="text_c"
								list="#{-1:'',1:'己关联',2:'未关联'}" headerValue="-1" 
								cssStyle="width:100;padding:2 0 0 4;">
							</ww:select>
						</td>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							由我分配
						</td>
						<td width="100" class="tdtxt" align="left" style="border-right:0">
							<ww:checkbox id="defBugId" name="dto.defBug" value="false"
								fieldValue="1"  />
						</td>
						<td width="80" id="newQuery" class="rightM_center" align="right" style="border-right:0">
							
						</td>
						<td width="90" id="newQuery" class="tdtxt" align="left" style="border-right:0">
							<ww:hidden id="saveQuery" name="saveQuery" value="0"></ww:hidden>
							<div style="display:none">
							<ww:checkbox id="saveQueryN" name="dto.saveQueryN"
								onclick="newQuery(this)" value="false" fieldValue="1" />
							</div>
						</td>
					</tr>
					<tr id="newQueryNameTr" style="display: none;border-right:0">
						<td width="100" class="rightM_center" >
							查询器名:
						</td>
						<td  align="left" colspan="4" width="360" style="border-right:0">
							<ww:textfield id="queryName" name="dto.queryName" cssClass="text_c"
								cssStyle="width:360;padding:2 0 0 4;"></ww:textfield>
						</td>
						<td width="90"  align="right" class="rightM_center" style="border-right:0">
							跨任务应用:
						</td>
						<td width="180" class="tdtxt" align="left" colspan="2" style="border-right:0">
							<ww:checkbox id="appScope" name="dto.appScope"
								 value="false" fieldValue="1" />
							&nbsp;(测试需求和当前任务被忽略)
						</td>
					</tr>
					<tr>
						<td class="tdtxt" align="center" width="720" colspan="8" style="border-right:0">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td class="rightM_center"  align="center" width="720" colspan="8" style="border-right:0">
							<a class="bluebtn" href="javascript:void(0);"
								onclick="javascript:fW_ch.setModal(false);fW_ch.hide();	if(typeof(mypmCalendar_ch)!='undefined'){mypmCalendar_ch.hide();};if(typeof(popW_ch)!='undefined'){popW_ch.hide();};if(typeof(testTaskUserSelW_ch)!='undefined'){testTaskUserSelW_ch.hide();};if(typeof(treeW_ch)!='undefined'){treeW_ch.hide();};"
								style="margin-left: 6px"><span> 返回</span> </a>
							<a class="bluebtn" href="javascript:void(0);" onclick="resetQuery()"
								style="margin-left: 6px"><span> 重置</span> </a>
							<div id="newQueryBtn" style="display: none">
								<a class="bluebtn" href="javascript:void(0);" onclick="saveQuery()"
									style="margin-left: 6px"><span> 保存查询器</span> </a>
								<a class="bluebtn" href="javascript:void(0);" onclick="query('save')"
									style="margin-left: 6px"><span> 查询并保存查询器</span> </a>
							</div>
							<a class="bluebtn" href="javascript:void(0);" onclick="query()"><span>
									查询</span> </a>
						<td>
					</tr>
					<tr>
						<td class="tdtxt" align="center" width="720" colspan="8" style="border-right:0">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td class="tdtxt" align="center" width="720" colspan="8" style="border-right:0">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td class="tdtxt" align="center" width="720" colspan="8" style="border-right:0">
							&nbsp;
						</td>
					</tr>
				</table>
			</ww:form>
		</div>
		</div>
	<div id="selPeopleDiv"   class="cycleTask gridbox_light" style="border:0px;display:none;">
	  <div class="objbox" style="overflow:auto;width:100%;">
		  <ww:form theme="simple" method="post" id="assignForm" name="assignForm" namespace="/outLineManager" action="">
			<ww:hidden id="currOwner" name="dto.currOwner"></ww:hidden>
			<ww:hidden id="currProjectId" name="dto.projectId"></ww:hidden>
		    <ww:hidden id="currTaskId" name="dto.taskId" ></ww:hidden>
		    <ww:hidden id="bugDescA" name="dto.bug.bugDesc"></ww:hidden>
		    <ww:hidden id="defBugIdA" name="dto.defBug"></ww:hidden>
		    <ww:hidden id="relaCaseFlagA" name="dto.bug.relaCaseFlag"></ww:hidden>
		    <ww:hidden id="testFlowAssign" name="dto.testFlow"></ww:hidden>
		    <ww:hidden id="moduleNameA" name="dto.moduleName"></ww:hidden>
		    <ww:hidden id="assignProName" name="dto.stateName"></ww:hidden>
		    
			<table  class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				  <td colspan="2" style="border-right:0">&nbsp;</td>
				</tr>
				<tr>
				  <td align="center" colspan="2" style="border-right:0">
				  	  <a class="bluebtn" href="javascript:void(0);" id="fres_btn"onclick="loadSelUser()"
					        style="margin-left: 6px;"><span> 刷新备选人员</span> </a>
				  </td>
				</tr>
				<tr height="2">
				  <td colspan="2" style="border-right:0">&nbsp;</td>
				</tr>
				<tr height="290" class="ev_mypm" >
					<td colspan="2" style="border-right:0">
						<div id="selGridbox" style="float:left;width:175px;"></div>
					</td>
				</tr>
			</table>
		</ww:form>
	  </div>
  </div>
		<ww:include value="/jsp/common/downLoad.jsp"></ww:include>
		<ww:include value="/jsp/common/testTaskSelSinglePersion.jsp"></ww:include>
	</BODY>
	<script type="text/javascript" src="<%=request.getContextPath()%>/jsp/bugManager/window.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/jsp/bugManager/bugHandling.js"></script>
   <link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/calendar/codebase/dhtmlxcalendar.css">	
   <script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/calendar/codebase/dhtmlxcalendar.js"></script>							
	<script type="text/javascript">
		importJs(conextPath+"/jsp/common/upload.js");
		var resVar = "1/"+pageSize+"/0$";
    	if($("listStr").value==resVar&&outLineCom){
    		hintMsg('当前项目没有您要分配的BUG,要查看其他BUG请查询');
    	}	
    	$("defBugId").checked=true;		
	</script>
</HTML>
