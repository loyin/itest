	function closeMain(){
		if(typeof popW_ch !="undefined"&&!popW_ch.isHidden()){
			popW_ch.setModal(false);
			popW_ch.hide();
		}
		cuW_ch.hide();
		cuW_ch.setModal(false);
	}
	function clearQueryTd(){
		$("queryNameF").value="";
		$("queryBtnTd").style.display="none";
		$("currQueryId").value="";
	}

	function findInit(win){
		if($("customSprHomeTaskId").value==""){
			hintMsg("请先切换到某个测试项目");
			return;
		}
		if(typeof win=="undefined"){
			opreType="query";
		}else{
			handWin = win;
			opreType = "repeFind";
		}
		var url="";
		if($("typeSelStr").value==""){
			url=conextPath +"/bugManager/bugManagerAction!findInit.action?dto.isAjax=true&dto.loadType=1";
		}else{
			url=conextPath +"/bugManager/bugManagerAction!findInit.action?dto.isAjax=true&dto.loadType=0";
		}
		var ajaxResut = postSub(url,"");
		adjustTable("findTable");
		if(ajaxResut=="failed"){
			hintMsg('初始查询化失败');
			return ;
		}	
		fW_ch = initW_ch(fW_ch,  "findDiv", true,800, 280);
		fW_ch.button("close").attachEvent("onClick", function(){
			if(typeof(mypmCalendar_ch)!='undefined')
			 mypmCalendar_ch.hide();
		});
		$("bugDescF").focus();
		fW_ch.setText("查询");
		setUpInfo(ajaxResut);
		fW_ch.bringToTop();
	}
	function setUpInfo(updInfo){
		if(updInfo==""){
			return;
		}
		var updInfos = updInfo.split("^");
		var valueStr = "";
		var selStr = "";
		var selId = ""
		for(var i=0; i < updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] != "null"){
				currInfo[1] =recovJsonKeyWord(currInfo[1]);
				valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";
				eval(valueStr);	
				if(currInfo[0]=="ReProStep"){
					$("initReProStep").value=currInfo[1];
					initImg = getAttaInRichHtm(currInfo[1])
				}else if(currInfo[0]=="nextFlowCd"){
					flowControl_new(currInfo[1]);
				}else if(currInfo[0]=="testFlow"){
					flwStr=currInfo[1];
				}else if(currInfo[0]=="roleInTask"){
					roleStr =currInfo[1];
				}else if(currInfo[0]=="attachUrl"&&currInfo[1]!=""){
					 $("currAttach").style.display="";
					 $("currAttach").title="附件";
					// $("currAttach").title=currInfo[1].substring(currInfo[1].indexOf("_")+1);			
				}			
			}
		}
	}
	function disReset(){
		$("impPhaTdtxt").style.display="none";
		$("impPhaTd").style.display="none";
		$("repropTd").style.display="none";
		$("reprop").style.display="none";
		$("geneCauseTd").style.display="none";
		$("geneCauseF").style.display="none";
		$("testOwnerTr").style.display="none";
		$("analOwnerTr").style.display="none";
		$("anasnOwnerTr").style.display="none";
		$("devOwnerTr").style.display="none";
		$("intecsOwnerTr").style.display="none";
		$("assFromMdTd").style.display="none";	
	}
	function flowControl_new(nextFlowCd){
		if(nextFlowCd=="2"){
			$("testOwnerTr").style.display="";
		}else if(nextFlowCd=="3"){
			$("analOwnerTr").style.display="";
		}else if(nextFlowCd=="4"){
			$("anasnOwnerTr").style.display="";
		}else if(nextFlowCd=="5"){
			$("devOwnerTr").style.display="";
		}else if(nextFlowCd=="7"){
			$("intecsOwnerTr").style.display="";
		}
	}
	var oEditor ;
	function loadFCK(){
		if(typeof oEditor != "undefined"){
			oEditor.SetData("");
			return;
		}
		importFckJs();
    	var pmEditor = new FCKeditor('reProStep') ;
    	pmEditor.BasePath = conextPath+"/pmEditor/" ;
    	pmEditor.Height = 200;
    	pmEditor.ToolbarSet = "Basic" ; 
    	pmEditor.ToolbarStartExpanded=false;
    	pmEditor.ReplaceTextarea();
	}
	function FCKeditor_OnComplete( editorInstance ){
		oEditor = FCKeditorAPI.GetInstance('reProStep') ;
		oEditor.SetData("");
		return;
	}	


	String.prototype.trim = function(){
	     var _argument = arguments[0]==undefined ? " ":arguments[0];
	     if(typeof(_argument)=="string"){
	        return this.replace(_argument == " "?/(^s*)|(s*$)/g : new RegExp("(^"+_argument+"*)|("+_argument+"*$)","g"),"");
	     }else if(typeof(_argument)=="object"){
	        return this.replace(_argument,"")
	     }else if(typeof(_argument)=="number" && arguments.length>=1){
	        return arguments.length==1? this.substring(arguments[0]) : this.substring(arguments[0],this.length-arguments[1]);
	     }
	};
	
	function resetHdeField(){
		$("moduleId").value="";
		$("bugTypeId").value="";
		$("bugGradeId").value="";
		$("bugFreqId").value="";
		$("bugOccaId").value="";
		$("priId").value="";
		$("genePhaseId").value="";
		$("geneCauseId").value="";
		$("sourceId").value="";
		$("platformId").value="";
		$("devOwnerId").value="";
		$("testOwnerId").value="";
		$("analyseOwnerId").value="";
		$("assinOwnerId").value="";
		$("intercessOwnerId").value="";
		$("platformId").value="";
		$("cUMTxt").innerHTML = "";
		if(typeof oEditor != "undefined"){
			oEditor.SetData("");
		}
		
	}
	var contin ="";	
	function delFromStr(StrVal,septor,intceptStr){
		var strTmp = StrVal.split(intceptStr);
		var tmpAf = strTmp[1];
		var tmpBe = strTmp[0];
		if(tmpBe!=""&&tmpBe.lastIndexOf("]")==tmpBe.length-1){
			tmpBe=tmpBe.substring(0,tmpBe.length-1);
			tmpBe=tmpBe.substring(0,tmpBe.indexOf("["));
		}
		if(tmpAf!=""&&tmpAf.indexOf("[")==0){
			tmpAf=tmpAf.substring(1,tmpAf.length);
			tmpAf=tmpAf.substring(tmpAf.indexOf("]")+1,tmpAf.length);
		}
		if(tmpBe!=""&&tmpBe.lastIndexOf(septor)==tmpBe.length-1){
				tmpBe=tmpBe.substring(0,tmpBe.length-1);
		}
		var subStr = tmpBe+tmpAf;
		if(subStr.indexOf(septor)==0){
			subStr=subStr.substring(1);
		}
		return subStr;
	}
	var pageUrl = conextPath+ "/bugManager/bugManagerAction!batchAssign.action?dto.isAjax=true";
	var repeFinUrl = "";
	function findByquery(){
		if($("currQueryId").value==""){
			hintMsg('您没有选择查询器');
			return;
		}
		var url = conextPath+ "/bugManager/bugManagerAction!batchAssFindByquery.action?dto.queryId="+$("currQueryId").value;
		if(opreType !="repeFind"){
			url+="&dto.pageSize=" + pageSize+"&dto.taskId="+$("customSprHomeTaskId").value;
		}
		var ajaxResut = postSub(url,"");
		if(ajaxResut!="failed"){
			if(ajaxResut.split("$")[1]!=""){
				$("listStr").value=ajaxResut;
				if(opreType =="repeFind"){
					fW_ch.setModal(false);
					fW_ch.hide();
					handWin.popRepeWin();
					repeFinUrl= conextPath+ "/bugManager/bugManagerAction!findByquery.action?dto.isAjax=true&dto.queryId="+$("currQueryId").value;
					if(typeof(mypmCalendar_ch)!="undefined")
						mypmCalendar_ch.hide();
					return;
				}
				colTypeReset();
				initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");	
				loadLink();
				sw2Link();
				fW_ch.setModal(false);
				fW_ch.hide();	
				if(typeof(mypmCalendar_ch)!="undefined")
						mypmCalendar_ch.hide();
				pageUrl = conextPath+ "/bugManager/bugManagerAction!batchAssFindByquery.action?dto.isAjax=true&dto.queryId="+$("currQueryId").value+"&dto.taskId="+$("customSprHomeTaskId").value;
			}else{
				hintMsg('没有查询到相关记录');
			}
		}else{
			hintMsg('执行查询时发生错误');
		}	
	}

	function query(saveFlg){
		var url = conextPath+ "/bugManager/bugManagerAction!batchAssign.action?dto.isAjax=true";
		if(opreType !="repeFind"){
			//url+="&dto.pageSize=" + pageSize+"&dto.taskId="+$("customSprHomeTaskId").value;
			url+="&dto.pageSize=" + pageSize;
		}
		var ajaxResut = postSub(url,"findForm");
		if(ajaxResut!="failed"){
			var data = ajaxResut.split("$");
			if(typeof saveFlg != "undefined"){
				if($("queryCount").value>0&&$("querySelStr").value!=""){
					$("querySelStr").value+="$"+data[0]+";"+getQeyAliasName();
				}
				$("queryCount").value ="1" ;
				$("listStr").value = data[1]+"$"+data[2];
			}else{
				$("listStr").value=ajaxResut;
			}
			if($("listStr").value.split("$")[1]!=""){
				if(opreType =="repeFind"){
					fW_ch.setModal(false);
					fW_ch.hide();
					handWin.popRepeWin();
					repeFinUrl=conextPath+ "/bugManager/bugManagerAction!findBug.action?dto.isAjax=true";
					if(typeof(mypmCalendar_ch)!="undefined")
						mypmCalendar_ch.hide();
					return;
				}
			 	colTypeReset();
				initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");	
				loadLink();
				sw2Link();
				fW_ch.setModal(false);
				fW_ch.hide();
				if(typeof(mypmCalendar_ch)!="undefined")
					mypmCalendar_ch.hide();
				pageUrl = conextPath+ "/bugManager/bugManagerAction!batchAssign.action?dto.isAjax=true";	
			}else{
				hintMsg('没有查询到相关记录');
			}
		}else{
			hintMsg('执行查询时发生错误');
		}	
	}
	function getQeyAliasName(){
		var quryAliasNme = $("queryName").value;
		if($("defBugId").checked){
			if($("appScope").checked){
				quryAliasNme+="[与我有关--跨任务]";
			}else{
				quryAliasNme+="[与我有关]";
			}
		}else if($("appScope").checked){
			quryAliasNme+="[跨任务]";
		}
		return 	quryAliasNme;
	}
	function queryDtal(){
		if($("currQueryId").value==""){
			hintMsg('您没选择查询器');
			return;
		}
		var url = conextPath+ "/bugManager/bugManagerAction!queryDetail.action?dto.isAjax=true&dto.queryId="+$("currQueryId").value;
		var ajaxResut = postSub(url,"");
		if(ajaxResut!="failed"){
			resetQuery("show");
			setQuryDtalInfo(ajaxResut);
			//$("upQuBtn").style.display="";
			$("saveQueryN").checked=false;
			$("newQueryNameTr").style.display="";
			return;
		}else if(ajaxResut.indexOf("failed^")!=-1){
			hintMsg('查询器己被删除');
		}
		hintMsg('加载查询器发生错误');
	}
	
	function setQuryDtalInfo(queryInfo){
		if(queryInfo==""){
			return;
		}
		var queryInfos = queryInfo.split("^");
		for(var i=0; i < queryInfos.length; i++){
			var currInfo = queryInfos[i].split("=");
			if(currInfo[1] != "null"&&currInfo[0]!="taskId"&&currInfo[0]!="defBugId"&&currInfo[0]!="appScope"){
				if(currInfo[0]=="bugDescF"){
					currInfo[1] = currInfo[1].substring(1,currInfo[1].length-1);
				}
				currInfo[1] =recovJsonKeyWord(currInfo[1]);
				valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";
				eval(valueStr);	
				loadName(currInfo[0],currInfo[1]);
			}
			if(currInfo[0]=="appScope"&&currInfo[1]=="1"){
				$("appScope").checked=true;
			}else if(currInfo[0]=="defBugId"&&currInfo[1]=="1"){
				$("defBugId").checked=true;
			}else if(currInfo[0]=="defBugId"&&currInfo[1]=="0"){
				$("defBugId").checked=false;
			}
			if(currInfo[0]=="moduleNameF"&&currInfo[1]!=""){
				$("mdBtn").style.display="";
			}
		}	
	}
	function loadName(fieldId,fieldVal){
		if(fieldId=="genePhaseIdF"){ 
 			$("genPhNameF").value=getName($("genePhaseSelStr").value,fieldVal,fieldId);
		}else if(fieldId=="bugFreqIdF"){
		    $("bugFreqNameF").value=getName($("freqSelStr").value,fieldVal,fieldId);
		}else if(fieldId=="priIdF"){
		     $("priNameF").value=getName($("priSelStr").value,fieldVal,fieldId);   
		}else if(fieldId=="geneCauseIdF"){
		  	$("geneCaseNameF").value=getName($("geCaseSelStr").value,fieldVal,fieldId);
		}else if(fieldId=="bugOccaIdF"){
		    $("occaNameF").value=getName($("occaSelStr").value,fieldVal,fieldId);
		}else if(fieldId=="sourceIdF"){
		    $("sourceNameF").value=getName($("sourceSelStr").value,fieldVal,fieldId); 
		}else if(fieldId=="platformIdF"){ 
		 	$("pltfomNameF").value=getName($("plantFormSelStr").value,fieldVal,fieldId);
		}else if(fieldId=="bugGradeIdF"){ 
		 	$("bugGradeNameF").value=getName($("gradeSelStr").value,fieldVal,fieldId);
		}else if(fieldId=="bugTypeIdF"){
			$("bugTypeNameF").value=getName($("typeSelStr").value,fieldVal,fieldId);
		}else if(fieldId=="bugReptVerF"){
			$("bugReptVerNameF").value=getName($("verSelStr").value,fieldVal,fieldId);
		}		
	}
	function getName(selStr,fieldVal,fieldId){
		if(selStr.indexOf(fieldVal+";")<0){
			$(fieldId).value="-1";
			return;
		}
		var nameTmp = selStr.split(fieldVal+";");
		if(nameTmp[1].indexOf("$")>0){
			return nameTmp[1].substring(0,nameTmp[1].indexOf("$"));
		}else{
			return nameTmp[1];
		}	
	}
	function resetQuery(mdBtnHide){
		$("newQueryNameTr").style.display="none";
		$("newQueryBtn").style.display="none";
		if(typeof mdBtnHide == "undefined"){
			$("mdBtn").style.display="none";
		}
		var queryNameF = $("queryNameF").value;
		findForm.reset();
		$("queryNameF").value= queryNameF;
		$("upQuBtn").style.display="none";
		$("genePhaseIdF").value="-1";
		$("bugFreqIdF").value="-1";
		$("priIdF").value="-1";
		$("geneCauseIdF").value="-1";
		$("bugOccaIdF").value="-1";
		$("sourceIdF").value="-1";
		$("platformIdF").value="-1";
		$("bugGradeIdF").value="-1";
		$("bugTypeIdF").value="-1";
		$("devOwnerIdF").value="-1";
		$("testOwnerIdF").value="-1";
		$("moduleIdF").value="-1";
		$("queryId").value="-1";			
	}
