	function closeMe(){
		parent.editW_ch.setModal(false);
		parent.editW_ch.hide();
		if(typeof parent.treeW_ch != "undefined"){
			parent.treeW_ch.hide();
		}
		try{parent.collapseLayout('open');}catch(err){}
		return;
	}
	var popW_ch;
	var popGrid;
	var tyId,tpNa;

	function upSubCheck(){
		if($("moduleId").value==""){
			hintMsg("请选择对应测试需求");
			return false;
		}else if(isAllNull($("bugDesc").value)){
			hintMsg("请填写问题描述");
			$("bugDesc").focus();
			return false;
		}else if($("bugTypeId").value==""){
			hintMsg("请选择类型");
			return false;
		}else if($("bugGradeId").value==""){
			hintMsg("请选择等级");
			return false;
		}else if($("platformId").value==""){
			hintMsg("请选择平台");
			return false;
		}else if($("sourceId").value==""){
			hintMsg("请选择来源");
			return false;
		}else if($("bugOccaId").value==""){
			hintMsg("请选择发现时机");
			return false;
		}else if($("geneCauseF").style.display==""&& $("geneCauseId").value==""){
			hintMsg("请选测试时机");
			return false;		
		}else if($("bugFreqId").value==""){
			hintMsg("请选择频率");
			return false;
		}else if($("reprop").style.display==""&&$("reproPersent").value==""){
			hintMsg("请填写再现比例");
			$("reprop").focus();
			return false;		
		}else{
			$("reProStep").value=oEditor.GetXHTML().replace(/\s+$|^\s+/g,"");
			if($("reProStep").value=="<br />"||$("reProStep").value==""||$("reProStep").value=="<strong><br></strong>"){
				hintMsg("请填写再现步骤");
				oEditor.Focus();
				return false;			
			}
			for(var i=1; i<31;i++){
				$("reProStep").value = $("reProStep").value.trim("<br>");
				$("reProStep").value = $("reProStep").value.trim("&nbsp;");	
				$("reProStep").value = $("reProStep").value.replace(/\s+$|^\s+/g,"");	
				$("reProStep").value = $("reProStep").value.trim("\\");		
			}
			if(parent.checkIsOverLong($("reProStep").value,1000)){
				hintMsg("再现步骤不能超过1000个字符");
				return false;			
			}
			var repTxt = $("reProStep").value.replace(/\s+$|^\s+/g,"");
			var mdPath = "<strong>"+$("moduleName").value +"</strong>:";
			if(repTxt==mdPath){
				hintMsg("请填写再现步骤");
				oEditor.Focus();
				return false;			
			}
			$("reProTxt").value=html2PlainText("reProStep");
		}
		if(parent.checkIsOverLong($("bugDesc").value,150)){
			hintMsg("描述不能超过150个字符");
			return false;			
		}
		return true;
	}

	function upSub(){
		if(upSubCheck()){
			var url = conextPath + "/bugManager/bugManagerAction!update.action?dto.isAjax=true";
			var ajaxResut = postSub(url,"createForm");
			if(ajaxResut.indexOf("^") != -1){
				ajaxResut = ajaxResut.replace(/[\r\n]/g, "");
				var result = ajaxResut.split("^");
				if(result[0] == "success"){
					eraseAttach();
					$("cUMTxt").innerHTML =  "操作成功";
					parent.upRow(result[1],result[2]);
					closeMe();
					upVarReset();
					return;					
				}else{
					$("cUMTxt").innerHTML =  "操作失败";
				}
			}else{
				$("cUMTxt").innerHTML =  "操作失败";
			}
		}
	}