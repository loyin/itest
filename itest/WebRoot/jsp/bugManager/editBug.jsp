<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<HTML>
	<HEAD>
		<TITLE>BUG管理</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page2.css'>
		<script type="text/javascript" src="<%=request.getContextPath()%>/pmEditor/fckeditor.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
	    <script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
	     <script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.js"></script>
	    <script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/windows/codebase/ext/dhtmlxwindows_wtb.js"></script>	
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
	</HEAD>
	<script type="text/javascript">
	var flwPageNo=1,flwPageCount=0,flwBar;
	var reProStep ="${dto.bug.reProStep2}";
	var myId = "${session.currentUser.userInfo.id}";
	if("${dto.bug.typeSelStr}"!=""){
		parent.$("typeSelStr").value="${dto.bug.typeSelStr}";  
		parent.$("gradeSelStr").value="${dto.bug.gradeSelStr}";     
		parent.$("freqSelStr").value="${dto.bug.freqSelStr}";      
		parent.$("occaSelStr").value="${dto.bug.occaSelStr}";      
		parent.$("geCaseSelStr").value="${dto.bug.geCaseSelStr}";    
		parent.$("sourceSelStr").value="${dto.bug.sourceSelStr}";    
		parent.$("plantFormSelStr").value="${dto.bug.plantFormSelStr}";  
		parent.$("genePhaseSelStr").value="${dto.bug.genePhaseSelStr}";  
		parent.$("priSelStr").value="${dto.bug.priSelStr}"; 
		parent.$("verSelStr").value="${dto.bug.verSelStr}";      
	}
	parent.$("assignSelStr").value="${dto.bug.assignSelStr}";      
	parent.$("analySelStr").value="${dto.bug.analySelStr}"; 
	parent.$("testSelStr").value="${dto.bug.testSelStr}";
	parent.$("devStr").value="${dto.bug.devStr}";    
	parent.roleStr="${dto.bug.roleInTask}";
	parent.flwStr="${dto.bug.testFlow}";  
	parent.editW_ch.setText("修改软件问题报告"); 
	var flwStr ="${dto.testFlow}";
	var roleStr ="${dto.roleInTask}"; 
	var taskId = parent.pmGrid.cells("${dto.bug.bugId}",21).getValue();
	var url = conextPath + "/bugManager/bugManagerAction!getInTaskRole.action?dto.taskId="+taskId;
	var ajaxResut = postSub(url,"");
	roleStr = ajaxResut;
	var oEditor ;
	function loadFCK(){
		if(typeof oEditor != "undefined"){
			oEditor.SetData(reProStep) ;	
			return;$("initReProStep").value
		}
    	var pmEditor = new FCKeditor('reProStep') ;
    	pmEditor.BasePath = conextPath+"/pmEditor/" ;
    	pmEditor.Height = 200;
    	pmEditor.ToolbarSet = "Basic" ; 
    	pmEditor.ToolbarStartExpanded=false;
    	pmEditor.ReplaceTextarea();
    	$("bugDesc").focus();
	}
	function FCKeditor_OnComplete( editorInstance ){
		oEditor = FCKeditorAPI.GetInstance('reProStep') ;
		oEditor.SetData(recovJsonKeyWord(reProStep,"notRepComma")) ;	
		return;
	}	
	</script>
	<BODY  onload="loadFCK()" style="overflow-y:auto;">
	<table align="center">
	<tr><td>
		<div align="center">
		<table border="0" style="width:100%" cellpadding="0" cellspacing="0" >
		  <tr>
	    	<td class="jihuo" id="baseInfoTab" width="60" class="tdtxt" align="right"><strong><a onclick="styleChange(1)"  href="javascript:void(0);">基本信息</a></strong></td>
	    	<td class="nojihuo" id="flowInfoTab" width="60" class="tdtxt" align="left"><strong><a onclick="styleChange(2)"  href="javascript:void(0);">处理历史</a></strong></td>
		    <td colspan="6" width="600">&nbsp;</td>
		  </tr>
		</table>
		</div>
	</td>
	</tr>
	<tr><td>
		<div id="baseInfoDiv"align="center" class="cycleTask gridbox_light" style="border:0px;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			 <ww:hidden id="typeSelStr" name="dto.bug.typeSelStr" ></ww:hidden>
			 <ww:hidden id="gradeSelStr" name="dto.bug.gradeSelStr" ></ww:hidden>
			 <ww:hidden id="freqSelStr" name="dto.bug.freqSelStr" ></ww:hidden>
			 <ww:hidden id="occaSelStr" name="dto.bug.occaSelStr" ></ww:hidden>
			 <ww:hidden id="geCaseSelStr" name="dto.bug.geCaseSelStr"></ww:hidden>
			 <ww:hidden id="sourceSelStr" name="dto.bug.sourceSelStr" ></ww:hidden>
			 <ww:hidden id="plantFormSelStr" name="dto.bug.plantFormSelStr" ></ww:hidden>
			 <ww:hidden id="genePhaseSelStr" name="dto.bug.genePhaseSelStr" ></ww:hidden>
			 <ww:hidden id="verSelStr" name="dto.bug.verSelStr" ></ww:hidden>
			 <ww:hidden id="priSelStr" name="dto.bug.priSelStr" ></ww:hidden>
			 <ww:hidden id="testSelStr" name="dto.bug.testSelStr" ></ww:hidden>
			 <ww:hidden id="devStr" name="dto.bug.devStr" ></ww:hidden>
			 <ww:hidden id="assignSelStr" name="dto.bug.assignSelStr" ></ww:hidden>
			 <ww:hidden id="analySelStr" name="dto.bug.analySelStr"></ww:hidden>
			 <ww:hidden id="interCesSelStr" name="dto.bug.interCesSelStr" ></ww:hidden>
			 <ww:hidden id="testPhase" name="dto.bug.testPhase"></ww:hidden>
			 <input type="hidden" id="listStr" name="listStr" value=""/>
		  <ww:form theme="simple" method="post" id="createForm" name="createForm" namespace="" action="">
			 <ww:hidden id="bugId" name="dto.bug.bugId"></ww:hidden>
	     	 <ww:hidden id="reProTxt" name="dto.bug.reProTxt"></ww:hidden>
			 <ww:hidden id="bugReptId" name="dto.bug.bugReptId"></ww:hidden>
			 <ww:hidden id="currHandlerId" name="dto.bug.currHandlerId"></ww:hidden>
			 <ww:hidden id="currStateId" name="dto.bug.currStateId"></ww:hidden>
			 <ww:hidden id="nextFlowCd" name="dto.bug.nextFlowCd"></ww:hidden>
			 <ww:hidden id="currFlowCd" name="dto.bug.currFlowCd"></ww:hidden>
			 <ww:hidden id="currRemark" name="dto.bug.currRemark"></ww:hidden>
			 <ww:hidden id="currHandlDate" name="dto.bug.currHandlDate"></ww:hidden>
			 <ww:hidden id="bugResoVer" name="dto.bug.bugResoVer"></ww:hidden>
			 <ww:hidden id="reptDate" name="dto.bug.reptDate"></ww:hidden>
			 <ww:hidden id="msgFlag" name="dto.bug.msgFlag"></ww:hidden>
			 <ww:hidden id="bugAntimodDate" name="dto.bug.bugAntimodDate"></ww:hidden>
			 <ww:hidden id="planAmendHour" name="dto.bug.planAmendHour"></ww:hidden>
			 <ww:hidden id="relaCaseFlag" name="dto.bug.relaCaseFlag"></ww:hidden>
			 <ww:hidden id="initReProStep" name="dto.initReProStep" ></ww:hidden>
			 <ww:hidden id="testOwnerId" name="dto.bug.testOwnerId"></ww:hidden>
			 <ww:hidden id="analyseOwnerId" name="dto.bug.analyseOwnerId"></ww:hidden>
			 <ww:hidden id="assinOwnerId" name="dto.bug.assinOwnerId"></ww:hidden>
			 <ww:hidden id="devOwnerId" name="dto.bug.devOwnerId"></ww:hidden>
			 <ww:hidden id="intercessOwnerId" name="dto.bug.intercessOwnerId"></ww:hidden>
			 <ww:hidden id="attachUrl" name="dto.bug.attachUrl"></ww:hidden>
			 <ww:hidden id="nextOwnerId" name="dto.bug.nextOwnerId"></ww:hidden>
			 <table  border="0" id="baseTable" class="obj row20px" cellspacing="0" align="center" cellpadding="0" border="0" width="100%">
    		   <tr>
   			    <td colspan="6" class="tdtxt" align="center" width="720" style="border-right:0">
   			      <div id="cUMTxt" align="center" style="color: Blue; padding: 2px;"></div>&nbsp;
   			    </td>
    		   </tr>
			    <tr>
			      <td width="80" class="rightM_center" align="right" style="color:red;border-right:0" >测试需求:</td>
			      <td  class="tdtxt"  align="left" colspan="5"  width="640" style="padding:2 0 0 4;border-right:0">
			      <ww:hidden id="moduleId" name="dto.bug.moduleId"></ww:hidden>
    			     <ww:textfield id="moduleName" name="dto.moduleName" cssClass="text_c" cssStyle="width:640;padding:2 0 0 4;" readonly='true' onblur="javascript:this.readonly='true';" onfocus="parent.loadTree(window)"></ww:textfield>
    			  </td>
			    </tr>
			    <tr>
	 		     <td width="80" class="rightM_center" align="right" style="border-right:0">状态:</td>
	 			 <td  class="tdtxt"  align="left"   width="160" style="padding:2 0 0 4;border-right:0">
	 			   <ww:property value="dto.stateName" escape="false" id="stateName"/>
	 		     </td>
	 		     <td width="80" class="rightM_center" align="right" style="border-right:0">发现人:</td>
	 			 <td  class="tdtxt"  align="left"  width="160" style="padding:2 0 0 4;border-right:0">
	 			   <ww:property value="dto.bug.author.uniqueName"/>
	 		     </td>	
	 		     <td width="80" class="rightM_center" align="right" style="border-right:0">处理人:</td>
	 			 <td  class="tdtxt"  align="left"  width="160" style="padding:2 0 0 4;border-right:0">
	 			   <ww:property value="dto.bug.currHander.uniqueName"/>
	 		     </td>		    
			    </tr>
			    <tr>
			      <td width="80" class="rightM_center" align="right" style="color:red;border-right:0">Bug描述:</td>
			      <td  class="tdtxt" colspan="5" width="640" style="padding:2 0 0 4;border-right:0">
    			      <ww:textfield id="bugDesc" name="dto.bug.bugDesc" cssClass="text_c" cssStyle="width:640;padding:2 0 0 4;" ></ww:textfield>
    			  </td>
			    </tr>
			   <tr>
	 		     <td width="80" class="rightM_center" align="right" style="color:red;border-right:0">类型:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			 <ww:hidden id="bugTypeId" name="dto.bug.bugTypeId" onkeypress="javascript:return;"></ww:hidden>
	 			   <ww:textfield id="bugTypeName" name="dto.bug.dtoHelper.bugType.typeName" cssClass="text_c" cssStyle="width:160;padding:2 0 0 4;"  readonly='true' onblur="javascript:this.readonly='true';" onfocus="popSelWin('bugTypeId','bugTypeName','typeSelStr','类型')"></ww:textfield>
	 		     </td>	
	 		     <td width="80" class="rightM_center" align="right" style="color:red;border-right:0">等级:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			 <ww:hidden id="bugGradeId" name="dto.bug.bugGradeId"></ww:hidden>
	 			   <ww:textfield id="bugGradeName" name="dto.bug.dtoHelper.bugGrade.typeName" cssClass="text_c" cssStyle="width:160;padding:2 0 0 4;" readonly='true'  onblur="javascript:this.readonly='true';" onfocus="popSelWin('bugGradeId','bugGradeName','gradeSelStr','等级')"></ww:textfield>
	 		     </td>
	 		     <td width="80" class="rightM_center" align="right" style="color:red;border-right:0">平台:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			 <ww:hidden id="platformId" name="dto.bug.platformId"></ww:hidden>
	 			  <ww:textfield id="pltfomName" name="dto.bug.dtoHelper.occurPlant.typeName" cssClass="text_c" cssStyle="width:160;padding:2 0 0 4;" readonly='true'  onblur="javascript:this.readonly='true';" onfocus="popSelWin('platformId','pltfomName','plantFormSelStr','发生平台')"></ww:textfield>
	 		     </td>	
			   </tr>
			   <tr>
	 		     <td width="80" class="rightM_center" align="right" style="color:red;border-right:0">来源:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			  <ww:hidden id="sourceId" name="dto.bug.sourceId"></ww:hidden>
	 			  <ww:textfield id="sourceName" name="dto.bug.dtoHelper.bugSource.typeName" cssClass="text_c" cssStyle="width:160;padding:2 0 0 4;" readonly='true'  onblur="javascript:this.readonly='true';" onfocus="popSelWin('sourceId','sourceName','sourceSelStr','来源')"></ww:textfield>
	 		     </td>
	 		     <td width="80" class="rightM_center" align="right" style="color:red;border-right:0">发现时机:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			  <ww:hidden id="bugOccaId" name="dto.bug.bugOccaId"></ww:hidden>
	 			  <ww:textfield id="occaName" name="dto.bug.dtoHelper.bugOpotunity.typeName" cssClass="text_c" cssStyle="width:160;padding:2 0 0 4;" readonly='true'  onblur="javascript:this.readonly='true';" onfocus="popSelWin('bugOccaId','occaName','occaSelStr','发现时机')"></ww:textfield>
	 		     </td>	
	 		     <ww:if test="${dto.bug.geneCauseId}==null||${dto.bug.geneCauseId}==''">
		 		     <td width="80" id="geneCauseTd" class="rightM_center" align="right" style="color:red;border-right:0">测试时机:</td>
		 			 <td  class="tdtxt" id="geneCauseF" width="160" style="padding:2 0 0 4;border-right:0">
		 			 <ww:hidden id="geneCauseId" name="dto.bug.geneCauseId"></ww:hidden>
		 			 <ww:textfield id="geneCaseName" name="dto.bug.dtoHelper.geneCause.typeName" cssClass="text_c" cssStyle="width:160;padding:2 0 0 4;"  readonly='true' onblur="javascript:this.readonly='true';" onfocus="popSelWin('geneCauseId','geneCaseName','geCaseSelStr','测试时机')"></ww:textfield>
		 		     </td>	
	 		      </ww:if>	
	 		       <ww:else>
		 		     <td width="80"  class="rightM_center" align="right" style="color:red;border-right:0" >
		 		      <div id="geneCauseTd" style="display: none">
		 		        测试时机:
		 		       </div>
		 		       </td>
		 			 <td  class="tdtxt" id="geneCauseF"  width="160" style="padding:2 0 0 4;display: none;border-right:0">
		 			 <ww:hidden id="geneCauseId" name="dto.bug.geneCauseId"></ww:hidden>
		 			   <ww:textfield id="geneCaseName" name="dto.bug.dtoHelper.geneCause.typeName" cssClass="text_c" cssStyle="width:160;padding:2 0 0 4;" readonly='true' onfocus="popSelWin('geneCauseId','geneCaseName','geCaseSelStr','测试时机')"></ww:textfield>
		 		     </td>		 		       
	 		       </ww:else>	   
			   </tr>
			   <tr>
	 		     <td width="80" class="rightM_center" align="right" style="color:red;border-right:0">优先级:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			 <ww:hidden id="priId" name="dto.bug.priId"></ww:hidden>
	 			  <ww:textfield id="priName" name="dto.bug.dtoHelper.bugPri.typeName" cssClass="text_c" cssStyle="width:160;padding:2 0 0 4;"  readonly='true' onblur="javascript:this.readonly='true';" onfocus="popSelWin('priId','priName','priSelStr','优先级')"></ww:textfield>
	 		     </td>	
	 		     <td width="80" class="rightM_center" align="right" style="color:red;border-right:0">频率:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			 <ww:hidden id="bugFreqId" name="dto.bug.bugFreqId"></ww:hidden>
	 			  <ww:textfield id="bugFreqName" name="dto.bug.dtoHelper.bugFreq.typeName" cssClass="text_c" cssStyle="width:160;padding:2 0 0 4;"  readonly='true' onblur="javascript:this.readonly='true';" onfocus="popSelWin('bugFreqId','bugFreqName','freqSelStr','频率')"></ww:textfield>
	 		     </td>	
	 		     
	 		     <ww:if test="${dto.bug.reproPersent}!=null||${dto.bug.reproPersent}!=''">
	 		     	 <td width="80" id="repropTd" class="rightM_center" align="right"style="color:red;border-right:0" >再现比例:</td>
		 		      <td  id="reprop"class="tdtxt" align="left"  style="width:160;padding:2 0 0 4;border-right:0">
		 		      <ww:textfield id="reproPersent" name="dto.bug.reproPersent" cssClass="text_c" cssStyle="width:160;padding:2 0 0 4;" ></ww:textfield>
				      </td>	
			      </ww:if>
			      <ww:else>
			      <td width="80" class="rightM_center" align="right" style="color:red;border-right:0">
			        <div id="repropTd"  style="display:none;"> 
			         再现比例:
			      </td>
			      <td class="tdtxt" align="left"  style="width:160;padding:2 0 0 4;border-right:0">
			      	<div id="reprop"  style="display:none;"> 
			      	 <ww:textfield id="reproPersent" name="dto.bug.reproPersent" cssClass="text_c" cssStyle="width:160;padding:2 0 0 4;" ></ww:textfield>
			       </div>
			      </td>
			      </ww:else>  
			   </tr>
			   <tr id="impPhaTr">
	 		     <ww:if test="${dto.bug.genePhaseId}!=null||${dto.bug.genePhaseId}!=''">
		 		     <td id="impPhaTdtxt" width="80" class="rightM_center" align="right" style="color:red;border-right:0" >引入原因:</td>
		 			 <td id="impPhaTd" class="tdtxt" colspan="5" width="640" style="padding:2 0 0 4;border-right:0">
		 			 <ww:hidden id="genePhaseId" name="dto.bug.genePhaseId"></ww:hidden>
		 			 <ww:property value="dto.bug.dtoHelper.importPhase.typeName"/>
		 		     </td>	
		 		     <td colspan="4"></td>	 		     
	 		     </ww:if>
	 		     <ww:else>
		 		     <td width="80" class="rightM_center" align="right" style="border-right:0" >
		 		        <div   id="impPhaTdtxt" style="display:none"> 引入原因:</div>
		 		     </td>
		 			 <td  class="tdtxt" colspan="5" width="640" style="padding:2 0 0 4;color:red;border-right:0">
		 			 <ww:hidden id="genePhaseId" name="dto.bug.genePhaseId"></ww:hidden>
		 			 <div  id="impPhaTd" style="display:none">
		 			   <ww:property value="dto.bug.dtoHelper.importPhase.typeName"/>
		 			 </div>
		 		     </td>	
		 		     <td colspan="4"></td> 		     
	 		     </ww:else>
			   </tr>
			   <tr>
   			    <td width="80" class="rightM_center" style="color:red;border-right:0" nowrap>再现步骤:</td>
   			    <td width="640" class="tdtxt" colspan="5" style="background-color: #f7f7f7; hight:200;width:640;border-right:0">
   			      <textarea name="dto.bug.reProStep" id="reProStep" cols="50" rows="30" Class="text_c" readonly='true' style="width:640;hight:200;padding:2 0 0 4;">
   			      </textarea>
   			    </td>			   
			   </tr>
			   <c:if test="${fn:indexOf(dto.currOwner,'处理人')<0}">
				 <tr id="intecsOwnerTr" >
	 		       <td width="80" class="rightM_center" align="right" style="padding:2 0 0 4;border-right:0">
	 			   <ww:property value="dto.currOwner" escape="false"/>
	 		     </td>	
	 		     <td colspan="4"></td>
				</tr>
				</c:if>
			   <tr>
			   <td  class="rightM_center" align="right" width="80" style="border-right:0">发现日期:</td>
			   <td  class="tdtxt" align="left" width="160" style="width:160;padding:2 0 0 4;border-right:0" >
			   <fmt:formatDate value="${dto.bug.reptDate}" pattern="yyyy-MM-dd HH:mm:ss" />
			   </td>
			   <td  class="rightM_center" align="right"" width="80" style="border-right:0">发现版本:</td>
			   <td  class="tdtxt" align="left" width="160"style="width:160;padding:2 0 0 4;border-right:0">
			   <ww:hidden id="bugReptVer" name="dto.bug.bugReptVer"></ww:hidden>
							<ww:textfield id="bugReptVerName" name="dto.bug.reptVersion.versionNum"
								cssStyle="width:160;padding:2 0 0 4;" cssClass="text_c" readonly="true"
								onfocus="popSelWin('bugReptVer','bugReptVerName','verSelStr','发现版本')"></ww:textfield>
			   </td>
			   <c:if test="${not empty dto.bug.versionLable}">
			   <td class="rightM_center" style="border-right:0" align="right" width="80">
			   <ww:property value="dto.bug.versionLable"/>:
			   </td>
			   <td  class="tdtxt" align="left" width="160"style="width:160;padding:2 0 0 4;border-right:0">
			     <ww:property value="dto.bug.currVersion.versionNum" />
			   </td>
			   </c:if>
			   <c:if test="${ empty dto.bug.versionLable}">
			   <td class="rightM_center" style="border-right:0" align="right" width="80">
			   </td>
			   <td  class="tdtxt" align="left" width="160"style="width:160;padding:2 0 0 4;border-right:0">
			   </td>			   
			   </c:if>
			   </tr>
			   <tr id="reamrkTr">
			      <td width="90" class="rightM_center"  align="right" style="border-right:0">备注:</td>
			      <td  class="tdtxt"  align="left" colspan="5"  width="630" style="padding:2 0 0 4;border-right:0">
    			     <ww:textfield id="currRemark" cssClass="text_c" readonly="true" name="dto.bug.currRemark" cssStyle="width:630;padding:2 0 0 4;" ></ww:textfield>
    			  </td>
			    </tr>
				</ww:form>
			    <ww:form enctype="multipart/form-data" theme="simple" name="fileform" id="fileform" action="" method="post" target="target_upload">
				  <tr class="odd_mypm">
				  	 <td class="rightM_center" width="90" id="attachTd" style="border-right:0">
				  	 附件/插图片:
				  	 </td>
				    <td class="dataM_left"  colspan="5" width="630" style="border-right:0">
						<input name="currUpFile" id="currUpFile" type="file" style="padding:2 0 0 4;width:210" Class="text_c">
						<c:if test="${not empty dto.bug.attachUrl}">
						<img src="<%=request.getContextPath()%>/images/button/attach.gif" id="currAttach" alt="当前附件" title="${dto.bug.attachUrl2}" onclick="openAtta()" />
						</c:if>
						<img src="<%=request.getContextPath()%>/dhtmlx/toolbar/images/img_insert.png" id="insertImg" alt="当前位置插入图片" title="当前位置插入图片" onclick="upLoadAndSub('upSub','upSubCheck',1,oEditor);" />
						<img src="" id="currAttach" alt="" title="" style="display:none" />
				    </td>
				  </tr>		
				  <tr class="ev_mypm">
				    <td width="90" align="right"  style="border-right:0">&nbsp;</td>
					<td width="630" style="width:200;padding:2 0 0 4;border-right:0" colspan="5"><div id="upStatusBar"></div></td>		  
				  </tr>	
			    </ww:form>	
 		  	      <iframe id="target_upload" name="target_upload" src="" frameborder="0" scrolling="no" width="0" height="0"></iframe>
			   <tr>
				<td  class="tdtxt" align="center" width="720" colspan="6" style="border-right:0">
				<div class="buttonwrapper">
				    <a class="bluebtn" href="javascript:eraseAttach('eraseAllImg');closeMe();" style="margin-left: 6px"><span> 返回</span></a>
				    <a class="bluebtn" href="javascript:void(0);" id="saveBtn" onclick="upLoadAndSub('upSub','upSubCheck',0,oEditor);" style="margin-left: 6px"><span> 确定</span></a>
				    <a class="bluebtn" href="javascript:void(0);" id="reSetAttaBtn" onclick="$('currUpFile').value='';if(_isIE)$('currUpFile').outerHTML=$('currUpFile').outerHTML" ><span>取消选取的文件</span> </a>
    			 </div>
    			 </td> 	   
			   </tr>
			 </table>
		 </div>
	  </div>
	  </td></tr>
	  <tr><td>
	   <div id="flowInfoDiv" align="center"  style="background-color: #ffffff;display:none; width:100%;">
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top">
					<div id="toolbarObj1"></div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<div id="gridbox1" ></div>
				</td>
			</tr>
		</table>  
	  </div></td></tr>
	  </table>
	  <ww:include value="/jsp/common/dialog.jsp"></ww:include>
	  <ww:include value="/jsp/common/downLoad.jsp"></ww:include>
	<script type="text/javascript">
	if($("impPhaTd").style.display=="none")
		$("impPhaTr").style.display="none";
	adjustTable("baseTable","odd");
	pmGrid = new dhtmlXGridObject('gridbox1');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
    pmGrid.setHeader("&nbsp;,序号,处理人,处理结果,处理说明,处理日期");
    pmGrid.setInitWidths("0,40,80,260,*,80");
    pmGrid.setColAlign("center,center,left,left,left,left");
    pmGrid.setColTypes("ra,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,str,str,str,str,str");
	pmGrid.enableAutoHeight(true,500);
    pmGrid.init();
    pmGrid.enableTooltips("false,true,true,true,true,true");
    pmGrid.setSkin("light");  
	flwBar = new dhtmlXToolbarObject("toolbarObj1");
	flwBar.setIconsPath(conextPath+"/dhtmlx/toolbar/images/");
	flwBar.addButton("first", 1, "", "first.gif", "first.gif");
	flwBar.setItemToolTip("first", "第一页");
	flwBar.addButton("pervious",2, "", "pervious.gif", "pervious.gif");
	flwBar.setItemToolTip("pervious", "上一页");
	flwBar.addSlider("slider",3, 80, 1, 30, 1, "", "", "%v");
	flwBar.setItemToolTip("slider", "滚动条翻页");
	flwBar.addButton("next",4, "", "next.gif", "next.gif");
	flwBar.setItemToolTip("next", "下一页");
	flwBar.addButton("last",5, "", "last.gif", "last.gif");
	flwBar.setItemToolTip("last", "末页");
	flwBar.addInput("page",6, "", 25);
	flwBar.addText("pageMessage",7, "");
	flwBar.addText("pageSizeText",8, "每页");
	flwBar.addText("pageSizeTextEnd",9, "20条");
	 </script>
	<script type="text/javascript"src="<%=request.getContextPath()%>/jsp/bugManager/history.js"></script>
	<script type="text/javascript"src="<%=request.getContextPath()%>/jsp/bugManager/edit.js"></script>
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
  </BODY>
	<script type="text/javascript">
		importJs(conextPath+"/jsp/common/upload.js");
		var reProStepTem = recovJsonKeyWord(reProStep);
		//initImg = getAttaInRichHtm(reProStepTem);
		initImg = getAttaInRichHtm($("initReProStep").value);
		parent.editW_ch.button("close").attachEvent("onClick", function(){
			eraseAttach('eraseAllImg');
			parent.editW_ch.setModal(false);
			parent.editW_ch.hide();
		});
		if($("geneCauseId").value!=""){
			$("geneCauseTd").style.display="";
			$("geneCauseF").style.display="";
		}
	</script>
</HTML>
