	function pageAction(pageNo, pSize){
		if(pageNo>pageCount){
			pmBar.setValue("page", pageNo);
			return ;
		}
		purl = conextPath+"/bugManager/relaCaseAction!loadRelaCase.action?dto.isAjax=true&dto.moduleId="+$("moduleIdTmp").value+"&dto.bugId="+$("bugId").value +"&dto.pageNo="+ pageNo+"&dto.pageSize=" + pSize;
		var ajaxRest = postSub(purl,"");
		if(ajaxRest=="failed"){
			hintMsg("加载数据发生错误");
		}
		$("listStr").value=ajaxRest;
		colTypeReset();
		initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");	
		loadLink();
   		return;
	}		
	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		pageAction(pageNo, pageSize);
		return;
	});
	pmBar.attachEvent("onEnter", function(id, value) {
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
		return;
	});
	pmBar.attachEvent("onClick", function(id) {
		if(id == "new"){
			detalViewInit('add');
		}else if(id == "first"){
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}else if(id == "last"){
			pageNo = pageCount;
			pageAction(pageNo, pageSize);
		}else if(id == "next"){
			pageNo = pageNo +1
			pageAction(pageNo, pageSize);
		}else if(id == "pervious"){
			pageNo = pageNo -1
			pageAction(pageNo, pageSize);
		}else if(id == "id1" || id == "id2" || id == "id3"){
			var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
			if(pageSize==pageSizeTemp){
				return;
			}
			pageSize = pageSizeTemp;
			pageAction(pageNo, pageSize);
		}
	});

	function initCufmsW(){
		cufmsW = new dhtmlXWindows();
		cufmsW.enableAutoViewport(true);
		cufmsW.setViewport(50, 50, 100, 100);
		cufmsW.setImagePath(conextPath+"/dhtmlx/windows/codebase/imgs/");
		return cufmsW;
	} 
	function initW_ch(obj, divId, mode, w, h,wId){
		if((typeof obj != "undefined")&& !obj.isHidden()){
			obj.setDimension(w, h);
			obj.centerOnScreen();
			return obj;
		}else if((typeof obj != "undefined") && obj.isHidden()){
			obj.show();
			obj.bringToTop();
			obj.centerOnScreen();
		}else{
			if(typeof cufmsW == "undefined"){
				cufmsW = initCufmsW();
			}
			if(typeof wId != "undefined" && wId!=""){
				obj = cufmsW.createWindow(wId, 110, 110, w, h);
			}else{
				obj = cufmsW.createWindow(divId, 110, 110, w, h);
			}
			if(divId != null&&divId !=""){
				obj.attachObject(divId, false);
			}
			obj.centerOnScreen();
			hiddenB(obj, mode);
		}
		obj.setModal(mode);
		return obj;
	}
	function setBaseInfo(baseInfo){
		var updInfos = baseInfo.split("^");
		for(var i=0; i < updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] != "null"){
				currInfo[1] =recovJsonKeyWord(currInfo[1]);
				var valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";;
				eval(valueStr);
				if(currInfo[0]=="bugAntimodDate"&&currInfo[1]!=""){
					$("deadLineDateTr").style.display="";
				}else if(currInfo[0]=="geneCaseName"&&currInfo[1]!=""){
					$("geneCauseTxtTd").style.display="";
					$("geneCauseTd").style.display="";
				}else if(currInfo[0]=="reproPersent"&&currInfo[1]!=""){
					$("repropTxtTd").style.display="";
					$("repropTd").style.display="";
				}else if(currInfo[0]=="imphaName"&&currInfo[1]!=""){
					$("impPhaTdtxt").style.display="";
					$("impPhaTd").style.display="";
					$("impPhaTr").style.display="";
					parent.relCaseW_ch.setDimension(850, 530);
				}else if(currInfo[0]=="currRemark"&&currInfo[1]!=""){
					$("remarkTr").style.display="";
				}else if(currInfo[0]=="reProStep"){
					reProStep=currInfo[1];
				}else if(currInfo[0]=="attachUrl"&&currInfo[1]!=""){
					$("currAttach2").style.display="";
					$("currAttach2").title=currInfo[1].substring(currInfo[1].indexOf("_")+1);			
				}else if(currInfo[0]=="versionLable"&&currInfo[1]!=""){
					$("verLableTd").innerHTML=currInfo[1];
				}else if(currInfo[0]=="currVer"&&currInfo[1]!=""){
					$("verValTd").innerHTML=currInfo[1];
				}			
			}
		}
	}
	function detalViewInit(optFlag){
		var url = conextPath+"/caseManager/caseManagerAction!viewDetal.action?dto.isAjax=true&dto.testCaseInfo.testCaseId="+pmGrid.getSelectedId();
		dW_ch = initW_ch(dW_ch,  "createDiv", true,700, 430);
		dW_ch.button("close").attachEvent("onClick", function(){
			eraseAttach('eraseAllImg');
			dW_ch.setModal(false);
			dW_ch.hide();
		});
		upVarReset();
		if(optFlag=="add"){
			viewDetalSw(false);
			dW_ch.setText("增加用例");
			url = conextPath+"/bugManager/relaCaseAction!addCaseInit.action?dto.isAjax=true&dto.moduleId="+$("moduleIdTmp").value+"&dto.mdPath="+$("mdPath").value ;	
			$("insertImg").style.display="";
			$("attachTd").innerHTML="附件/插图片:";
			$("currUpFile").style.display="";		
		}else{
			viewDetalSw(true);
			dW_ch.setText("用例明细");
			$("insertImg").style.display="none";
			$("attachTd").innerHTML="附件:";	
			$("currUpFile").style.display="none";
		}
		$('createForm').reset();
		var ajaxResut = postSub(url,"");
		if(ajaxResut=="failed"){
			hintMsg('初始化失败');
			return;
		}
		setUpInfo(ajaxResut);
		adjustTable("createTable");
		dW_ch.bringToTop();
		loadCaseFCK();
	}
	function viewDetalSw(detal){
		$("testCaseDes").readOnly=detal;
		$("expResult").readOnly=detal;
		$("weight").readOnly=detal;
		$("priId").disabled =detal;
		$("caseTypeId").disabled=detal;
		if(detal){
			$("saveBtn").style.display="none";
		}else{
			$("saveBtn").style.display="";
		}
	}
	function setUpInfo(updInfo){
		if(updInfo==""){
			return;
		}
		var updInfos = updInfo.split("^");
		for(var i=0; i < updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] != "null"){
				var valueStr = "";
				currInfo[1] =recovJsonKeyWord(currInfo[1]);
				if(currInfo[0]=="typeSelStr"||currInfo[0]=="priSelStr"){
					var selStr = currInfo[1];
					var selId="priId";
					if(currInfo[0]=="typeSelStr"){
						selId="caseTypeId";
					}
					loadSel(selId,selStr);	
				}else{
					valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";
					eval(valueStr);	
					if(currInfo[0]=="expResult"){
						$("expResultOld").value=currInfo[1];
					}else if(currInfo[0]=="operDataRichText"){
						$("initOpd").value=currInfo[1];
						initImg = getAttaInRichHtm(currInfo[1]);
					}else if(currInfo[0]=="attachUrl"&&currInfo[1]!=""){
						$("currAttach").style.display="";
						$("currAttach").title="附件";
						//$("currAttach").title=currInfo[1].substring(currInfo[1].indexOf("_")+1);			
					}else if(currInfo[0]=="versionLable"&&currInfo[1]!=""){
						$("currVerLableDiv").style.display="";
						$("currVerLableDiv").innerHTML=	currInfo[1];
						$("currVerValDiv").style.display="";		
					}				
				}
			}
		}
	}
	function loadSel(selId,selStr,splStr){
		if(selStr==""){
			return;
		}
		$(selId).options.length = 1;
		var options ;
		if(splStr){
			options = selStr.split(splStr);
		}else{
			options = selStr.split("$");
		}
		for(var i = 0; i < options.length; i++){
			var optionArr = options[i].split(";");
			if(optionArr[0] != "")
				var selvalue = optionArr[0] ;
				var selable = optionArr[1];
				$(selId).options.add(new Option(selable,selvalue));
		}	
	}
	function saveCase(){
		if(subChk()){
			$("testData").value=html2PlainText("operDataRichText");
			var testDataV = $("testData").value;
			var mdPath = $("mdPath").value+":";
			if(mdPath.replace(/\s+$|^\s+/g,"")==testDataV.replace(/\s+$|^\s+/g,"")){
				hintMsg("请填写过程及数据");
				return;
			}
			if("可在过程及数据中用表格形式写预期结果,或单独在此填写"==$("expResult").value){
				$("expResult").value="";
			}
			var url = conextPath+"/caseManager/caseManagerAction!addCase.action?dto.isAjax=true";	
			var ajaxResut = postSub(url,"createForm");
			if(ajaxResut.indexOf("^") != -1){
				ajaxResut = ajaxResut.replace(/[\r\n]/g, "");
				var result = ajaxResut.split("^");
				if(result[0] == "success"){
					eraseAttach();
					colTypeReset();
					var rowDatas = result[2].split(",");
					var rowData = rowDatas[0]+","+rowDatas[1] +","+rowDatas[2]+","+rowDatas[5]+","+rowDatas[6]+","+rowDatas[7];
					pmGrid.addRow(result[1],rowData,0);
					//setRowNum(pmGrid);
					loadLink();
					pmGrid.setSelectedRow(pmGrid.getRowId(0));	
					pmGrid.setSizes();		
					$("createForm").reset();
					$("expResult").value="";
					dW_ch.setModal(false);
					dW_ch.hide();
					upVarReset();
					return;
				}else if(result[0]=="failed"&&ajaxResut.indexOf("^")>0){
					hintMsg(result[1]);
					return;	
				}else{
					hintMsg("操作失败");
					return;	
				}
			}
			hintMsg("操作失败");
			return;	
		}
	}
	function subChk(){
		if($("caseTypeId").value=="-1"){
			hintMsg("请选择用例类型");
			return false;
		}else if($("priId").value=="-1"){
			hintMsg("请选择用例优先级");
			return false;
		}else if(isAllNull($("testCaseDes").value)){
			hintMsg("请填写用例描述");
			return false;
		}else if(checkIsOverLong($("testCaseDes").value,60)==true){
			hintMsg("用例描述不能超过60个字符");
			return false;
		}else if(!isAllNull($("expResult").value)&&checkIsOverLong($("expResult").value,400)){
			hintMsg("预期结果不能超过400个字符");
			return false;
		}else if(caseEditor.EditorDocument.body.innerHTML.trim()=="<br>"||caseEditor.EditorDocument.body.innerHTML.trim()==""){
			hintMsg("请填写过程及数据");
			return false;		
		}else if(caseEditor.EditorDocument.body.innerHTML.trim()!=""){
			$("operDataRichText").value=caseEditor.EditorDocument.body.innerHTML.trim();
			var endStr = "";
			if($("operDataRichText").value.endWith("</strong>")){
				$("operDataRichText").value=$("operDataRichText").value.trim("</strong>");
				endStr = "</strong>";
			}
			if($("operDataRichText").value.endWith("</b>")){
				$("operDataRichText").value=$("operDataRichText").value.trim("</b>");
				endStr = "</b>";
			}
			$("operDataRichText").value = $("operDataRichText").value.replace(/\s+$|^\s+/g,"");
			$("operDataRichText").value = $("operDataRichText").value.trim("&nbsp;");
			for(var i=1; i<31;i++){
				$("operDataRichText").value = $("operDataRichText").value.trim("<br>");
				$("operDataRichText").value = $("operDataRichText").value.trim("&nbsp;");	
				$("operDataRichText").value = $("operDataRichText").value.replace(/\s+$|^\s+/g,"");		
			}
			$("operDataRichText").value +=endStr;
			if(checkIsOverLong($("operDataRichText").value,1000)){
				hintMsg("过程及数据不能超过1000个字符");
				return false;				
			}	
		}
		return true;
	}
	function relaCase(){
		if(!canExe){
			hintMsg("您没有执行用例的权限");
			return;		
		}	
		if($("testCaseIds").value.replace(/\s+$|^\s+/g,"")==""&&$("saveRela").style.display==""){
			hintMsg("请选择要关联的用例");
			return;
		}
		if($("testCaseIds").value==testCaseIds&&$("upRela").style.display==""){
			parent.relCaseW_ch.setModal(false);
			parent.relCaseW_ch.hide();
			return;		
		}
		var url= conextPath+"/bugManager/relaCaseAction!bugRelaCase.action?dto.bugId="+$("bugId").value
		url+="&dto.moduleId="+$("moduleIdTmp").value+"&dto.testCaseIds="+$("testCaseIds").value +"&dto.bugReptVer="+$("bugReptVer").value;
		var ajaxResut = postSub(url,"");
		if(ajaxResut=="failed"){
			hintMsg("关联用例发生错误"); 
			return;
		}
		if($("testCaseIds").value.replace(/\s+$|^\s+/g,"")==""){
			parent.reSetLinkCol(1,"green");
		}else{
			parent.reSetLinkCol(1);
		}
		parent.relCaseW_ch.setModal(false);
		parent.relCaseW_ch.hide();
	}

