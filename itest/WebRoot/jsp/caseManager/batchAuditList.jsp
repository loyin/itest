<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
	<head>
		<title>测试用例</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page.css'>
	</head>
	<body bgcolor="#ffffff" style="overflow-x:hidden;">
	<ww:include value="/jsp/common/dialog.jsp"></ww:include>
	  <ww:hidden id="currNodeId" name="dto.currNodeId"></ww:hidden>
	  <input type="hidden" id="listStr" name="listStr" value="${dto.listStr}"/>
	  <input type="hidden" id="initOpd" name="initOpd"/>
	  <ww:hidden id="isReview" name="dto.isReview"></ww:hidden>
	  <ww:hidden id="isTestLeader" name="dto.isTestLeader"></ww:hidden>
	  <ww:hidden id="customCaseHomeTaskId" name="dto.taskId" ></ww:hidden>
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top" ><div id="toolbarObj"></div></td>
			</tr>
			<tr>
				<td valign="top"><div id="gridbox"></div></td>
			</tr>
			<tr>
				<td valign="top"><div id="countStr" style="color: Blue;font-size:9pt">${dto.countStr}</div></td>
			</tr>
		</table>
	<script type="text/javascript">
	var taskOpts = Array(Array('00000000000000000000000000000000', 'obj', '请选择任务'));
	var myName = "${session.currentUser.userInfo.name}";
	var ontLineState = "${dto.outLineState}";
	ininPage("toolbarObj", "gridbox", 555);
	pageBreakUrl = conextPath+'/caseManager/caseManagerAction!batchAuditInit.action';
	var pageSizec = 0;
	pmBar = new dhtmlXToolbarObject("toolbarObj");
	pmBar.setIconsPath(conextPath+"/dhtmlx/toolbar/images/");
	pmBar.addButton("allChk", 0, "", "ch_false.gif");
	pmBar.setItemToolTip("allChk", "全选");
	//pmBar.addButton("ldTree",1 , "", "tree.gif");
	//pmBar.setItemToolTip("ldTree", "关闭功能分解窗口");
	pmBar.addButton("loadCaseBoard",2 , "", "chart.gif");
	pmBar.setItemToolTip("loadCaseBoard", "测试人员工作面板");
	pmBar.addButton("adtPass",3 , "", "enable.png");
	pmBar.setItemToolTip("adtPass", "提交测试");
	pmBar.addButton("adtFailed",4 , "", "disable.png");
	pmBar.setItemToolTip("adtFailed", "打回修正");
	pmBar.addButton("expCase",5 , "", "xls.gif");
	pmBar.setItemToolTip("expCase", "导出");
	pmBar.addButton("find",6 , "", "search.gif");
	pmBar.setItemToolTip("find", "查询");
	pmBar.addButton("reFreshP",7 , "", "page_refresh.gif");
	pmBar.setItemToolTip("reFreshP", "刷新页面");
	pmBar.addButton("back",8 , "", "back.gif");
	pmBar.setItemToolTip("back", "返回");
	pmBar.addButton("first",9 , "", "first.gif", "first.gif");
	pmBar.setItemToolTip("first", "第一页");
	pmBar.addButton("pervious",10, "", "pervious.gif", "pervious.gif");
	pmBar.setItemToolTip("pervious", "上一页");
	pmBar.addSlider("slider",11, 80, 1, 30, 1, "", "", "%v");
	pmBar.setItemToolTip("slider", "滚动条翻页");
	pmBar.addButton("next",12, "", "next.gif", "next.gif");
	pmBar.setItemToolTip("next", "下一页");
	pmBar.addButton("last",13, "", "last.gif", "last.gif");
	pmBar.setItemToolTip("last", "末页");
	pmBar.addInput("page",14, "", 25);
	pmBar.addText("pageMessage",15, "");
	pmBar.addText("pageSizeText",16, "每页");
	var opts = Array(Array('id1', 'obj', '10'), Array('id2', 'obj', '15'), Array('id3', 'obj', '20'), Array('id4', 'obj', '25'));
	pmBar.addButtonSelect("pageP",17, "page", opts);
	pmBar.setItemToolTip("pageP", "每页记录数");
	pmBar.addText("pageSizeTextEnd",18, "条");
	pmBar.setListOptionSelected('pageP','id1');
	pmBar.addSeparator("sep_swTask3",19); 
	pmBar.addButton("sw2Task",20, "切换项目", "");
	pmBar.addText("qSearch",22, "编号快查");
	pmBar.addInput("qSearchIpt",22, "", 80);
	pmBar.addSeparator("sep_swTask2",23); 
	pmBar.addButton("currPosition",24, "批量审批测试用例", "hand.gif", "hand.gif"); 
	pmBar.attachEvent("onClick", function(id) {
		if(id =="reFreshP"){
	   		pageAction(pageNo, pageSize);
		}
	});
	var caseBoard_wh;
	pmBar.attachEvent("onClick", function(id) {
		if(id =="back"){
	   		var reUrl= document.referrer;
	   		if(reUrl!=""&&(reUrl.indexOf('main.jsp')>0||reUrl.indexOf('myHome.jsp')>0)){
	   			history.back();
	   			return;
	   		}
	   		window.location=reUrl;
		}else if(id=="loadCaseBoard"){
			var url = conextPath+ "/caseManager/caseManagerAction!loadCaseBoard.action";
			caseBoard_wh = initW_ch(caseBoard_wh, "", true, 630, 380,'caseBoard_wh');
			caseBoard_wh.attachURL(url);
			caseBoard_wh.setText("测试人员工作面板---点击待处理BUG数目查看相关待处理BUG");	
		    caseBoard_wh.show();
		    caseBoard_wh.bringToTop();
		    caseBoard_wh.setModal(true);
		}
	});

	pmBar.attachEvent("onClick", function(id) {
		if(id=="custHome"){
			dhtmlxAjax.post(conextPath+"/commonAction!setCaseAsHome.action");
			parent.setCheckboxUnchk('caseHome');
		}else if(id=="sw2Task"){
			openSwTaskList();
		}
	});
	var treeW_ch,treeWin,swWin;
	function loadTree(showMe){
		if($("customCaseHomeTaskId").value==""){
			openSwTaskList();
			return;
		}
		if(typeof treeW_ch == "undefined"){
			treeW_ch = initW_ch(treeW_ch, "", false, 300, 470,'treeW_ch');
			var url = conextPath+"/caseManager/caseManagerAction!loadTree.action";
			treeW_ch.attachURL(url);
			treeW_ch.setText("需求分解树");	
			treeW_ch.button("close").attachEvent("onClick", function(){
				pmBar.setItemToolTip("ldTree", "显示需求分解窗口");
				treeW_ch.hide();
			});
		treeW_ch.show();
		treeW_ch.bringToTop();
		pmBar.setItemToolTip("ldTree", "关闭需求分解窗口");
		return;
		}
		if(treeW_ch.isHidden()){
			treeW_ch.show();
			treeW_ch.bringToTop();
			pmBar.setItemToolTip("ldTree", "关闭需求分解窗口");	
		}else if(typeof(showMe)=="undefined"){
			pmBar.setItemToolTip("ldTree", "显示需求分解窗口");
			treeW_ch.hide();		
		}
	}
	function openSwTaskList(){
		if(typeof(swWin)!="undefined" &&swWin.isHidden()){
	    	swWin.show();
	    	swWin.bringToTop();
	    	swWin.setModal(true);
	    	return;			
		}
		swWin = initW_ch(swWin, "", true, 890, 500,'swWin');
		swWin.attachURL(conextPath+"/singleTestTask/singleTestTaskAction!swTestTaskList.action?dto.operCmd=caseList");
		swWin.setText("请选择测试项目");	
    	swWin.show();
    	swWin.bringToTop();
    	swWin.setModal(true);			
	}
	if(ontLineState!="1"&&$("customCaseHomeTaskId").value!=""){
		openSwTaskList();
		hintMsg("所选|当前项目未提交测试需求,请切换到其他项目");	
	}
	</script>
	<script type="text/javascript"src="<%=request.getContextPath()%>/jsp/caseManager/batchAuditInitList.js"></script>
		<div id="createDiv" class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="createForm" enctype="multipart/form-data"
				name="createForm" namespace="" action="">
				<input type="hidden" id="mdPath" name="mdPath" value="" />
				<ww:hidden id="taskId" name="dto.testCaseInfo.taskId" value="${dto.taskId}"></ww:hidden>
			    <ww:hidden id="moduleId" name="dto.testCaseInfo.moduleId"></ww:hidden>
			    <ww:hidden id="createrId" name="dto.testCaseInfo.createrId"></ww:hidden>
			    <ww:hidden id="testCaseId" name="dto.testCaseInfo.testCaseId"></ww:hidden>
			    <ww:hidden id="isReleased" name="dto.testCaseInfo.isReleased"></ww:hidden>
				<ww:hidden id="creatdate" name="dto.testCaseInfo.creatdate"></ww:hidden>
				<ww:hidden id="attachUrl" name="dto.testCaseInfo.attachUrl"></ww:hidden>
				<ww:hidden id="auditId" name="dto.testCaseInfo.auditId"></ww:hidden>
			    <ww:hidden id="testStatus" name="dto.testCaseInfo.testStatus"></ww:hidden>
			    <ww:hidden id="testData" name="dto.testCaseInfo.testData"></ww:hidden>
			    <ww:hidden id="moduleNum" name="dto.testCaseInfo.moduleNum"></ww:hidden>
			    <ww:hidden id="expResultOld" ></ww:hidden>
    			<table class="obj row20px" cellspacing="0" id="createTable" cellpadding="0" border="0" width="100%">
    			  <tr class="ev_mypm">
    			    <td colspan="5" class="tdtxt" align="center">
    			      <div id="cUMTxt" align="center" style="color: Blue; padding: 2px"></div>
    			    </td>
    			  </tr>
    			  <tr class="odd_mypm">
    			    <td width="50" class="rightM_center" style="color:red;">类型:</td>
    			    <td  class="dataM_left" width="75">
    			      <select id="caseTypeId" name="dto.testCaseInfo.caseTypeId"  Class="text_c"style="width:75" >
    			       <option   value="-1">请选择</option> 	        
    			      </select>
    			    </td>
    			    <td  width="50" class="rightM_center" style="color:red;" nowrap>优先级:</td>
    			    <td  class="dataM_left" width="75">
    			      <select id="priId" name="dto.testCaseInfo.priId" style="width: 75"  Class="text_c">
    			       <option   value="-1">请选择</option> 
    			      </select>
    			    </td>
    			    <td  class="rightM_center" nowrap>&nbsp;执行成本:
    			       <ww:textfield id="weight" name="dto.testCaseInfo.weight"cssStyle="width:70;padding:2 0 0 4;"  cssClass="text_c" onkeypress="javascript:return numChk(event);"></ww:textfield>
    			       <font color="Blue">一个成本单位代表5分钟</font> 
    			    </td>
    			  </tr>
    			  <tr class="ev_mypm">
    			    <td width="90" class="rightM_center" style="color:red;" >用例描述:</td>
    			    <td  class="dataM_left" colspan="4">
    			      <ww:textfield id="testCaseDes" cssClass="text_c" name="dto.testCaseInfo.testCaseDes" cssStyle="width:560;padding:2 0 0 4;" ></ww:textfield>
    			    </td>
    			  </tr>
    			  <tr class="odd_mypm">
    			    <td width="90" class="rightM_center" style="color:red;" >过程及数据:</td>
    			    <td width="570" class="dataM_left"  Class="text_c" colspan="4" style="background-color: #f7f7f7;">
    			      <textarea name="dto.testCaseInfo.operDataRichText" id="operDataRichText" cols="50" rows="15"  style="width:570;padding:2 0 0 4;">
    			        <ww:property value="dto.testCaseInfo.operDataRichText" escape="false"/>
    			      </textarea>
    			    </td>
    			  </tr>
    			  <tr class="ev_mypm">
    			    <td width="90" class="rightM_center" align="right">预期结果:</td>
    			    <td  class="dataM_left" colspan="4">
    			      <textarea id="expResult"  Class="text_c" name="dto.testCaseInfo.expResult" onMouseOver="if(this.value=='可在过程及数据中用表格形式写预期结果,或单独在此填写'){select();}" onblur="javascript:if(this.value==''){this.value=$('expResultOld').value;}"  style="width:570;padding:2 0 0 4;" cols="100" rows="3">
    			      </textarea>
    			    </td>
    			  </tr>
				  <tr id="verTr" style="display:none">
				   <td class="rightM_center" width="90">执行版本</td>
				   <td width="570" style="width:200;padding:2 0 0 4;border-right:0" colspan="4">
				   	 <select id="sel_vers" name="sel_vers" style="width: 150;" class="text_c"> 
				      <option value="-1"></option> 
				    </select> &nbsp;&nbsp;&nbsp;
				    <font style="color:#055A78;font-weight: bold;font-size:12px;">执行备注:</font>
				     <input type="text" id="remark" name="dto.remark" Class="text_c" style="width:350;padding:2 0 0 4;"  maxlength="40"/>
				   </td>		  
				  </tr>	
				  <tr id="auditTr" style="display:none">
				   <td class="rightM_center" width="90" style="border-right:0"><div id="auditSelTxr">备注:</div></td>
				    <td width="570" style=";border-right:0" colspan="4">
				     <ww:textfield id="adtRemark" name="dto.testCaseInfo.remark" cssClass="text_c"  cssStyle="width:570;padding:2 0 0 4"  maxlength="100"></ww:textfield>
				   </td>	
				</ww:form>
			    <ww:form enctype="multipart/form-data" theme="simple" name="fileform" id="fileform" action="" method="post" target="target_upload">
				  <tr class="odd_mypm">
				  	 <td class="rightM_center" width="90" id="attachTd">
				  	 附件/插图片:
				  	 </td>
				    <td class="dataM_left"  colspan="4" width="570" style="border-right:0">
						<input name="currUpFile" id="currUpFile" type="file" style="padding:2 0 0 4;width:210" Class="text_c">
						<img src="<%=request.getContextPath()%>/images/button/attach.gif" style="display:none"id="currAttach" alt="当前附件" title="打开当前附件" onclick="openAtta()" />
						<img src="<%=request.getContextPath()%>/dhtmlx/toolbar/images/img_insert.png" id="insertImg" alt="当前位置插入图片" title="当前位置插入图片" onclick="upLoadAndSub('save','subChk',1,oEditor)" />
				    </td>
				  </tr>	
				  <tr class="ev_mypm">
				    <td width="90" align="right" >&nbsp;</td>
					<td width="570" style="width:200;padding:2 0 0 4;" colspan="4"><div id="upStatusBar"></div></td>		  
				  </tr>		
				  <img src="" style="display:none"id="picArea" alt="" />  
			  </ww:form>	
 		  	 <iframe id="target_upload" name="target_upload" src="" frameborder="0" scrolling="no" width="0" height="0"></iframe>		 
    			  <tr class="odd_mypm">
    			    <td  class="rightM_center" align="center" colspan="5">
    			    <a class="bluebtn" href="javascript:void(0);" id="ret_b"onclick="javascript:eraseAttach('eraseAllImg');dW_ch.hide();dW_ch.setModal(false);"
						style="margin-left: 6px;"><span> 返回</span> </a>
    			    <a class="bluebtn" href="javascript:void(0);" id="block2_b"onclick="exeCase('4','不适用')"
						style="margin-left: 6px;display: none;"><span> 不适用</span> </a> 
    			    <a class="bluebtn" href="javascript:void(0);" id="block_b"onclick="exeCase('5','阻塞')"
						style="margin-left: 6px;display: none;"><span> 阻塞</span> </a> 
    			    <a class="bluebtn" href="javascript:void(0);" id="failed_b"onclick="exeCase('3','未通过')"
						style="margin-left: 6px;display: none;"><span> 未通过</span> </a> 
    			    <a class="bluebtn" href="javascript:void(0);" id="pass_b"onclick="exeCase('2','通过')"
						style="margin-left: 6px;display: none;"><span> 通过</span> </a> 
    			    <a class="bluebtn" href="javascript:void(0);" id="auditoK_b"onclick="javascript:auditRest=1;cfDialog('adtExe','当前操作不可逆，确定要提交测试?');"
						style="margin-left: 6px;display: none;"><span> 提交测试</span> </a>  
    			    <a class="bluebtn" href="javascript:void(0);" id="auditRf_b"onclick="javascript:auditRest=6;cfDialog('adtExe','当前操作不可逆，确定要发回修正?');"
						style="margin-left: 6px;display: none;"><span> 发回修正</span> </a>  			     
    			    <a class="bluebtn" href="javascript:void(0);" id="saveBtn" onclick="upLoadAndSub('save','subChk',0,oEditor)"
						style="margin-left: 6px;display: none;"><span> 保存</span> </a>
					 <a class="bluebtn" href="javascript:void(0);" id="reSetAttaBtn" onclick="$('currUpFile').value='';if(_isIE)$('currUpFile').outerHTML=$('currUpFile').outerHTML" ><span>取消选取的文件</span> </a>
    			    </td>
    			  </tr>
    			  <tr class="ev_mypm"><td colspan="5">&nbsp;</td></tr>
    			</table>
    		</div>
		</div>
		<div id="findDiv"class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="findForm" name="findForm" namespace="/impExpMgr" action="caseImpExpAction!expCase.action">
    			 <input type="hidden" id=currNodeIdF name="dto.currNodeId" value=""/>
    			 <input type="hidden"  id="testRestF" name="dto.testCaseInfo.testStatus" value="0"/>
    			 <input type="hidden"  id="remarkF" name="dto.remark" value=""/>
    			<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
    			  <tr class="ev_mypm">
    			    <td width="80" class="rightM_center" align="right" style="border-right:0">类型:</td>
    			    <td  class="dataM_left" width="75" style="border-right:0">
    			      <select id="caseTypeIdF" name="dto.testCaseInfo.caseTypeId" style="width:75"  Class="text_c">
    			       <option   value="-1"></option> 	        
    			      </select>
    			    </td>
    			    <td  width="80" class="rightM_center" align="right" nowrap style="border-right:0">优先级:</td>
    			    <td  class="dataM_left" width="75" style="border-right:0">
    			      <select id="priIdF" name="dto.testCaseInfo.priId" style="width: 75"  Class="text_c">
    			       <option   value="-1"></option> 
    			      </select>
    			    </td>
    			    <td width="80" class="rightM_center" align="right" nowrap style="border-right:0">成本:</td>
    			    <td  class="dataM_left" width="75">	
    			      <ww:textfield id="weightF" name="dto.testCaseInfo.weight" cssClass="text_c" cssStyle="width:75;padding:2 0 0 4;" onkeypress="javascript:return numChk(event);"></ww:textfield>			
    			    </td>
    			  </tr>
    			  <tr class="odd_mypm">
    			    <td width="80" class="rightM_center" align="right" style="border-right:0">编写人:</td>
    			     <td colspan="5" width="393"  class="dataM_left" style="border-right:0">
    			      <ww:hidden id="createrIdF" name="dto.testCaseInfo.createrId"></ww:hidden>
					  <ww:textfield id="crNameF" name="dto.crName" readonly="true" onclick="popselWin('createrIdF','crNameF',' ' ,true,10)" cssClass="text_c" cssStyle="width:393;padding:2 0 0 4;" ></ww:textfield>
    			    </td>
    			  </tr>
    			  <tr class="ev_mypm" style="display:none">
    			    <td width="80" class="rightM_center" align="right" nowrap style="border-right:0">最近处理人:</td>
    			    <td colspan="5" width="393"  class="dataM_left">
    			    <ww:hidden id="testAuditIdF" name="dto.testCaseInfo.auditId"></ww:hidden>
    			    <ww:textfield id="auditNameF" name="auditNameF" readonly="true" onclick="popselWin('testAuditIdF','auditNameF',' ' ,true,10)" cssClass="text_c" cssStyle="width:393;padding:2 0 0 4;" ></ww:textfield>
    			    </td>
    			  </tr>
    			  <tr class="ev_mypm">
    			    <td width="80" class="rightM_center" align="right" nowrap style="border-right:0">用例描述:</td>
    			    <td colspan="5" width="393"  class="dataM_left" style="border-right:0">
    			    <ww:textfield id="testCaseDesF" cssClass="text_c" name="dto.testCaseInfo.testCaseDes" cssStyle="width:393;padding:2 0 0 4;" ></ww:textfield>
    			    </td>
    			  </tr>
    			  <tr class="odd_mypm">
    			    <td width="80" class="rightM_center" align="right"  style="border-right:0"></td>
    			    <td  class="dataM_left" width="75" style="border-right:0">		
    			    </td>
    			    <td width="80" align="right" style="border-right:0">
    			    </td>
    			    <td align="left" colspan="2" width="155" class="dataM_left" style="border-right:0" >
    			      
    			    </td>
    			    <td width="75" class="dataM_left" align="right"  style="border-right:0"></td>
    			  </tr>
    			  <tr class="ev_mypm">
    			    <td   align="center" colspan="6" style="border-right:0" >
    			      <a class="bluebtn" href="javascript:void(0);" id="ret_b" onclick="javascript:fW_ch.hide();fW_ch.setModal(false);"
						style="margin-left: 6px;"><span> 返回</span> </a>
					 <a class="bluebtn" href="javascript:void(0);" onclick="javascript:$('findForm').reset();"
						style="margin-left: 6px;"><span> 清空</span> </a>
    			     <a class="bluebtn" href="javascript:void(0);" id="query_b"onclick="findExe()"
						style="margin-left: 6px;"><span> 查询</span> </a>
					<a class="bluebtn" href="javascript:void(0);" onclick="javascript:$('currNodeIdF').value=$('currNodeId').value;$('findForm').submit();"><span>
									导出</span> </a>
    			    </td>
    			  </tr>
    			  <tr class="odd_mypm">
    			    <td width="80" align="right" style="border-right:0"> 
    			    </td>
    			    <td align="left" colspan="2" width="155"  class="dataM_left" style="border-right:0">
    			    </td>
    			    <td width="75" class="dataM_left" align="right"  style="border-right:0"></td>
    			    <td width="80" class="rightM_center" align="right"  style="border-right:0"></td>
    			    <td  class="dataM_left" width="75" style="border-right:0">	
    			    </td>
    			  </tr>    			  
			</table>
			</ww:form>
			<ww:form theme="simple" method="post" id="pform" name="pform" namespace="" action="">
              <input type="hidden" id="priIdP" name="dto.testCaseInfo.priId" value=""/>
              <input type="hidden" id="caseTypeIdP" name="dto.testCaseInfo.caseTypeId" value=""/>
              <input type="hidden" id="testRestP" name="dto.testCaseInfo.testStatus" value=""/>
              <input type="hidden" id="createrIdP" name="dto.testCaseInfo.createrId" value=""/>
              <input type="hidden" id="testAuditIdP" name="dto.testCaseInfo.auditId" value=""/>				
              <input type="hidden" id="weightP" name="dto.testCaseInfo.weight" value=""/>
              <input type="hidden" id="testCaseDesP" name="dto.testCaseInfo.testCaseDes" value=""/>
			</ww:form>	
		  </div>
		</div>
		<ww:include value="/jsp/common/selCompPerson.jsp"></ww:include>
		<ww:include value="/jsp/common/downLoad.jsp"></ww:include>
	</body>
	<script type="text/javascript">
		importJs(conextPath+"/jsp/caseManager/batchBase.js");
		importJs(conextPath+"/jsp/common/upload.js");
	</script>

</html>
