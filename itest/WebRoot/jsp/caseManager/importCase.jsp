<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/webwork" prefix="ww"%>
<%@ taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<title>测试用例</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
<link rel="STYLESHEET" type="text/css"
	href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
<script type="text/javascript"
	src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
<link rel="STYLESHEET" type="text/css"
	href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
<link rel="STYLESHEET" type="text/css"
	href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
<script type="text/javascript"
	src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
<link rel='STYLESHEET' type='text/css'
	href='<%=request.getContextPath()%>/css/page.css'>
</head>

<style>
.prog-border {
	height: 15px;
	width: 150px;
	background: #fff;
	border: 1px solid #000;
	margin: 0;
	padding: 0;
}

.prog-bar {
	height: 11px;
	margin: 2px;
	padding: 0px;
	background: #178399;
	font-size: 10pt;
}
</style>

<body bgcolor="#ffffff">
<ww:form enctype="multipart/form-data" name="importFileForm" id="importFileForm"
	action="" method="post" >
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr class="odd_mypm">
			<td class="rightM_center" width="80" nowrap style="color: red;fontSize:10" >导入文件:</td>
			<td class="dataM_left"  width="150"><input
				name="importFile" id="importFile" type="file"
				style="padding: 2 0 0 4; width: 150" cssClass="text_c"></td>
		</tr>
		<tr class="ev_mypm">
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr class="odd_mypm">
			<td class="dataM_center" align="center" width="230 " colspan="2">
				<a class="bluebtn" href="javascript:void(0);"
				onclick="javascript:parent.caseImp_ch.setModal(false);parent.caseImp_ch.hide();"
				style="margin-left: 6px"><span> 取消</span> </a> <a class="bluebtn"
				href="javascript:void(0);" id="saveBtn" onclick="exeImp()"
				style="margin-left: 6px; "><span> 导入</span> </a>
			<td>
		</tr>


	</table>
</ww:form>
<ww:include value="/jsp/common/dialog.jsp"></ww:include>
</body>
</html>
<script type="text/javascript">
	function exeImp(){
		$("importFileForm").action=conextPath+"/impExpMgr/caseBugImpAction!impCase.action";
		$("importFileForm").submit();
	}
	var opeFlg = "<%=request.getAttribute("operaFlg")%>";
	if(opeFlg!="null"&&opeFlg=="success"){
		parent.hintMsg("导入成功")
	}	if(opeFlg!="null"&&opeFlg!="success"){
		parent.hintMsg(opeFlg,"200","200");
	}
	
</script>