<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<html>
	<head>
		<title>最近执行的测试用例</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
		<link rel='STYLESHEET' type='text/css'href='<%=request.getContextPath()%>/css/page.css'>
	</head>
	<%
	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd");
	java.util.Date currentTime = new java.util.Date();
	String nowStr = formatter.format(currentTime);
	%>
	<script type="text/javascript">
		<pmTag:priviChk urls="caseManagerAction!exeCase" varNames="canExe"/>
	</script>
	<body bgcolor="#ffffff" style="overflow-x:hidden;">
	
		<ww:hidden id="currNodeId" name="dto.currNodeId"></ww:hidden>
		<ww:hidden id="testPhase" name="dto.testPhase"></ww:hidden>
		<input type="hidden" id="listStr" name="listStr"value="${dto.listStr}" />
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top">
					<div id="toolbarObj"></div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<div id="gridbox"></div>
				</td>
			</tr>
		</table>
		<script type="text/javascript">
		ininPage("toolbarObj", "gridbox", 695);
		var nowStr = "<%=nowStr%>";
		var testRemark = "关联BUG自动生成测试结果";
		var testRest = "未通过";
		var myName = "${session.currentUser.userInfo.name}";
		importJs(conextPath+"/jsp/caseManager/lastExeCase.js");
		if(!canExe)
		   try{pmBar.disableItem("exeCase");}catch(err){}
		</script>
		<div id="findDiv"class="cycleTask gridbox_light" style="border:0px;display:none;" align="center">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<input type="hidden" id="taskName" name="taskName" value="" />
			<ww:form theme="simple" method="post" id="findForm" name="findForm" namespace="" action="">
			<ww:hidden id="taskIdF" name="dto.taskId"></ww:hidden>
    			<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
    			  <tr class="ev_mypm">
    			    <td width="80" class="rightM_center" align="right" style="border-right:0">类型:</td>
    			    <td  class="dataM_left" width="100" style="border-right:0">
    			      <select id="caseTypeIdF" name="dto.queryHelp.caseTypeId" style="width:100"  Class="text_c">
    			       <option   value="-1"></option> 	        
    			      </select>
    			    </td>
    			    <td  width="80" class="rightM_center" align="right" nowrap style="border-right:0">优先级:</td>
    			    <td  class="dataM_left" width="75" style="border-right:0">
    			      <select id="priIdF" name="dto.queryHelp.priId" style="width: 75"  Class="text_c">
    			       <option   value="-1"></option> 
    			      </select>
    			    </td>
    			    <td width="80" class="rightM_center" align="right" style="border-right:0">测试结果:</td>
    			    <td  class="dataM_left" width="75" style="border-right:0">
					<ww:select id="testRestF" name="dto.queryHelp.testResult" cssStyle="width:75;"  cssClass="text_c"
						list="#{-1:'',1:'未测试',2:'通过',3:'未通过',4:'不适用',5:'阻塞'}" headerValue="-1" >
					</ww:select>					
    			    </td>
    			  </tr>
    			  <tr class="odd_mypm">
    			    <td width="80" class="rightM_center" align="right" style="border-right:0">执行版本:</td>
    			    <td  class="dataM_left" width="100" style="border-right:0">
    			      <select id="exeVerF" name="dto.queryHelp.exeVerId" style="width:100"  Class="text_c">
    			       <option   value="-1"></option> 	        
    			      </select>
    			    </td>
    			    <td  width="80" class="rightM_center" align="right" nowrap style="border-right:0">执行日期:</td>
    			    <td  class="dataM_left" width="75" style="border-right:0">
    			      <ww:textfield id="exeDateF" name="dto.queryHelp.exeDate" readonly="true" onclick="initDhtmlxCalendar(); showCalendar(this);" cssClass="text_c" cssStyle="width:75;padding:2 0 0 4;" ></ww:textfield>
    			    </td>
    			    <td  style="border-right:0" colspan="2">
    			    </td>
    			  </tr>
    			  <tr class="ev_mypm">
    			    <td width="80" class="rightM_center" align="right" style="border-right:0">执行人:</td>
    			     <td colspan="5" width="393"  class="dataM_left">
    			      <ww:hidden id="actorIdF" name="dto.queryHelp.actorId"></ww:hidden>
					  <ww:textfield id="actorIdNameF" name="dto.crName" readonly="true" onclick="popselWin('actorIdF','actorIdNameF',' ' ,true,5)" cssClass="text_c" cssStyle="width:393;padding:2 0 0 4;" ></ww:textfield>
    			    </td>
    			  </tr>
    			  <tr class="odd_mypm">
    			    <td width="80" class="rightM_center" align="right" style="border-right:0">编写人:</td>
    			     <td colspan="5" width="393"  class="dataM_left">
    			      <ww:hidden id="createrIdF" name="dto.queryHelp.createrId"></ww:hidden>
					  <ww:textfield id="crNameF" name="dto.crName" readonly="true" onclick="popselWin('createrIdF','crNameF',' ' ,true,5)" cssClass="text_c" cssStyle="width:393;padding:2 0 0 4;" ></ww:textfield>
    			    </td>
    			  </tr>
    			  <tr class="ev_mypm">
    			    <td width="80" class="rightM_center" align="right"  style="border-right:0">审核人:</td>
    			    <td colspan="5" width="393"  class="dataM_left">
    			    <ww:hidden id="testAuditIdF" name="dto.queryHelp.auditerId"></ww:hidden>
    			    <ww:textfield id="auditNameF" name="auditNameF" readonly="true" onclick="popselWin('testAuditIdF','auditNameF',' ' ,true,5)" cssClass="text_c" cssStyle="width:393;padding:2 0 0 4;" ></ww:textfield>
    			    </td>
    			  </tr>
    			  <tr class="odd_mypm">
    			    <td width="80" class="rightM_center" align="right"  style="border-right:0">用例描述:</td>
    			    <td colspan="5" width="393"  class="dataM_left">
    			    <ww:textfield id="testCaseDesF" cssClass="text_c" name="dto.queryHelp.testCaseDes" cssStyle="width:393;padding:2 0 0 4;" ></ww:textfield>
    			    </td>
    			  </tr>
    			  <tr class="ev_mypm">
    			    <td width="80" class="rightM_center" align="right"  style="border-right:0">成本:</td>
    			    <td  class="dataM_left" width="75" style="border-right:0">	
    			      <ww:textfield id="weightF" name="dto.queryHelp.weight" cssClass="text_c" cssStyle="width:75;padding:2 0 0 4;" onkeypress="javascript:return numChk(event);"></ww:textfield>			
    			    </td>
    			    <td style="border-right:0" colspan="4"></td>
    			  </tr>
    			  <tr class="odd_mypm">
    			    <td   align="center" colspan="6" style="border-right:0">
    			      <a class="bluebtn" href="javascript:void(0);" id="ret_b" onclick="javascript:fW_ch.hide();fW_ch.setModal(false);if(typeof(mypmCalendar_ch)!='undefined') mypmCalendar_ch.hide();"
						style="margin-left: 6px;"><span> 返回</span> </a>
					 <a class="bluebtn" href="javascript:void(0);" onclick="javascript:$('findForm').reset();"
						style="margin-left: 6px;"><span> 清空</span> </a>
    			     <a class="bluebtn" href="javascript:void(0);" id="query_b"onclick="findExe()"
						style="margin-left: 6px;"><span> 查询</span> </a>
    			    </td>
    			  </tr>
			</table>
			</ww:form>
		</div>
		</div>
		<div id="createDiv" class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="createForm" enctype="multipart/form-data"
				name="createForm" namespace="" action="">
				<input type="hidden" id="mdPath" name="mdPath" value="" />
				<ww:hidden id="taskId" name="dto.testCaseInfo.taskId" value="${dto.taskId}"></ww:hidden>
			    <ww:hidden id="moduleId" name="dto.testCaseInfo.moduleId"></ww:hidden>
			    <ww:hidden id="createrId" name="dto.testCaseInfo.createrId"></ww:hidden>
			    <ww:hidden id="testCaseId" name="dto.testCaseInfo.testCaseId"></ww:hidden>
			    <ww:hidden id="isReleased" name="dto.testCaseInfo.isReleased"></ww:hidden>
				<ww:hidden id="creatdate" name="dto.testCaseInfo.creatdate"></ww:hidden>
				<ww:hidden id="attachUrl" name="dto.testCaseInfo.attachUrl"></ww:hidden>
				<ww:hidden id="auditId" name="dto.testCaseInfo.auditId"></ww:hidden>
			    <ww:hidden id="testStatus" name="dto.testCaseInfo.testStatus"></ww:hidden>
			    <ww:hidden id="testData" name="dto.testCaseInfo.testData"></ww:hidden>
			    <ww:hidden id="moduleNum" name="dto.testCaseInfo.moduleNum"></ww:hidden>
			    <ww:hidden id="expResultOld" ></ww:hidden>
    			<table class="obj row20px" cellspacing="0" id="createTable" cellpadding="0" border="0" width="100%">
    			  <tr class="ev_mypm">
    			    <td colspan="5" class="tdtxt" align="center">
    			      <div id="cUMTxt" align="center" style="color: Blue; padding: 2px"></div>
    			    </td>
    			  </tr>
    			  <tr class="odd_mypm">
    			    <td width="50" class="rightM_center" style="color:red;">类型:</td>
    			    <td  class="dataM_left" width="75">
    			      <select id="caseTypeId" name="dto.testCaseInfo.caseTypeId"  Class="text_c" style="width:75">
    			       <option   value="-1">请选择</option> 	        
    			      </select>
    			    </td>
    			    <td  width="50" class="rightM_center" style="color:red;" nowrap>优先级:</td>
    			    <td  class="dataM_left" width="75">
    			      <select id="priId" name="dto.testCaseInfo.priId" style="width: 75"  Class="text_c">
    			       <option   value="-1">请选择</option> 
    			      </select>
    			    </td>
    			    <td  class="rightM_center" nowrap>&nbsp;执行成本:
    			       <ww:textfield id="weight" name="dto.testCaseInfo.weight" cssStyle="width:70;padding:2 0 0 4;"  cssClass="text_c" onkeypress="javascript:return numChk(event);"></ww:textfield>
    			       <font color="Blue">一个成本单位代表5分钟</font> 
    			    </td>
    			  </tr>
    			  <tr class="ev_mypm">
    			    <td width="90" class="rightM_center" style="color:red;" >用例描述:</td>
    			    <td  class="dataM_left" colspan="4">
    			      <ww:textfield id="testCaseDes" cssClass="text_c" name="dto.testCaseInfo.testCaseDes" cssStyle="width:560;padding:2 0 0 4;" ></ww:textfield>
    			    </td>
    			  </tr>
    			  <tr class="odd_mypm">
    			    <td width="90" class="rightM_center" style="color:red;" >过程及数据:</td>
    			    <td width="570" class="dataM_left"  Class="text_c" colspan="4" style="background-color: #f7f7f7;">
    			      <textarea name="dto.testCaseInfo.operDataRichText" id="operDataRichText" cols="50" rows="15" style="width:570;padding:2 0 0 4;">
    			        <ww:property value="dto.testCaseInfo.operDataRichText" escape="false"/>
    			      </textarea>
    			    </td>
    			  </tr>
    			  <tr class="ev_mypm">
    			    <td width="90" class="rightM_center" align="right">预期结果:</td>
    			    <td  class="dataM_left" colspan="4">
    			      <textarea id="expResult"  Class="text_c"name="dto.testCaseInfo.expResult" onMouseOver="select()" onblur="javascript:if(this.value==''){this.value=$('expResultOld').value;}"  style="width:570;padding:2 0 0 4;" cols="100" rows="3">
    			      </textarea>
    			    </td>
    			  </tr>
				  <tr id="verTr" style="display:none">
				   <td class="rightM_center" width="90">执行版本</td>
				   <td width="570" style="width:200;padding:2 0 0 4;" colspan="4">
				   	 <select id="sel_vers" name="sel_vers" style="width: 150;" class="text_c"> 
				      <option value="-1"></option> 
				    </select>
				    &nbsp;&nbsp;&nbsp;<font style="color:#055A78;font-weight: bold;font-size:12px;">备注:</font>
				     <input type="text" id="remark" name="dto.remark" Class="text_c" style="width:370;padding:2 0 0 4;"/>
				   </td>		  
				  </tr>	
				  <tr class="odd_mypm">
				  	 <td class="rightM_center" width="90" id="attachTd">
				  	 附件:
				  	 </td>
				    <td class="dataM_left"  colspan="4" width="570" style="border-right:0">
						<img src="<%=request.getContextPath()%>/images/button/attach.gif" style="display:none" id="currAttach" alt="当前附件" title="打开当前附件" onclick="openAtta()" />
				    </td>
				  </tr>	
    			  <tr class="odd_mypm">
    			    <td  class="rightM_center" align="center" colspan="5">
    			    <a class="bluebtn" href="javascript:void(0);" id="ret_b"onclick="javascript:dW_ch.hide();dW_ch.setModal(false);"
						style="margin-left: 6px;"><span> 返回</span> </a>
    			    <a class="bluebtn" href="javascript:void(0);" id="block2_b"onclick="exeCase('4');"
						style="margin-left: 6px;display: none;"><span> 不适用</span> </a> 
    			    <a class="bluebtn" href="javascript:void(0);" id="block_b"onclick="exeCase('5');"
						style="margin-left: 6px;display: none;"><span> 阻塞</span> </a> 
    			    <a class="bluebtn" href="javascript:void(0);" id="failed_b"onclick="exeCase('3');"
						style="margin-left: 6px;display: none;"><span> 未通过</span> </a> 
    			    <a class="bluebtn" href="javascript:void(0);" id="pass_b"onclick="exeCase('2');"
						style="margin-left: 6px;display: none;"><span> 通过</span> </a> 			     
    			    <a class="bluebtn" href="javascript:void(0);" id="saveBtn" onclick="save();"
						style="margin-left: 6px;display: none;"><span> 保存</span> </a>
    			  </tr>
    			  <tr class="ev_mypm"><td colspan="5">&nbsp;</td></tr>
    			</table>
    		 </ww:form>
    		</div>
		</div>
		<ww:include value="/jsp/common/dialog.jsp"></ww:include>
		<ww:include value="/jsp/common/downLoad.jsp"></ww:include>
		<ww:include value="/jsp/common/selCompPerson.jsp"></ww:include>
	</BODY>
</HTML>
