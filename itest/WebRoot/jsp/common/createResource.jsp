<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<div id="createResource" class="gridbox_light" style="border:0px;display:none;">
	<div class="objbox" style="overflow:auto;width:100%;">
		<ww:form theme="simple" method="post" id="createResourceF" name="createResourceF" namespace="helper" action="helperAction!createOrUpdateResource.action">
			<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr class="ev_mypm">
					<td class="leftM_center" style="color:red;">&nbsp;&nbsp;资源名称:&nbsp;</td>
					<td class="dataM_left" colspan="3" valign="middle">
					<input id="displayName" class="text_c" type="text" style="width: 245px;" name="helperDto.resource.displayName" maxlength="50" /><img id="searchUserImg" src="<%=request.getContextPath()%>/jsp/common/images/searchObject.gif" onclick="searchUser();" title="查找可选的用户" style="cursor:pointer;" /></td>
				</tr>
				<tr class="odd_mypm">
					<td class="leftM_center" style="color:red;">资源数值:</td>
					<td class="dataM_left" width="100"><div style="float:left;"><ww:textfield id="scalar" name="helperDto.resource.scalar" cssStyle="width:79;" cssClass="text_c" onkeypress="javascript:return numChk(event);" maxlength="5"></ww:textfield></div><div id="scalarLable" style="float:left;width:20px;">/月</div></td>
					<td class="leftM_center">数值单位:</td>
					<td class="dataM_left" width="100">
						<select id="scalarUnit" style="width:79px;" name="helperDto.resource.scalarUnit" class="select_c">
							<option value="人民币" selected>人民币</option>
							<option value="美金">美金</option>
							<option value="日元">日元</option>
							<option value="港币">港币</option>
							<option value="欧元">欧元</option>
						</select>
					</td>
				</tr>
				<tr class="ev_mypm">
					<td class="leftM_center">资源类型:</td>
					<td class="dataM_left" colspan="3">
						<input type="radio" name="helperDto.resource.type" id="resourceType0" checked="checked" value="0" onclick="setResourceType(this);" /><label for="resourceType0">工时资源</label>
						<input type="radio" name="helperDto.resource.type" id="resourceType1" value="1" onclick="setResourceType(this);" /><label for="resourceType1">材料资源</label>
						<input type="radio" name="helperDto.resource.type" id="resourceType2" value="2" onclick="setResourceType(this);" /><label for="resourceType2">成本资源</label>
	        		</td>
				</tr>
				<tr class="odd_mypm">
					<td class="leftM_center">加班费率:</td>
					<td class="dataM_left"><ww:textfield id="overTime" name="helperDto.resource.overTime" cssStyle="width:79px;" cssClass="text_c" onkeypress="javascript:return numChk(event);" maxlength="3"></ww:textfield>/时</td>
					<td class="leftM_center">职位名称:</td>
					<td class="dataM_left"><ww:textfield id="headship" name="helperDto.resource.headship" cssStyle="width:79;" cssClass="text_c" maxlength="50"></ww:textfield></td>
				</tr>
				<tr class="odd_mypm">
					<td class="leftM_center" style="color:red;" id="startDateTD">开始可用:</td>
					<td class="dataM_left">
						<ww:textfield id="startDate" name="helperDto.resource.startDate" cssStyle="width:79;" cssClass="text_c" readonly="true" onclick="showCalendar(this);"></ww:textfield>
						<img style="cursor:pointer;" src="<%=request.getContextPath()%>/jsp/common/images/refresh.gif" onclick="$('startDate').value='';" title="清空" />	
					</td>
					<td class="leftM_center">可用到:</td>
					<td class="dataM_left">
						<ww:textfield id="endDate" name="helperDto.resource.endDate" cssStyle="width:79px;" cssClass="text_c" readonly="true" onclick="showCalendar(this);"></ww:textfield>
						<img style="cursor:pointer;" src="<%=request.getContextPath()%>/jsp/common/images/refresh.gif" onclick="$('endDate').value='';" title="清空" />
					</td>
				</tr>
				<tr class="ev_mypm">
					<td class="leftM_center">&nbsp;&nbsp;临时资源:&nbsp;</td>
					<td class="dataM_left" colspan="3" valign="middle">
						<input type="checkbox" name="helperDto.resource.temporary" id="temporary" value="1" onclick="this.checked=!this.checked" /><label for="temporary" class="checkboxLabel">临时资源</label>
					</td>
				</tr>
				<tr class="odd_mypm">
					<td class="leftM_center">备注信息:</td>
					<td class="dataM_left" colspan="3">
						<ww:textarea id="remark" name="helperDto.resource.remark" cssStyle="width:265px;height:50px !ie;" cssClass="textarea" rows="2"></ww:textarea>
	        		</td>
				</tr>
				<tr class="ev_mypm"><td colspan="4"><div id="createResourceText" class="waring">&nbsp;</div></td></tr>
				<tr class="odd_mypm">
					<td colspan="4" align="right" style="padding-right:8px;"><img id="cuR_b" src="<%=request.getContextPath()%>/jsp/common/images/save.gif" onclick="createOrUpdateResource();" title="创建" style="cursor:pointer;" />&nbsp;&nbsp;<img src="<%=request.getContextPath()%>/jsp/common/images/reset.gif" onclick="formResetAll();setUserResourceEnable(false);" title="重置" style="cursor:pointer;" /></td>
				</tr>
				<tr class="ev_mypm"><td colspan="4" >&nbsp;</td></tr>
			</table>
			<ww:hidden id="resource_id" name="helperDto.resource.id"></ww:hidden>
			<ww:hidden id="userId" name="helperDto.resource.userId"></ww:hidden>
		</ww:form>
	</div>
</div>
<div id="batchCreateResourceDiv" class="gridbox_light" style="border:0px;display:none;">
	<div class="objbox" style="overflow:auto;width:100%;">
		<ww:form theme="simple" method="post" id="batchCreateResourceF" name="batchCreateResourceF" namespace="helper" action="helperAction!batchCreateResource.action">
			<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr class="ev_mypm">
					<td class="leftM_center" style="color:red;">&nbsp;&nbsp;资源名称:&nbsp;</td>
					<td class="dataM_left" colspan="3" valign="middle">
					<input id="name_batch" class="text_c" type="text" style="width: 245px;" name="helperDto.resource.displayName" maxlength="50" readonly="readonly" /><img id="searchUserImg_batch" src="<%=request.getContextPath()%>/jsp/common/images/searchObject.gif" onclick="searchUser_batch();" title="查找可选的用户" style="cursor:pointer;" /></td>
					<ww:hidden id="userId_batch" name="helperDto.resource.userId"></ww:hidden>
				</tr>
				<tr class="odd_mypm">
					<td class="leftM_center" style="color:red;">资源数值:</td>
					<td class="dataM_left" width="100"><div style="float:left;"><ww:textfield id="scalar_batch" name="helperDto.resource.scalar" cssStyle="width:79;" cssClass="text_c" onkeypress="javascript:return numChk(event);" maxlength="5"></ww:textfield></div><div style="float:left;width:20px;">/月</div></td>
					<td class="leftM_center">数值单位:</td>
					<td class="dataM_left" width="100">
						<select id="scalarUnit_batch" style="width:79px;" name="helperDto.resource.scalarUnit" class="select_c">
							<option value="人民币" selected>人民币</option>
							<option value="美金">美金</option>
							<option value="日元">日元</option>
							<option value="港币">港币</option>
							<option value="欧元">欧元</option>
						</select>
					</td>
				</tr>
				<tr class="ev_mypm">
					<td class="leftM_center">加班费率:</td>
					<td class="dataM_left" colspan="3"><ww:textfield id="overTime_batch" name="helperDto.resource.overTime" cssStyle="width:79px;" cssClass="text_c" onkeypress="javascript:return numChk(event);" maxlength="3"></ww:textfield>/时</td>
				</tr>
				<tr class="odd_mypm">
					<td class="leftM_center" style="color:red;">开始可用:</td>
					<td class="dataM_left">
						<ww:textfield id="startDate_batch" name="helperDto.resource.startDate" cssStyle="width:79;" cssClass="text_c" readonly="true" onclick="showCalendar(this);"></ww:textfield>
						<img style="cursor:pointer;" src="<%=request.getContextPath()%>/jsp/common/images/refresh.gif" onclick="$('startDate_batch').value='';" title="清空" />	
					</td>
					<td class="leftM_center">可用到:</td>
					<td class="dataM_left">
						<ww:textfield id="endDate_batch" name="helperDto.resource.endDate" cssStyle="width:79px;" cssClass="text_c" readonly="true" onclick="showCalendar(this);"></ww:textfield>
						<img style="cursor:pointer;" src="<%=request.getContextPath()%>/jsp/common/images/refresh.gif" onclick="$('endDate_batch').value='';" title="清空" />	
					</td>
				</tr>
				<tr class="ev_mypm">
					<td class="leftM_center">&nbsp;&nbsp;临时资源:&nbsp;</td>
					<td class="dataM_left" colspan="3" valign="middle">
						<input type="checkbox" name="helperDto.resource.temporary" id="temporary_batch" value="1" onclick="this.checked=!this.checked" /><label for="temporary_batch" class="checkboxLabel">临时资源</label>
					</td>
				</tr>
				<tr class="odd_mypm"><td colspan="4"><div id="createResourceText_batch" class="waring">&nbsp;</div></td></tr>
				<tr class="ev_mypm">
					<td colspan="4" align="right" style="padding-right:8px;"><img src="<%=request.getContextPath()%>/jsp/common/images/save.gif" onclick="batchCreateResource();" title="批量创建" style="cursor:pointer;" />&nbsp;&nbsp;<img src="<%=request.getContextPath()%>/jsp/common/images/reset.gif" onclick="formReset('batchCreateResourceF', 'name_batch');" title="重置" style="cursor:pointer;" /></td>
				</tr>
				<tr class="odd_mypm"><td colspan="4" >&nbsp;</td></tr>
			</table>
		</ww:form>
	</div>
</div>
<script type="text/javascript">
	importJs(conextPath + "/jsp/common/createResource.js");
</script>