<%@ page contentType="text/html; charset=UTF-8"%>
	<form action="downloadfile" method="post" id="downDocForm" name="downDocForm">
		<input type="hidden" id="downloadDocFileName" name="downloadDocFileName" value=""/>
   </form>	
	<script type="text/javascript">
		function openDoc(fileId){
			if(typeof (fileId)!="undefined"&&""!=fileId){
				$("downloadDocFileName").value=fileId;
			}else{
				return;
			}
			var ajaxResut = postSub(conextPath+"/mypmDocDelegate?cmd=secChk&downloadDocFileName="+fileId,"");
			if("access" !=ajaxResut){
				if("haveDel"==ajaxResut){
					if(typeof hintMsg != "undefined")
						hintMsg("该文档己被删除");
					else
					   	showMessage(true, "提醒", "该文档己被删除");	
					return;					
				}else{
					if(typeof hintMsg != "undefined")
						hintMsg("您没有访问该文档权限");
					else
					   	showMessage(true, "提醒", "您没有访问该文档权限");	
					return;					
				}
			}
			$("downDocForm").action=conextPath+"/mypmDocDelegate?cmd=dw";
			$("downDocForm").submit();	
		}
	</script>