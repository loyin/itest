<%@ page contentType="text/html; charset=UTF-8"%>
<input type="hidden" id="setProjectId" name="setProjectId">
<div id="selProjectResourceDiv" class="gridbox_light" style="border:0px;display:none;">
	<div class="objbox" style="overflow:auto;width:100%;border:0px;">
		<table class="obj row20px" cellspacing="0" cellpadding="0" width="100%" border="0">
			<tr class="ev_mypm">
				<td style="border-right:0;" colspan="2">
					<div id="selProjectResourceText" style="width:365px;float:left;font-family: Tahoma;font-weight: bold;color:#055A78;font-size:12px;text-align:left;vertical-align:middle;padding-left:5px;">提示:项目周期内都使用的资源, 如办公场地费用等, 与任务无关</div><div style="float:left;"><img id="selProjectResourceImg" style="cursor:pointer;" src="<%=request.getContextPath()%>/jsp/common/images/selected.gif" onclick="selProjectResource();" title="确定" /></div>
				</td>
			</tr>
			<tr class="ev_mypm" style="padding-top:2px;border:0px;">
				<td valign="top" style="border:0px;">
					<div id="selGridbox_proR" style="width:198px;margin-left:-2px;"></div>
				</td>
				<td valign="top" style="border:0px;">
					<div id="seledGridbox_proR" style="width:198px;margin-left:-5px;"></div>
				</td>
			</tr>
		</table>
	</div>
</div>
<script type="text/javascript">
	importJs(conextPath + "/jsp/common/selProjectResource.js");
</script>