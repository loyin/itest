<%@ page contentType="text/html; charset=UTF-8"%>
<div id="selPeopleDiv" class="gridbox_light" style="border:0px;display:none;">
	<div class="objbox" style="overflow:auto;width:100%;">
	  	<form  method="post" id="selPeopleForm" name="selPeopleForm"  action="">
			<table class="obj row20px" cellspacing="0" cellpadding="0" width="100%">
				<tr >
				  <td colspan="2" style="border-right:0">&nbsp;</td>
				</tr>
				<tr >
				  <td align="center"  class="leftM_center" style="border-right:0;">用户组:</td>
				  <td style="border-right:0;">
				    <select id="sel_groupIds" name="dto.group.id" style="width:80;" class="select_c" onchange="loadSelUser()"> 
				    	<option value="-1"></option>
				    </select>
				  </td>
				</tr>
				<tr  >
				  <td align="center" class="leftM_center" style="border-right:0">姓名:</td>
				  <td style="border-right:0;">
				  	<input type="text" id="userNameSF" name="dto.userName" class="text_c"  style="width:80;" />
				  	<img id="fres_btn" style="cursor:pointer;" src="<%=request.getContextPath()%>/jsp/common/images/search.gif" onclick="loadSelUser();" title="查询人员" />&nbsp;&nbsp;
				  </td>
				</tr>
				<tr >
				  <td colspan="2" style="border-right:0">&nbsp;</td>
				</tr>
				<tr height="290" class="ev_mypm" >
					<td colspan="2" style="border-right:0">
						<div id="selGridbox" style="float:left;width:175px;"></div>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
<script type="text/javascript">
importJs(conextPath+"/jsp/common/selSingleCompanyPerson.js");
</script>