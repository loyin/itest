	var currTime,initFileBk="",fileChkMsg="";
	function eraseAttach(eraseAllImg){
		if(initFileBk=="")
			return;
		dhtmlxAjax.post(conextPath+"/fileUpload?cmd=delete&eraseFiles="+initFileBk);
		upVarReset();
	}
	function upVarReset(){
		initFileBk="";
		fileChkMsg="";
		$3("uploadText").value="";
		if(_isIE)
		    $('uploadText').outerHTML=$('uploadText').outerHTML;
		$3("downloadImg").style.display="none";
		$3('upStatusBar').innerHTML="";
		$3("uploadHidden").value="";
	}

	var loadStatus = "",upMsg="",upErrorCount=0,saveBtnId="saveBtn",subFun="",subChkMeth="";
	function upLoadAndSub(sunFunction,chkMethod){
		loadStatus = "loadIng";
		upMsg="";
		if($3("uploadText").value==""){
			eval(sunFunction+"()");
		}else{
			if(includeSpeChar(getFileName())){
				$('upStatusBar').innerHTML="文件名不能含特殊字符";
				return;
			}
			subFun = sunFunction;
			subChkMeth = chkMethod;
			if(eval(subChkMeth+"()")){
				initFileBk="";
				$3('upStatusBar').innerHTML="";
				currTime = (new Date()).getTime();
				if(fileChk()){
					$3("fileform").action=conextPath+"/fileUpload?cmd=upload&upTime="+currTime;
					$3("fileform").target="target_upload";
					$3("fileform").submit();
					loadStatus = "loadIng"
					$3(saveBtnId).style.display="none";
					$3('upStatusBar').innerHTML ="<div class=\"prog-border\"><div class=\"prog-bar\" style=\"width:5%\"></div></div>";
					statusChk();					
				}else{
					$3("upStatusBar").innerHTML = "<div style='font-size: 12px;padding: 2 0 0 4;'><font color='Red'>"+fileChkMsg+"</font></div>";
					return;
				}
			}
		}
	}
	function statusChk(){
		var upPercent = "0";
	 	var ajaxRes = "";
	 	var waitCount = 1;
	 	for(var i=0; i<2000;i++){//空循环是为了等上传一会以后再检查上传进度
		}
		$3('upStatusBar').innerHTML ="<div class=\"prog-border\"><div class=\"prog-bar\" style=\"width:5%\"></div></div>";
		while(loadStatus=="loadIng"){
			if(upMsg!=""){
				$3('upStatusBar').innerHTML = "<div class='tdtxt'><font color='Red'>" + upMsg + "</font></div>";
				return;
			}
			if(upPercent=="error"){
				return;
			}			
			var chkUrl = conextPath+"/fileUpload?cmd=chkUpLoad&upPercent="+upPercent+"&upTime="+currTime;
			if(chkUrl.indexOf("error")>=0){
				return;
			}
			if(ajaxRest=="error"){
				 "<div style='width:200;padding:2 0 0 4;'><font color='Red'>上传发生错误</font></div>";
				 if(!_isFF)
				 	eraseUpFlag();
				 return;
			}else if(ajaxRest=="ManyUpOccur"){
				 "<div style='width:200;padding:2 0 0 4;'><font color='Red'>正在上传其他附件，稍后再试</font></div>";
				 return;			
			}
			if(waitCount>=3&&ajaxRest.split("^")[0]=="0"){
				loadStatus = "error";
				return;
			}
			if(ajaxRest!=""&&ajaxRest.split("^")[0]!=upPercent){
				//$3('upStatusBar').innerHTML = ajaxRest.split("^")[1];
				 var rest = ajaxRest.split("^")[1];
				 if(typeof rest!="undefined")
				 	$$3('upStatusBar').innerHTML = rest;
				if(ajaxRest.split("^")[0]=="100"){
				 if(!_isFF)
				 	eraseUpFlag();
					return;
				}
				if(upPercent=="0"||(ajaxRest!=""&&upPercent==ajaxRest.split("^")[0])){//上传进度不变，等一会再检查
					for(var l=0; l<20000;l++){
					}
				}
				upPercent= ajaxRest.split("^")[0];	
			}
			if(upPercent=="0"){
				waitCount ++;
			}
		}
	}
	function eraseUpFlag(){
			 dhtmlxAjax.post(conextPath+"/fileUpload?cmd=erase&upTime="+currTime);
	}
	
	function upMsgConv(msg){
		upMsg ="上传发生错误";
		if(msg=="ManyUpOccur"){
			upMsg="您正在上传其他附件，稍后再试";
		}else if(msg=="typeDeny"){
			upMsg="不能上传exe,bat,sh,jsp,js,htm,html,sql类型的文件";
		}else if(msg=="fileExceeds"){
			upMsg="单个文件不能超1M";
		}else if(msg=="Over5M"){
			upMsg="多个附件总计不能超5M";
		}	
	}
	function stopStatusChk(message,saveFileName){
		if(message!="finish"){
			loadStatus="error";
			upMsgConv(message);
			$3('upStatusBar').innerHTML = "<div class='tdtxt'><font color='Red'>" + upMsg + "</font></div>";
			$3(saveBtnId).style.display="";
			if(_isFF)
				eraseUpFlag();
			return;
		}
		loadStatus = "finish";
		$3("upStatusBar").innerHTML ="<div class=\"prog-border\"><div class=\"prog-bar\" style=\"width:100%\"></div></div>";
		if(_isFF)
			eraseUpFlag();
		var upFiles=saveFileName.split(" ");
		initFileBk = $3("uploadHidden").value;
		$3("uploadHidden").value = upFiles[1];
		eval(subFun+"()");
		$3(saveBtnId).style.display="";
		$3("uploadText").value="";
		saveBtnId="saveBtn";
	}
	/**
	function fileChk(){
		var allowType ="exe,EXE,BAT,bat,sh,SH,jsp,js,htm,html,sql";
		fileChkMsg = "不能上传exe,bat,sh,jsp,js,htm,html,sql类型的文件";
		var allowTypes = allowType.split(",");
		var form = $3("fileform");
		var elements = form.elements;
		for (i = 0; i < elements.length; ++i) {
      		var element = elements[i];
      		if(element.type == "file"){
      			var fileName = element.value;
      			if(_isIE)//IE下取的是完整路径，要处理一下
      				fileName = fileName.substring(fileName.lastIndexOf("\\")+1);
	      		if(fileName.indexOf(" ")>0||fileName.indexOf(".")<0)
	      			return false;
	      		for(var l=0; l<allowTypes.length; l++){
	      			if(fileName.endWith("."+allowTypes[l])){
	      				return false;
	      			}
	      		}
	      	}
      	}
      	return false;	
	}**/
	var instImg=0
	function fileChk(){
		var allowType ="exe,EXE,BAT,bat,sh,SH,jsp,js,htm,html,sql";
		if(instImg==1){
			allowType="GIF,PNG,JPG,gif,png,jpg";
			fileChkMsg = "只能插入gif,pnp,jpg类型的图片";
		}else
			fileChkMsg = "不能上传exe,bat,sh,jsp,js,htm,html,sql类型的文件";
		var allowTypes = allowType.split(",");
		var form = $3("fileform");
		var elements = form.elements;
		for (i = 0; i < elements.length; ++i) {
      		var element = elements[i];
      		if(element.type == "file"){
      			var fileName = element.value;
      			if(_isIE)//IE下取的是完整路径，要处理一下
      				fileName = fileName.substring(fileName.lastIndexOf("\\")+1);
	      		if(fileName.indexOf(" ")>0||fileName.indexOf(".")<0)
	      			return false;
	      		for(var l=0; l<allowTypes.length; l++){
	      			if(fileName.endWith("."+allowTypes[l])){
	      				if(instImg==1)
	      					return true;
	      				else
	      					return false;
	      			}
	      		}
	      	}
      	}
      	if(instImg==0)
      		return true;	
      	else
      	   	return false;	
	}
	String.prototype.endWith=function(str){  
		 if(str==null||str==""||this.length==0||str.length>this.length){  
		   return false;  
		 }if(this.substring(this.length-str.length)==str){ 
		   return true;  
		 }else{  
		   return false;
		 }
		 return true;  
	};
	//解决上传中文附件名出错问题
	function getFileName(){
		var fileName = $("uploadText").value;
		if(_isIE){
			return fileName.substring(fileName.lastIndexOf("\\")+1);
		}else{
			return  fileName;
		}
	}