﻿	pmBar.attachEvent("onClick", function(id) {
		if($("taskId2").value==""){
			parent.popSwTaskList();
			return;			
		}
		if(id == "ldTree"){
			loadTree();
		}else if(id == "new"){
			addNodeInit('child');
		}else if(id == "switchSt"){
			swState();
		}else if(id == 'assign'){
			popselWin();
		}else if(id == 'back'){
			parent.cusBack();
		}else if(id == 'mup'){
			parent.mypmLayout.items[0]._frame.contentWindow.itemMove('up');
		}else if(id == 'mdown'){
			parent.mypmLayout.items[0]._frame.contentWindow.itemMove('down');
		}else if(id == 'subMod'){
	 		if(treeWin.$("nodeDataStr").value=="0,1,无数据,0,1"){
	 			hintMsg("请先切换到某测试任务");
	 			return;
	 		}
			cfDialog('subOutline','提交测试后不能删除只能停用，您确定');
		}else if(id.length>30&&id!="00000000000000000000000000000000"){
			if($("taskId2").value==id)
				return;
			parent.parent.mypmMain.location=conextPath+"/outLineManager/outLineAction!initList.action?dto.taskId="+id;
		}else if(id=="sw2Task"){
			openSwTaskList();
		}else if(id=="viewCost"){	
			var url = conextPath+"/frameset?__report=report/testReport/cost_prediction.rptdesign&taskId="+$("taskId2").value;
			parent.popViewCost(url);
		}else if(id=="reFreshP"){
			reFreshPeosonP();
		}
	});
	var uGrid;
	var pmGrid;
	//用button标签画用户可看到的操作按钮
	//列表定义及初始化
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
	//两隐匿行分别人，测试人员ID，开发人员ID
    pmGrid.setHeader("&nbsp;,&nbsp;,需求项名称,一级需求KLoc值,&nbsp;,难度系统数,预估用例数,ids,开发人员,ids2,分配人员,预估脚本数,预估场景数,&nbsp;");
    pmGrid.setInitWidths("30,0,130,110,0,80,80,0,*,0,100,80,80,0");
    pmGrid.setColAlign("center,center,left,center,left,center,center,left,left,left,left,center,center,left");
    pmGrid.setColTypes("ch,ro,ro,ed,ro,ed,ed,ro,ro,ro,ro,ed,ed,ro");
    pmGrid.setColSorting("int,int,str,int,int,int,int,str,str,str,str,int,int,int");
	pmGrid.enableAutoHeight(true, 800);
    pmGrid.init();
    pmGrid.enableTooltips("false,false,true,true,false,true,true,false,true,false,true,true,true,false");
    pmGrid.setSkin("light");
    pmGrid.enableRowsHover(true, "red");
    //设置列表数据,listStr为列格数据html元素ID
    var data = "";
    if(data != ""){
	    var jsons = eval("(" + data +")");
	    pmGrid.parse(jsons, "json");
	    reSetRelaVal();
    }	
    function reFreshPeosonP(){
    	var url = conextPath+"/outLineManager/outLineAction!loadPeople.action?dto.currNodeId="+$("currNodeId").value;
        var ajaxResut  =postSub(url, "");
		if(ajaxResut!="failed"){
			pmGrid.clearAll();
		}
	    if(ajaxResut!= ""&&ajaxResut!="failed"){
	    	ajaxResut = ajaxResut.replace(/[\r\n]/g, "");
	    	jsons = eval("(" + ajaxResut +")");
	    	pmGrid.parse(jsons, "json");
	    	parent.mypmLayout.items[1]._frame.contentWindow.reSetRelaVal();
	    }
    }
    function reSetRelaVal(){
		for(var i = 0; i < pmGrid.getRowsNum(); i++){
		    if(pmGrid.cells2(i,4).getValue()=="0"){
				pmGrid.cells2(i,5).setValue("");
				pmGrid.cells2(i,6).setValue("");
				pmGrid.cells2(i,11).setValue("");
				pmGrid.cells2(i,12).setValue("");
				pmGrid.cells2(i,2).setTextColor("blue");
			}
		}    
    }
    var tree; 
    pmGrid.attachEvent("onEditCell",doOnCellEdit); 
    pmGrid.attachEvent("OnCheck",doOnCheck); 
	function doOnCellEdit(stage,rowId,cellInd,nVal,oVal){ 
		if(cellInd==3&&tree.getLevel(rowId)!="2"){
			hintMsg("只有一级功能测试需求项可以设置KLoc值");
			return false;
		}else if(cellInd==3&&pmGrid.cells(rowId,13).getValue()=="1"){
				hintMsg("只有一级功能测试需求项可以设置KLoc值");
				return false;
		}else if(pmGrid.cells(rowId,4).getValue()=="0"&&cellInd==5){
			hintMsg("只有叶子需求项可设置难度系数");
			return false;
		}else if(pmGrid.cells(rowId,4).getValue()=="0"&&cellInd==6){
			hintMsg("只有叶子需求项可设置预估用例数");
			return false;
		}else if(cellInd==11&&pmGrid.cells(rowId,13).getValue()=="0"){
			hintMsg("只有性能测试叶子需求项可设置预估脚本数");
			return false;
		}else if(cellInd==11&&pmGrid.cells(rowId,4).getValue()=="0"){
			hintMsg("只有性能测试叶子需求项可设置预估脚本数");
			return false;
		}else if(cellInd==12&&pmGrid.cells(rowId,13).getValue()=="0"){
			hintMsg("只有性能测试叶子需求项可设置预估场景数");
			return false;
		}else if(cellInd==12&&pmGrid.cells(rowId,13).getValue()=="0"){
			hintMsg("只有性能测试叶子需求项可设置预估场景数");
			return false;
		}
		if(stage==0){
			return true; 
		}else if(stage==1){ 
			return true; 
		}else if(stage==2&&!isNaN(nVal)&&(nVal!=oVal)){
			var url = conextPath+"/outLineManager/outLineAction";
			if(cellInd==3){
				url+="!setKlog.action?dto.klc="+nVal;
			}else if(cellInd==5){
				url+="!setQuotiety.action?dto.klc="+nVal;
			}else  if(cellInd==6){
				url+="!setCaseCount.action?dto.klc="+nVal;
			}else  if(cellInd==11){
				url+="!setScrpCount.action?dto.klc="+nVal;
			}else {
				url+="!setSceneCount.action?dto.klc="+nVal;
			}
			url +="&dto.currNodeId="+rowId;
			var ajaxResut =postSub(url,"");
			if(ajaxResut=="sucess"){
				return true;
			}
			return false;
		}
		return false;
	} 
    function doOnCheck(rowId,cellInd,state){
		this.setSelectedRow(rowId);
		return true;
	}
	//初始化window,在 initW_ch中被调用
	function initCufmsW(){
		cufmsW = new dhtmlXWindows();
		cufmsW.enableAutoViewport(true);
		cufmsW.setViewport(50, 50, 100, 100);
		cufmsW.setImagePath(conextPath+"/dhtmlx/windows/codebase/imgs/");
		return cufmsW;
	}
	function initW_ch(obj, divId, mode, w, h){
		importWinJs();
		if((typeof obj != "undefined") && !obj.isHidden()){
			obj.setDimension(w, h);
			return obj;
		}else if((typeof obj != "undefined") && obj.isHidden()){
			obj.show();
			obj.setDimension(w,h);
		}else{
			if(typeof cufmsW == "undefined"){
				cufmsW = initCufmsW();
			}
			obj = cufmsW.createWindow(divId, 110, 110, w, h);
			if(divId != null){
				obj.attachObject(divId, false);
			}
			hiddenB(obj, mode);
		}
		obj.setModal(mode);
		return obj;
	}
	function cusFormReset(id, focus){
		if(!_isIE){
			$(id).reset();
			$(focus).focus();
			return 
		}
		var form = $(id);
		var elements = form.elements;
		var element;
		for (var i = 0; i < elements.length; i++) {
		 	element = elements[i];
		 	if (element.type == "text"&&( element.name.indexOf("dto.moduleData")==0||element.id.indexOf("_2")>0)){
				element.value="";
		 	}else if (element.id.indexOf("_1")>0){
		 		element.value="1";
		 	}
		}		
		$(focus).focus();
	}
	function addNode2db(closeFlg){
		if(!addCheck()){
			return ;
		}
		if(!speCharChk()){
			$("cUMTxt").innerHTML="输入中不能含特殊字符";
			return;
		}
		var url =conextPath+"/outLineManager/outLineAction!addNodes.action?dto.isAjax=true";
		var ajaxResut = postSub(url, "createForm");
		if(ajaxResut.indexOf("reName_")==0){
			var rest = ajaxResut.split("$")
			var nodes = rest[1]
			treeWin.appendNode(nodes.split(";"));
			$("cUMTxt").innerHTML="需求项:"+rest[0].split("_")[1] +" 同级重名";
			return false;
		}
		if(ajaxResut!="failed"&&ajaxResut!=""){
			$("cUMTxt").innerHTML="操作成功";
			var treeArr = ajaxResut.split(";");
			treeWin.appendNode(treeArr);
			if($("command").value=="addchild"){
				for(var i=0 ;i<treeArr.length; i++){
					var nodeInfo = treeArr[i].split(",");
					//只插入新增加的记录
					var relaVals = getRelaVal(nodeInfo[2]);
					if(pmGrid.getRowIndex(nodeInfo[1])<0){
						var rowDa = "0,,"+nodeInfo[2]+",,1,"+relaVals.split(";")[0]+",,,,,"+relaVals.split(";")[1]+","+$("requireType").value;
						pmGrid.addRow(nodeInfo[1], rowDa);
					}
				}			
			}
			cusFormReset("createForm", "module1");
			$("command").value="addchild";
			$("requireType").value = "0";
			$("funRd").checked = true;
			adjustCreTable("fun");
			if(typeof closeFlg !="undefined"){
				cuW_ch.setModal(false);
				cuW_ch.hide();
			}
		}else{
			$("cUMTxt").innerHTML="操作失败";
		}
	}
	function getRelaVal(val){
		var form = $("createForm");
		var elements = form.elements;
		var element;
		for (var i = 0; i < elements.length; i++) {
		 	element = elements[i];
		 	var value= element.value.replace(/(^\s*)|(\s*$)/g, "");
		 	if (element.id.indexOf("_")<0&&element.type == "text"&& value!=""){
				if(val==value){
					return $(element.id+"_1").value +","+$(element.id+"_2").value+";" +$(element.id+"_3").value +","+$(element.id+"_4").value;
				}
		 	}
		}	
	}
	function addCheck(){
		if(!blankCheck()){
			return false;
		}else if(!checkReName()){
			return false;
		}
		return true;
	}
	function blankCheck(){
		var form = $("createForm");
		var elements = form.elements;
		var element;
		for (var i = 0; i < elements.length; i++) {
		 	element = elements[i];
		 	if (element.id.indexOf("_")<0&&element.type == "text"&& ((element.value.replace(/(^\s*)|(\s*$)/g, ""))!="")){
				return true;
		 	}
		}
		$("cUMTxt").innerHTML="致少输入一需求项";
		return false;	
	}
	
	function checkReName(){
		var form = $("createForm");
		var elements = form.elements;
		var element;
		var valuesStr ="";
		for (var i = 0; i < elements.length; i++) {
		 	element = elements[i];
		 	var value= element.value.replace(/(^\s*)|(\s*$)/g, "");
		 	if (element.id.indexOf("_")<0&&element.type == "text"&& value!=""){
				valuesStr = valuesStr +","+value;
		 	}
		}
		valuesStr = valuesStr.substring(1);
		var valuesArr = valuesStr.split(",");
		var pId = $("parentNodeId").value;
		if($("command").value=="addchild"){
			pId=$("currNodeId").value;
		}
		var broNodes = tree.getSubItems(pId);
		var broNodeArr = broNodes.split(",");
		for(var i=0; i<valuesArr.length; i++ ){
			var nName = valuesArr[i];
			//先校验当前输入的相互有无重名
			for(var l=i+1; l<valuesArr.length; l++){
				if(nName==valuesArr[l]){
					$("cUMTxt").innerHTML="需求项:" +nName +" 重名";
					return false ;
				}
			}
			//再校验树中兄弟节点有无重名
			var broName = "";
			for(var h=0;h<broNodeArr.length; h++){
				broName = tree.getItemText(broNodeArr[h]);
				if(nName==broName){
					$("cUMTxt").innerHTML="需求项:" +nName +" 同级重名";
					return false ;
				}				
			}
		}
		//以后和数据库中兄弟节点校验重名(因为有并发，所以如此)
		return true;
	}
	function adjustCreTable(val){
		var tb = $("creTable");
		var rows=tb.rows;
        for(var i=0;i<rows.length;i++){
        	var cells=rows[i].cells;
            for(var j=0;j<cells.length;j++){
            	if(j>=(cells.length-2)){
            		if(val=="fun")
            		  cells[j].style.display="none";
            		else
            		  cells[j].style.display="";
            	}
            }
        }
   		if(val=="fun")
   		  cuW_ch.setDimension(390, 320);
   		else
   		  cuW_ch.setDimension(470, 320);
	}
	var first = 0 ;
	function addNodeInit(command){
	    if($("taskId").value==""){
 			hintMsg("请先切换到某测试项目");
 			return;	    
	    }
 		if(parent.mypmLayout.items[0]._frame.contentWindow.$("nodeDataStr").value=="0,1,无数据,0,1"){
 			hintMsg("请先切换到某测试项目");
 			return;
 		}
		if($("currNodeId").value==""){
			parent.mypmLayout.items[0].expand();
			hintMsg("请选择一需求项");
			return;
		}
		if(parent.mypmLayout.items[0]._frame.contentWindow.tree.getLevel($("currNodeId").value)==99){
			hintMsg("需求项级别不能大于100");
			return;		
		}
		parent.mypmLayout.items[0]._frame.contentWindow.recNormal();
		cuW_ch = initW_ch(cuW_ch, "createDiv", true, 390, 320);
		$("cUMTxt").innerHTML="&nbsp;";
		if(first==0){
			$("cUMTxt").innerHTML="一次可批量增加1到10个需求项";
		}
		cuW_ch.setText("增加需求项"+"--当前需求项:"+tree.getItemText($("currNodeId").value));
		cusFormReset("createForm", "module1");
		$("command").value="addchild";
		$("addchildRd").checked=true;
		first ++;
	}	
	function swState(){
		if($("currNodeId").value==""){
			hintMsg("请选择一需求项");
			return;
		}
		$("command").value = "swState";
		var msg = "";
		if($("moduleState").value==0){
			msg = "停用的需求项将不执行测试，您确定?";
		}else{
			msg = "您确定要启用选择的需求项？";
		}
		parent.mypmLayout.items[0]._frame.contentWindow.tree.stopEdit();	
		cfDialog("swStateExe",msg);
	}
	function swStateExe(){
		var nId = $("currNodeId").value;
		var url =conextPath+"/outLineManager/outLineAction!switchState.action?dto.isAjax=true";
		var ajaxResut = postSub(url, "createForm");
		if(ajaxResut!="failed"&&ajaxResut!=""){
			if($("moduleState").value==0){
				pmBar.setItemToolTip("switchSt", "启用");
				$("moduleState").value=1;
				tree.setItemColor(tree.getSelectedItemId(),'#ff0000','#ff0000');
			}else{
				pmBar.setItemToolTip("switchSt", "停用");
				$("moduleState").value=0;
				tree.setItemColor(tree.getSelectedItemId(),'black','white');
			}
			tree.setUserData(nId,"MyState",$("moduleState").value);	
			subArr = tree.getAllSubItems(nId).split(",");
			for(var i=0 ;i<subArr.length; i++){
				if($("moduleState").value==1){
					tree.setItemColor(subArr[i],'#ff0000','#ff0000');
					tree.setUserData(subArr[i],"MyState",1);	
				}else{
					tree.setItemColor(subArr[i],'black','white');
					tree.setUserData(subArr[i],"MyState",0);					
				}
			}
			clsoseCfWin();
			return true;
		}else{
			$("mText").innerHTML = "操作失败" ;
			return false;
		}	
	}


	function getAllParent(itemId,p){
		pId = tree.getParentId(itemId);
		if(pId==0){
			return p;
		}else{
			p+= ","+pId +getAllParent(pId,p)	
		}
		return p;
	}
	function getMyAllParent(id){
		return getAllParent(id,"").substring(1);
	}
	function subOutline(){
		var url = conextPath+"/outLineManager/outLineAction!submitModule.action?dto.isAjax=true&dto.taskId="+$("taskId").value;
		var ajaxResut  = postSub(url, "");
		if(ajaxResut=="sucess"){
			hintMsg("测试需求提交成功");
			pmBar.disableItem("subMod");
		}else{
			hintMsg("操作失败，稍后再试");
		}
	}
	function getChecked(){
		selectItems = "";
		var allItems = pmGrid.getAllItemIds();
		var items = allItems.split(',');
		for(var i = 0; i < items.length; i++){
			if(items[i] != "" && pmGrid.cells(items[i], 0).getValue() == 1){
				if (selectItems == ""){
					selectItems = items[i];
				}else{
					selectItems += "," + items[i];
				}
			}
		}
		return selectItems;
	}
	function popselWin(){
		var modIds = getChecked();
		if(modIds==""){
			hintMsg("请从表格中选择需求项");
			return;
		}
		suW_ch = initW_ch(suW_ch, "selPeopleDiv", true, 400, 330);
		suW_ch.setText("人员分配");
		initSelGrid();
		initSleEdPeople(modIds,'NoFresh');
		suW_ch.bringToTop();
	}
	var selIngDevPerson="",selIngDevLPerson="",loadType="5";
	function reLoadPeopleData(loadFlg){
		if(loadFlg=="5"){
			loadType="5";
			initSleEdPeople(getChecked(),'NoFresh');
		}else if(loadFlg=="4"){
			loadType="4";
			initSleEdPeople(getChecked(),'NoFresh');
		}
	}
	function getAssChgNid(rowId,valN){
		var colIndex = 7;
		if(loadType=="4")
		 	 colIndex = 9;
		var rowArr = rowId.split(",");
		var nIds = "";
		for(var i=0; i<rowArr.length; i++){
			if(pmGrid.cells(rowArr[i], colIndex).getValue()!=valN){
				nIds = nIds + ","+rowArr[i];
			}	
		}
		nIds = nIds.substring(1);
		return nIds;
	}
	function initSleEdPeople(modIds,freshFlg){
		//处理可选人员
		if(((loadType=="5"&&selIngDevPerson==""&&freshFlg=="NoFresh")||(loadType=="4"&&selIngDevLPerson==""&&freshFlg=="NoFresh"))||freshFlg=='fresh'){
			var url = conextPath+"/outLineManager/outLineAction!selectMember.action?dto.taskId="+$("taskId").value+"&dto.reqType="+loadType;
			var ajaxResut =postSub(url, "");
			selGrid.clearAll();
			ajaxResut = ajaxResut.split("$")[1];
		    if(ajaxResut != ""&&ajaxResut!="failed"){
	   			ajaxResut = ajaxResut.replace(/[\r\n]/g, "");
	  			selGrid.parse(eval("(" + ajaxResut +")"), "json");
	  			if(loadType=="5")
	  				selIngDevPerson=ajaxResut;
	  			else
	  				selIngDevLPerson=ajaxResut;
	  		}else if(ajaxResut=="failed"){
	  			hintMsg("加载数据发生错误");
	  			return;
	  		}
	  	}else{
	  		selGrid.clearAll();
	  		if(loadType=="5")
	  			selGrid.parse(eval("(" + selIngDevPerson +")"), "json");
	  		else
	  			selGrid.parse(eval("(" + selIngDevLPerson +")"), "json");
	  	}
	  	if(freshFlg=='fresh'){
	  		for(var rowNum=0;rowNum<seledGrid.getRowsNum();rowNum++){
	  			selGrid.deleteRow(seledGrid.getRowId(rowNum));
	  		}
	  		return;
	  	}
	  	seledGrid.clearAll();
  		//只选择一模快时，要从可选中删除己选，并在己选中列出己选
  		if(modIds.indexOf(",")<0){
  			var colIndex = 7;
	  		if(loadType=="4")
	  			 colIndex = 9;
			var seledPeopeIds = pmGrid.cells(modIds, colIndex).getValue();
			var seledPeopeNames = pmGrid.cells(modIds, (colIndex+1)).getValue();
			if(""==seledPeopeIds){
				return ;
			}
			peopleIdArr = seledPeopeIds.split("_");
			peopleNameArr = seledPeopeNames.split(" ");
			if(freshFlg!='fresh'){
	  			seledGrid.clearAll();
	  		}
			for(var i=0; i<peopleIdArr.length; i++){
				seledGrid.addRow(peopleIdArr[i],"0,,"+peopleNameArr[i]);
				selGrid.deleteRow(peopleIdArr[i]);
			}  		
  		}
  			
	}
	function initSelGrid(){
	    if(typeof selGrid == "undefined"){
		    selGrid = new dhtmlXGridObject('selGridbox');
			seledGrid = new dhtmlXGridObject('seledGridbox');
			selGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
			seledGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
			selGrid.setHeader(",,备选人员--<span style='font-size:11px'>双击选择</span>");
			selGrid.setInitWidths("0,0,*");
	    	selGrid.setColAlign("left,left,left");
	    	selGrid.setColTypes("ch,ro,ro");
	   		selGrid.setColSorting("Str,int,str");
	   		selGrid.enableTooltips("false,false,true");
	    	selGrid.enableAutoHeight(true, 240);
	    	selGrid.setMultiselect(true);
	        selGrid.init();
	    	selGrid.setSkin("light");
		    seledGrid.setHeader(",,己选人员--<span style='font-size:11px'>双击取消</span>");
			seledGrid.setInitWidths("0,0,*");
	    	seledGrid.setColAlign("left,left,left");
	    	seledGrid.setColTypes("ch,ro,ed");
	   		seledGrid.setColSorting("Str,int,str");
	   		seledGrid.enableTooltips("false,false,true");
	   		seledGrid.enableAutoHeight(true, 240);
	   		seledGrid.setMultiselect(true);
	        seledGrid.init();
	    	seledGrid.setSkin("light");	
	    	seledGrid.attachEvent("onRowDblClicked",function(rowId,cellInd,state){
				selGrid.addRow(rowId,"0,,"+seledGrid.cells(rowId,2).getValue(),0);
				seledGrid.deleteRow(rowId);
				seledGrid.setSizes();
				//return true;
			});
	    	selGrid.attachEvent("onRowDblClicked",function(rowId,cellInd,state){
				seledGrid.addRow(rowId,"0,,"+selGrid.cells(rowId,2).getValue(),0);
				selGrid.deleteRow(rowId);
				selGrid.setSizes();
				//return true;
			});
	    }
	}
	function selPeople(){
		var uids = "";
		var names = "";
		if(seledGrid.getRowsNum()!=0){
			var allItems = seledGrid.getAllItemIds();
			var items = allItems.split(',');
			for(var i=0; i<items.length;i++){
				uids = uids + "_"+items[i];
				names = names + " "+ seledGrid.cells(items[i], 2).getValue();
			}
			uids = uids.substring(1);
			names = names.substring(1);
		}
		if(uids==""){
			hintMsg("请选择人员");
			return ;
		}
		$("userIds").value = uids;
		var rowId = getChecked();
		var chgNid = getAssChgNid(rowId,uids);
		$("assignNIds").value = chgNid;
		if(chgNid==""){
			suW_ch.hide();
			suW_ch.setModal(false);
			return true;		
		}
		var url = conextPath+"/outLineManager/outLineAction!assignPeople.action?dto.taskId="+$("taskId").value+"&dto.reqType="+loadType;
		var ajaxResut  =postSub(url, "assignForm");
		if(ajaxResut=="sucess"){
			var nidArr = chgNid.split(",");
			 var colIndex = 7;
	  		if(loadType=="4")
	  			 colIndex = 9;
			for(var i=0; i<nidArr.length; i++){
				pmGrid.cells(nidArr[i], colIndex).setValue(uids);
				pmGrid.cells(nidArr[i], (colIndex+1)).setValue(names);
			}
			suW_ch.hide();
			suW_ch.setModal(false);
			return true;
		}else{
			hintMsg("分配人员生发错误");
		}
	}
	function delNodeExe(){
		if(treeWin.delNodeExe()){
			 clsoseCfWin();
		}
	}
  function speCharChk(){
	var form = document.getElementById("createForm");
    var elements = form.elements;  
    for (i = 0; i < elements.length; ++i) {
      var element = elements[i];
      if(element.type == "text" || element.type == "textarea" || element.type == "hidden"){
      	if(includeSpeChar(element.value)){
      		return false;
      	}
      }
     }
    return true;
  }
  function includeSpeChar(StrVal){
  	var speStr="~ ` ! # $ % ^ & * ( ) [ ] { } ; ' : \" , ， < >";
  	var speStrArr = speStr.split(" ");
  	if(typeof(StrVal)=="undefined"||isWhitespace(StrVal))
  		return false;
  	for(var i=0; i<speStrArr.length; i++){
  		if(StrVal.indexOf(speStrArr[i])>=0){
  			return true;
  		}
  	}
  	return false;
  }
	function clearNoNum(obj){
		//先把非数字的都替换掉，除了数字和.
		obj.value = obj.value.replace(/[^\d.]/g,"");
		//必须保证第一个为数字而不是.
		obj.value = obj.value.replace(/^\./g,"");
		//保证只有出现一个.而没有多个.
		obj.value = obj.value.replace(/\.{2,}/g,".");
		//保证.只出现一次，而不能出现两次以上
		obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
	}
 