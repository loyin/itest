	var rootId = "";
	popContextMenuFlg=0;
	tree=new dhtmlXTreeObject('treeBox',"100%","100%",0); 
	tree.setImagePath(conextPath+"/dhtmlx/dhtmlxTree/codebase/imgs/");
	tree.enableCheckBoxes(true);
	//tree.enableThreeStateCheckboxes(true);
	tree.enableHighlighting(1);
	tree.setImageArrays("plus","plus2.gif","plus3.gif","plus4.gif","plus.gif","plus5.gif");
	tree.setImageArrays("minus","minus2.gif","minus3.gif","minus4.gif","minus.gif","minus5.gif");
	tree.setStdImages("book.gif","books_open.gif","books_close.gif");
	var mytreeArr = $("nodeDataStr").value.split(";");
	var topNode = "";
	for(var i=0 ;i<mytreeArr.length;i++){
		var nodeInfo = mytreeArr[i].split(",");
		tree.insertNewItem(nodeInfo[0],nodeInfo[1],nodeInfo[2],0,0,0,0,"");
		if(nodeInfo[3]=="0"&& i>0){
			tree.setUserData(nodeInfo[1],"blankCh",nodeInfo[1]+"null");
			tree.insertNewChild(nodeInfo[1],nodeInfo[1]+"null","",0,0,0,0,""); 
			tree.closeItem(nodeInfo[1]);
		}else if(i==0){
			rootId = nodeInfo[1];
		}
	}	
	tree.selectItem(rootId,false,false);
	$("currNodeId").value=rootId;
	tree.setOnOpenEndHandler(onopenEndHdl);
	var nameIncPath;
	function dbClickHdl(id){
		return true;
	}
	function appendNode(treeArr){
		var topNode = "";
		for(var i=0 ;i<treeArr.length;i++){
			var nodeInfo = treeArr[i].split(",");
			topNode = nodeInfo[0];
			if(i==0){
				var blankId = tree.getUserData(nodeInfo[0],"blankCh");
				var haveBlank = typeof blankId=="undefined" ;
				if(!haveBlank){
					tree.deleteItem(blankId,false);
					tree.setUserData(topNode,"blankCh");
				}
			}
			tree.insertNewItem(nodeInfo[0],nodeInfo[1],nodeInfo[2],0,0,0,0,"");
			tree.setUserData(nodeInfo[1],"MyState",nodeInfo[4]);
			if(nodeInfo[4]==1){
				tree.setItemColor(nodeInfo[1],'#ff0000','#ff0000');
			}
			if(nodeInfo[3]=="0"){
				tree.setUserData(nodeInfo[1],"blankCh",nodeInfo[1]+"null");
				tree.insertNewChild(nodeInfo[1],nodeInfo[1]+"null","",0,0,0,0,""); 
				tree.closeItem(nodeInfo[1]);
			}
		}
	}
	function loadChildren(itemId){
		var url = conextPath+"/bugManager/bugManagerAction!loadReportTree.action";
		url = url+"?dto.taskId="+$("taskId").value +"&dto.currNodeId="+itemId;
		var ajaxResut = postSub(url,"");
		if(ajaxResut!="failed"&&ajaxResut!=""){
			var treeArr = ajaxResut.split(";");
			appendNode(treeArr);
		}
		if(ajaxResut==""){
			var blankId = tree.getUserData(itemId,"blankCh");
			tree.deleteItem(blankId,true);
			tree.setUserData(itemId,"blankCh");
		}				
	}
	function onopenEndHdl(id,mode){
		if(mode<0){
			return ;
		}
		var blankId = tree.getUserData(id,"blankCh");
		var haveBlank = typeof blankId=="undefined" ;
		if(!haveBlank){
			loadChildren(id);
		}
	}

	function veiwReport(){
		var seledNodes =tree.getAllChecked();
		var nodeArr= seledNodes.split(",");
		if(seledNodes==""){
			hintMsg("请选择测试需求项");
			return;
		}
		if(nodeArr.length>20){
			hintMsg("最多可选20个测试需求项");
			return;
		}
		var url = conextPath+"/frameset?__report=report/testReport/" + parent.$("repTemplet").value + ".rptdesign&taskId="+$("taskId").value+"&moduleIds="+seledNodes;
		parent.moduleTreeW_ch.setModal(false);
		parent.moduleTreeW_ch.hide();
		parent.parent.mypmLayout.items[1].attachURL(url);
		parent.parent.parameterW_ch.hide();
		parent.pmGrid.cells(parent.$("reportTaskId").value, 0).setValue(false);
	}