
<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
		<TITLE>测试项目</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<link rel='STYLESHEET' type='text/css'href='<%=request.getContextPath()%>/css/page.css'>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>			
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>			
	</HEAD>
	<BODY bgcolor="#ffffff">
		<table width="100%" cellspacing="0" cellpadding="0">
		   <tr>
				<td valign="top" style="padding-right: 3px;">
					<div id="repDiv"  class="cycleTask gridbox_light" style="border:0px;display:none;">
					  <div class="objbox" style="overflow:auto;width:100%;">
						<ww:form theme="simple" method="post" id="reportForm" name="reportForm"
							namespace="/singleTestTask" action="">
							<ww:hidden id="repTemplet" name="dto.repTemplet"></ww:hidden>
							<input type="hidden" id="reportTaskId" name="reportTaskId" value=""/>
							<input type="hidden"   id ="factStartDate" name="factStartDate" value=""/>
							<input type="hidden"   id ="factEndDate" name="factEndDate" value=""/>
							<ww:hidden id="dateChk" name="dto.dateChk"></ww:hidden>
							<table class="obj row20px" cellspacing="0" cellpadding="0" width="100%">
								<tr class="odd_mypm">
									<td align="right" class="rightM_center">项目编号:</td>
									<td style="border-right:0;" class="dataM_left">
										<ww:textfield id="proNum_rep" name="dto.singleTest.proNum"
											cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"></ww:textfield>
									</td>
									<td align="right" class="rightM_center">项目名称:</td>
									<td style="border-right:0;" class="dataM_left">
										<ww:textfield id="proName_rep" name="dto.singleTest.proName"
											cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"></ww:textfield>
									</td>
								</tr>
								<tr class="ev_mypm">
									<td align="right" class="rightM_center" style="color:red;">起始日期:</td>
									<td style="border-right:0;" class="dataM_left">
										<ww:textfield id="startDate" name="startDate"
											cssStyle="width:160;padding:2 0 0 4;" cssClass="text_c" readonly="true" onclick="showCalendar(this);"></ww:textfield>
									</td>
									<td align="right" class="rightM_center" style="color:red;">终止日期:</td>
									<td style="border-right:0;" class="dataM_left">
										<ww:textfield id="endDate" name="endDate"
											cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c" onclick="showCalendar(this);"></ww:textfield>
									</td>
								</tr>
								<tr class="odd_mypm">
									<td colspan="4" align="right">
										<a class="bluebtn" href="javascript:void(0);" id="quBtn"onclick="javascript:parent.parameterW_ch.setModal(false);parent.parameterW_ch.hide();"
										style="margin-left: 6px;"><span> 返回</span> </a>
										<a class="bluebtn" href="javascript:void(0);" id="quU_b"onclick="loadReport();"
											style="margin-left: 6px;"><span> 查看报表</span> </a>
										<a class="bluebtn" href="javascript:void(0);" id="qretBtn"onclick="reSetRepPra()"
										style="margin-left: 6px;"><span> 重置</span> </a>
									</td>
								</tr>
								<tr class="ev_mypm">
									<td align="right" class="rightM_center">日期范围限制:</td>
									<td style="border-right:0;" class="dataM_left" colspan="3" id="dateLimetTd">
										<div id="cUMTxt" align="center" style="color: Red; padding: 2px">
						     			&nbsp;
					                    </div>
									</td>

								</tr>
							</table>
						</ww:form>
					</div>
					</div>
		     </td>		   
		   </tr>
			<tr>
				<td valign="top" style="padding-right: 3px;">
					<div id="toolbarObj"></div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<div id="gridbox"></div>
				</td>
			</tr>
		</table>
		<input type="hidden" id="listStr" name="listStr" value="${dto.listStr}"/>
		<ww:hidden id="reProStep" name="reProStep"></ww:hidden>
	<script type="text/javascript">
		parent.paraWinObj = window;
		parent.parameterW_ch.setDimension(750, 540);
		if(window.screen.width>=1152)
			parent.parameterW_ch.setPosition(355, 20);
		else
			parent.parameterW_ch.setPosition(240, 20);
		if($("dateChk").value !=""){
			$("repDiv").style.display=""; 
		}else{
			var initText = parent.parameterW_ch.getText();
			parent.parameterW_ch.setText(initText+"----请选择项目");			
		}
		var pmBar = new dhtmlXToolbarObject("toolbarObj");
		pmBar.setIconsPath(conextPath+"/dhtmlx/toolbar/images/");
		pmBar.addButton("find",1 , "", "search.gif");
		pmBar.setItemToolTip("find", "查询");
		pmBar.addButton("reFreshP",2 , "", "page_refresh.gif");
		pmBar.setItemToolTip("reFreshP", "刷新页面");
		pmBar.addButton("first",3 , "", "first.gif", "first.gif");
		pmBar.setItemToolTip("first", "第一页");
		pmBar.addButton("pervious",4, "", "pervious.gif", "pervious.gif");
		pmBar.setItemToolTip("pervious", "上一页");
		pmBar.addSlider("slider",5, 80, 1, 30, 1, "", "", "%v");
		pmBar.setItemToolTip("slider", "滚动条翻页");
		pmBar.addButton("next",6, "", "next.gif", "next.gif");
		pmBar.setItemToolTip("next", "下一页");
		pmBar.addButton("last",7, "", "last.gif", "last.gif");
		pmBar.setItemToolTip("last", "末页");
		pmBar.addInput("page",8, "", 25);
		pmBar.addText("pageMessage",9, "");
		pmBar.addText("pageSizeText",10, "每页");
		var opts ;
		if($("dateChk").value =="")
			opts = Array(Array('id1', 'obj', '10'), Array('id2', 'obj', '15'), Array('id2', 'obj', '20'));
		else
		    opts = Array(Array('id1', 'obj', '10'), Array('id2', 'obj', '15'));
		pmBar.addButtonSelect("pageP",11, "page", opts);
		pmBar.setItemToolTip("pageP", "每页记录数");
		pmBar.addText("pageSizeTextEnd",12, "条");
		pmBar.setListOptionSelected('pageP','id1');pmBar.addSeparator("sep_swTask2",14); 
		pmBar.attachEvent("onClick", function(id) {
			if(id =="reFreshP"){
		   pageAction(pageNo, pageSize);
			}
		});
	</script>
	<script type="text/javascript"src="<%=request.getContextPath()%>/jsp/singleTestTaskManager/swTestTask4Report.js"></script>
		<div id="findDiv"  class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="findForm" name="findForm"
				namespace="/singleTestTask" action="">
			
				<table class="obj row20px" cellspacing="0" cellpadding="0" width="100%">
					<tr class="ev_mypm">
						<td colspan="4" style="border-right:0;width:340">&nbsp;</td>
					</tr>
					<tr class="odd_mypm" >
						<td align="right" class="rightM_center">项目编号:</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="proNum_f" name="dto.singleTest.proNum"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
						<td align="right" class="rightM_center">项目名称:</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="proName_f" name="dto.singleTest.proName"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td align="right" class="rightM_center" style="border-right:0;">研发部门:</td>
						<td style="border-right:0;">
							<ww:textfield id="devDept_f" name="dto.singleTest.devDept"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
						<td align="right" style="border-right:0;" class="rightM_center">状态:</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:select id="status_f" name="dto.singleTest.status"
								list="#{-1:'',0:'进行',1:'完成',2:'结束',2:'准备'}" headerValue="-1" cssStyle="width:120;" cssClass="text_c">
							</ww:select>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" class="dataM_left" style="border-right:0;width:340">&nbsp;</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" align="right">
							<a class="bluebtn" href="javascript:void(0);" id="quBtn"onclick="javascript:fW_ch.setModal(false);fW_ch.hide();"
							style="margin-left: 6px;"><span> 返回</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="quU_b"onclick="findExe();"
								style="margin-left: 6px;"><span> 查询</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="qretBtn"onclick="formReset('findForm');"
							style="margin-left: 6px;"><span> 重置</span> </a>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4">					

					    </td>
					</tr>
				</table>
			</ww:form>
		</div>
		</div>
	<ww:include value="/jsp/common/dialog.jsp"></ww:include>
   <link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/calendar/codebase/dhtmlxcalendar.css">	
   <script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/calendar/codebase/dhtmlxcalendar.js"></script>	
	</BODY>

</HTML>
