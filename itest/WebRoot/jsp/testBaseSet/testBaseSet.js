	contin='0';
	pmBar.attachEvent("onClick", function(id){
		if(id =="new"){
	    	addUpInit("new");
	   }else if(id =="upd"){
	   		if(pmGrid.getSelectedId()==null){
	   			hintMsg("请选择要修改的记录");
	   			return;
	   		}
	   		if(pmGrid.cells(pmGrid.getSelectedId(),6).getValue()=='1'){
	   			hintMsg("缺省数据不能修改");
	   			return;	   		
	   		}
	    	addUpInit("upd");
	   }else if(id =="del"){
	   		if(pmGrid.getSelectedId()==null){
	   			hintMsg("请选择要删除的记录");
	   			return;
	   		}
	   		if(pmGrid.cells(pmGrid.getSelectedId(),6).getValue()=='1'){
	   			hintMsg("缺省数据不能删除");
	   			return;	   		
	   		}
	   		cfDialog("delExe","您确定删除选择的记录?",false);	    	
	   }else if(id =="swStatus"){
	   		if(pmGrid.getSelectedId()==null){
	   			hintMsg("请选择要切换状态的记录");
	   			return;
	   		}
	   		//if(pmGrid.cells(pmGrid.getSelectedId(),6).getValue()=='1'){
	   		//	hintMsg("缺省数据不能切换状态");
	   		//	return;	   		
	   		//}
	   		if(pmGrid.cells(pmGrid.getSelectedId(), 4).getValue()=="启用")
	   			cfDialog("swStatusExe","您确定停用选择的记录?",false);	
	   		else
	   		  	cfDialog("swStatusExe","您确定启用选择的记录?",false);	   
	   }else if(id == "first"){
			pageAction(1, pageSize);
		}else if(id == "last"){
			pageAction(pageCount, pageSize);
		}else if(id == "next"){
			pageAction(pageNo + 1, pageSize);
		}else if(id == "pervious"){
			pageAction(pageNo -1, pageSize);
		}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4"){
			var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
			if(pageSize==pageSizeTemp){
				return;
			}
			pageSize = pageSizeTemp;
			var url = conextPath+"/testBaseSet/testBaseSetAction!testBaseSetList.action?dto.subName="+pmBar.getListOptionSelected('testBaseList');
			url = url +"&dto.pageNo="+ pageNo +"&dto.pageSize=" + pageSize;
			var userJson = dhtmlxAjax.postSync(url, "").xmlDoc.responseText.split("$");
	   		pmGrid.clearAll();
	   		try{
		   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
		   		setPageNoSizeCount(userJson[0]);
		   		setRowNum(pmGrid);
		   		reSetColor();
	   		}catch(e){}	
		}else if(pmBar.isListOptionEnabled("testBaseList", id)){
			var url = conextPath+"/testBaseSet/testBaseSetAction!testBaseSetList.action";
			url = url +"?dto.pageNo="+ pageNo +"&dto.pageSize=" + pageSize;
			$("sw2TypeId").value= id;			
			var userJson = dhtmlxAjax.postSync(url, "swTypeForm").xmlDoc.responseText.split("$");
	   		pmGrid.clearAll();
	   		try{
		   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
		   		setPageNoSizeCount(userJson[0]);
		   		setRowNum(pmGrid);
		   		reSetColor();
	   		}catch(e){}	
	   		pmBar.setItemText('testBaseList', pmBar.getListOptionText('testBaseList', id));	
		}else if(id == "reFreshP"){
			var url = conextPath+"/testBaseSet/testBaseSetAction!testBaseSetList.action?dto.subName="+pmBar.getListOptionSelected('testBaseList');
			url = url +"&dto.pageNo="+ pageNo +"&dto.pageSize=" + pageSize;
			var userJson = dhtmlxAjax.postSync(url, "").xmlDoc.responseText.split("$");
	   		pmGrid.clearAll();
	   		try{
		   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
		   		setPageNoSizeCount(userJson[0]);
		   		setRowNum(pmGrid);
		   		reSetColor();
	   		}catch(e){}	
		}
	});
	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		opreType="query";
		pageAction(pageNo, pageSize);
		return;
	});
	pmBar.attachEvent("onEnter", function(id, value) {
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
		return;
	});
	
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
    pmGrid.setHeader("&nbsp;,序号,数据类型,数据名称,状态,备注,&nbsp;");
    pmGrid.setInitWidths("40,40,100,160,60,"+nameWidth+",0");
    pmGrid.setColAlign("center,center,left,left,center,left,left");
    pmGrid.setColTypes("ra,ro,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,str,str,str,str,str,str");
	//pmGrid.attachEvent("onCheckbox",doOnCheck);
	pmGrid.enableAutoHeight(true, 700);
    pmGrid.init();
    pmGrid.enableRowsHover(true, "red");
    pmGrid.enableTooltips("false,true,true,true,true,true");
    pmGrid.setSkin("light");
    initGrid(pmGrid,"listStr");
    reSetColor();
    pmGrid.attachEvent("OnCheck",doOnCheck);
    pmGrid.attachEvent("onRowSelect",doOnSelect);  
    function doOnCheck(rowId,cellInd,state){
		this.setSelectedRow(rowId);
		var MyState = pmGrid.cells(rowId, 4).getValue();
		if(MyState=='启用'){
			pmBar.setItemToolTip("swStatus", "停用");
			pmBar.setItemImage("swStatus", "disable.png");
		}else{
			pmBar.setItemToolTip("swStatus", "启用");
			pmBar.setItemImage("swStatus", "enable.png");
		}
		return true;
	}
    function doOnSelect(rowId,index){
		pmGrid.cells(rowId, 0).setValue(true);
		pmGrid.setSelectedRow(rowId);
		var MyState = pmGrid.cells(rowId, 4).getValue();
		if(MyState=='启用'){
			pmBar.setItemToolTip("swStatus", "停用");
			pmBar.setItemImage("swStatus", "disable.png");
		}else{
			pmBar.setItemToolTip("swStatus", "启用");
			pmBar.setItemImage("swStatus", "enable.png");
		}
	}


	function pageAction(pageNo, pageSize){
		if(pageNo>pageCount && pageSizec<1){
			pmBar.setValue("page", pageNo);
			return ;
		}
		var url = conextPath+"/testBaseSet/testBaseSetAction!testBaseSetList.action?dto.pageNo="+ pageNo +"&dto.pageSize=" + pageSize;
		var userJson = dhtmlxAjax.postSync(url, "").xmlDoc.responseText.split("$");
		if(userJson[1] == ""){
			hintMsg("没查到相关记录");
		}else{
	   		pmGrid.clearAll();
	   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
	   		setPageNoSizeCount(userJson[0]);
	   		setRowNum(pmGrid);
	   		reSetColor();
   		}

	}	
	function reSetColor(){
		for(var i = 0; i<pmGrid.getRowsNum(); i++){
			if(pmGrid.cells2(i,6).getValue()=="1"){
				pmGrid.cells2(i,3).setTextColor("blue");
			}		
		}	
	}
	function formRest(){
		contin="0";
		$("typeId").value="";
		$("isDefault").value="";
		$("status").value="";
		$("typeName").value="";
		$("remark").value="";
		$("cUMTxt").innerHTML="";
	}
	function addUpInit(id){
		formRest();
		$("subName").options[0].selected = true;
		$("cUMTxt").innerHTML="";
		if(id=="upd"){
			$("saveBtnc").style.display="none";
			var url = conextPath+"/testBaseSet/testBaseSetAction!updInit.action?dto.testBaseSet.typeId="+pmGrid.getSelectedId();
			var rest= dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
			if(rest=="failed"){
				hintMsg("加载数据发生错误");
				return;
			}else if(rest=="NotFind"){
				hintMsg("所选记录己不存在");
				return;
			}else if(rest.indexOf("success")>=0){
				setUpInfo(rest.split("$")[1]);
			}
		}else{
			$("isDefault").value="0";
			$("status").value="1";	
			var currType = pmBar.getListOptionSelected("testBaseList");	
			var subCount = $("subName").options.length;
			for(var i=0; i<subCount; i++){
				if($("subName").options[i].value==currType)
					$("subName").options[i].selected = true;
			}
		}
		cuW_ch = initW_ch(cuW_ch, "createDiv", true, 430, 130);
		cuW_ch.button("close").attachEvent("onClick", function(){
			cuW_ch.setModal(false);
			cuW_ch.hide();
		});

		if(id=="new"){
			cuW_ch.setText("新建");
		}else{
			cuW_ch.setText("修改");
		}
		cuW_ch.show();
		cuW_ch.bringToTop();
		return;
	}
	function delExe(){
		var url = conextPath+"/testBaseSet/testBaseSetAction!delete.action?dto.testBaseSet.typeId="+pmGrid.getSelectedId();
		var ajaxRest = postSub(url,"");
		if(ajaxRest=="failed"){
			hintMsg("删除时发生错误");
		}else{
			pmGrid.deleteRow(pmGrid.getSelectedId());
			clsoseCfWin();
		}
	}
	
	function swStatusExe(){
		var rowId = pmGrid.getSelectedId();
		var currStatus = pmGrid.cells(rowId, 4).getValue();
		var url = conextPath+"/testBaseSet/testBaseSetAction!swStatus.action?dto.testBaseSet.typeId="+rowId;
		if(currStatus=="启用")
			chg2Status = "2";
		else
			chg2Status = "1";		
		url = url +"&dto.testBaseSet.status="+chg2Status;
		var ajaxRest = postSub(url,"");
		if(ajaxRest=="failed"){
			hintMsg("切换状态时发生错误");
		}else{
			var currState = "";
			if(currStatus=="启用")
				currState = "停用";
			else
				currState = "启用";
			pmGrid.cells(rowId, 4).setValue(currState);
			clsoseCfWin();
			var MyState = pmGrid.cells(rowId, 4).getValue();
			if(MyState=='启用'){
				pmBar.setItemToolTip("swStatus", "停用");
				pmBar.setItemImage("swStatus", "disable.png");
			}else{
				pmBar.setItemToolTip("swStatus", "启用");
				pmBar.setItemImage("swStatus", "enable.png");
			}
		}	
	}
	function addUpSub(){
		if(addSubCheck()){
			var url = conextPath+"/testBaseSet/testBaseSetAction!add.action";
			if($("typeId").value!=""){
				url = url.replace('add','update');
			}
			var rest = dhtmlxAjax.postSync(url, "createForm").xmlDoc.responseText;
			if(rest.indexOf("success")>=0){
				var rowData = rest.split("$")[1];
				if($("typeId").value=="")
					pmGrid.addRow(rowData.split("^")[0],rowData.split("^")[1],0);
				else{
				   pmGrid.deleteRow(pmGrid.getSelectedId());
				   pmGrid.addRow(rowData.split("^")[0],rowData.split("^")[1],0);
				 }
				setRowNum(pmGrid);;
				if(contin=="1"&&$("typeId").value==""){
					formRest();
					$("cUMTxt").innerHTML="操作成功，请继续";
					$("isDefault").value="0";
					$("status").value="1";
				}else{
					cuW_ch.setModal(false);
					cuW_ch.hide();
				}
				return;
			}else if(rest=="failed"){
				hintMsg("保存数据时发错错误");
			}else if(rest=="reName"){
				$("cUMTxt").innerHTML="同数据类型重名";
			}
		}
	}
	function addSubCheck(){
		if($("subName").value=="all"){
			$("cUMTxt").innerHTML="请选择数据类型";
			return false;		
		}else if(isWhitespace($("typeName").value)){
			$("cUMTxt").innerHTML="数据名称不能为空";
			return false;
		}
		return true;
	}
	function setUpInfo(updInfo){
		var updInfos = updInfo.split("^");
		for(var i=0; i < updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] != "null"){
				var valueStr = "";
				currInfo[1] =recovJsonKeyWord(currInfo[1],'notRepComma');
				valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";
				eval(valueStr);
			}
		}
	}
