	var rootId = "";
	tree=new dhtmlXTreeObject('treeBox',"100%","100%",0); 
	tree.setImagePath(conextPath+"/dhtmlx/dhtmlxTree/codebase/imgs/");
	tree.enableTreeLines(true);
	tree.setImageArrays("plus","plus2.gif","plus3.gif","plus4.gif","plus.gif","plus5.gif");
	tree.setImageArrays("minus","minus2.gif","minus3.gif","minus4.gif","minus.gif","minus5.gif");
	tree.setStdImages("book.gif","books_open.gif","books_close.gif");
	var Wtext = "需求分解树";
	if(canUp){
		tree.enableItemEditor(true);
		tree.setOnDblClickHandler(onDbclickHdl);
		Wtext +="&nbsp;&nbsp;&nbsp;双击修改";
	}
	if(canMv){
		tree.enableDragAndDrop(true);
		tree.setDragHandler(onDragHdl);
		Wtext +="--拖拽移动";
	}
	if(canDel&&$("isCommit").value=="0"){
		tree.setOnRightClickHandler(delNode);
		Wtext +="--右键删除";
	}
	parent.treeW_ch.setText(Wtext);
	tree.enableHighlighting(1);
	var mytreeArr = $("nodeDataStr").value.split(";");
	var topNode = "";
	for(var i=0 ;i<mytreeArr.length;i++){
		var nodeInfo = mytreeArr[i].split(",");
		tree.insertNewItem(nodeInfo[0],nodeInfo[1],nodeInfo[2],0,0,0,0,"");
		if(nodeInfo[3]=="0"&& i>0){
			tree.setUserData(nodeInfo[1],"blankCh",nodeInfo[1]+"null");
			tree.insertNewChild(nodeInfo[1],nodeInfo[1]+"null","",0,0,0,0,""); 
			tree.closeItem(nodeInfo[1]);
		}
		if(i==0)
			rootId = nodeInfo[1];
		tree.setUserData(nodeInfo[1],"MyState",nodeInfo[4]);
		if(nodeInfo[4]==1){
			tree.setItemColor(nodeInfo[1],'#ff0000','#ff0000');
		}
	}
	tree.setOnClickHandler(onclickHdl);
	tree.setOnOpenEndHandler(onopenEndHdl);
	tree.selectItem(rootId,true);
	function onDbclickHdl(id){
		if(tree.getParentId(id)==0){
			tree.stopEdit();
			return false;
		}
	}

	function postSub(url,formId){
		var ajaxResut = dhtmlxAjax.postSync(url,formId).xmlDoc.responseText;
		if(ajaxResut=="overdue"){
			toLogin();
		}
		return 	ajaxResut;
	}
	function onclickHdl(id){
		var dbClick = id== parent.$("currNodeId").value;
		if(dbClick){ //双击时，会引发两次单击，所以第二次只接返回
			return true;
		}
		parent.$("currNodeId").value= id;
		parent.$("taskId").value=$("taskId").value;
		parent.$("cUMTxt").innerHTML="&nbsp;";
		parent.$("currLevel").value=tree.getLevel(id)
		parent.$("parentNodeId").value=tree.getParentId(id);
		parent.$("assignNIds").value=id;
		if(typeof parent.cuW_ch != "undefined"){
			parent.cuW_ch.setText("增加需求"+"--当前需求:"+tree.getItemText(id));
		}
		var MyState = tree.getUserData(id,"MyState");
		parent.$("moduleState").value=MyState;
		try{//可能没这权限,这里没这按钮,所以要catch住
			if(MyState==0){
				parent.pmBar.setItemToolTip("switchSt", "停用");
				parent.pmBar.setItemImage("switchSt", "disable.png");
			}else{
				parent.pmBar.setItemToolTip("switchSt", "启用");
				parent.pmBar.setItemImage("switchSt", "enable.png");
			}
		}catch(err){
		}
		//根节点不能修改状态
		if(tree.getParentId(id)==0){
			try{
				parent.pmBar.disableItem("switchSt");
			}catch(err){}
			parent.$("addBroRd").disabled=true;
			parent.$("command").value="addchild";
			parent.$("addchildRd").checked=true;
			parent.$("addBroRd").checked=false;
		}else{
			try{
				parent.pmBar.enableItem("switchSt");
			}catch(err){}
			parent.$("addBroRd").disabled=false;
		}
		var pmGrid = parent.pmGrid;
		if(tree.hasChildren(id)>0){
			var url = conextPath+"/outLineManager/outLineAction!loadPeople.action";
			url = url +"?dto.currNodeId="+id;
			var ajaxResut = postSub(url,"");
			if(ajaxResut!="failed"){
				pmGrid.clearAll();
			}
		    if(ajaxResut!= ""&&ajaxResut!="failed"){
		    	ajaxResut = ajaxResut.replace(/[\r\n]/g, "");
		    	jsons = eval("(" + ajaxResut +")");
		    	pmGrid.parse(jsons, "json");
		    }		
		}else{
			pmGrid.clearAll();
		}
		return;
	}

	function onDragHdl(dragId,targetId,siblingId,sTree,tTree){
		if(tree.getLevel(targetId)==99){
			parent.hintMsg("目标需求级别为最大级别99,不能移动");
			return false;		
		}
		var url = conextPath+"/outLineManager/outLineAction!move.action";
		url = url+"?dto.isAjax=true&dto.currNodeId="+dragId +"&dto.targetId="+targetId +"&dto.parentNodeId="+tree.getParentId(dragId);
		url = url +"&dto.taskId="+$("taskId").value;	
		var ajaxResut = postSub(url,"");
		if(ajaxResut=="cancel"){
			parent.hintMsg("与目标需求有重名不能移动");
			return false ;
		}
		if(ajaxResut=="sucess"){
			return true;
		}else{
			parent.hintMsg("保存数据时发生错误");
		}
		return false;
	}
	function appendNode(treeArr,isOpening){
		var topNode = "";
		for(var i=0 ;i<treeArr.length;i++){
			var nodeInfo = treeArr[i].split(",");
			//当展开后，再增加时，己插入的不再插入
			if(tree.getLevel(nodeInfo[1])!=0){
				continue;
			}
			topNode = nodeInfo[0];
			if(i==0){
				var blankId = tree.getUserData(nodeInfo[0],"blankCh");
				var haveBlank = typeof blankId=="undefined" ;
				if(!haveBlank){
					tree.deleteItem(blankId,false);
					tree.setUserData(topNode,"blankCh");
				}
			}
			tree.insertNewItem(nodeInfo[0],nodeInfo[1],nodeInfo[2],0,0,0,0,"");
			tree.setUserData(nodeInfo[1],"MyState",nodeInfo[4]);
			if(nodeInfo[4]==1){
				tree.setItemColor(nodeInfo[1],'#ff0000','#ff0000');
			}
			if(nodeInfo[3]=="0"){
				tree.setUserData(nodeInfo[1],"blankCh",nodeInfo[1]+"null");
				tree.insertNewChild(nodeInfo[1],nodeInfo[1]+"null","",0,0,0,0,""); 
				tree.closeItem(nodeInfo[1]);
			}
		}
		if(typeof isOpening!="undefined" ){
			tree.closeItem(topNode);
		}
		return;
	}
	
	function loadChildren(itemId,isOpening){
		var url = conextPath+"/outLineManager/outLineAction!loadTree.action";
		url = url+"?dto.taskId="+$("taskId").value +"&dto.isAjax=true&dto.currNodeId="+itemId;
		var ajaxResut =  postSub(url,"");
		if(ajaxResut!="failed"&&ajaxResut!=""){
			var treeArr = ajaxResut.split(";");
			appendNode(treeArr,isOpening);
		}	
		return;	
	}
	function onopenEndHdl(id,mode){
		if(mode<0){
			return ;
		}
		var blankId = tree.getUserData(id,"blankCh");
		var haveBlank = typeof blankId=="undefined" ;
		if(!haveBlank){
			loadChildren(id);
		}
	}
	tree.setOnEditHandler(editTreeItem);
	function editTreeItem(state, id, tree, value) {
	    //记下原始节点名字
		if(state==0){
			$("editNodeName").value = value;
		}
	    if ((state == 2) && (value == "")){
	        return false;
	    }else if((state == 2) && (value != "")&&($("editNodeName").value!=value)){
	    	if(!reNameCheck(id)){
	    		parent.hintMsg("同级不能重名");
	    		return false ;
	    	}
	    	return updateNodeName(id,value);
	    }else{
	    	return true;
	    }
	}
	function reNameCheck(id){
		var broNodes = tree.getSubItems(tree.getParentId(id));
		var broNodeArr = broNodes.split(",");
		var nName = tree.getItemText(id);
		for(var h=0;h<broNodeArr.length; h++){
			broName = tree.getItemText(broNodeArr[h]);
			if(nName==broName&&broNodeArr[h]!=id){
				return false ;
			}				
		}
		return true;
	}
	function updateNodeName(id,name){
	    var pId = tree.getParentId(id);
		if(pId=="0"){
			parent.hintMsg("根节点不可修改");
			return false;
		}
		var url = conextPath+"/outLineManager/outLineAction!updateNode.action?dto.isAjax=true";
		$("dtoCurrNodeId").value=id ;
		$("dtoNodeName").value=name ;
		$("dtoParentNodeId").value=tree.getParentId(id) ;
		var ajaxResut =  postSub(url,"updForm");
		if(ajaxResut=="sucess"){
			return true
		}
		if(ajaxResut.indexOf("reName_")==0){
			var node = ajaxResut.split("_")[1];
			appendNode(node.split("_"));
			parent.hintMsg("同级不能重名");
			return false;
		}
		return false;
	}
	function recNormal(){
		tree.stopEdit();
	}
	function delNode(id,obj){
		if($("isCommit").value==1){
			return;
		}
		if(tree.getSelectedItemId()!=id){
			tree.stopEdit();
			tree.selectItem(id,false);
		} 
		if(tree.getSubItems(id)!=""){
			parent.hintMsg("所选节点含子节点不可删除");
	    	return ;
	    }
	    var pId = tree.getParentId(id);
		if(pId=="0"){
			parent.hintMsg("根节点不可删除");
			return;
		}
	    cfDialog("delNodeExe","您确认要删除",false,280);
	    $("delNodeId").value=id;
	}
	function delNodeExe(){
		var pId = tree.getParentId($("delNodeId").value);
		var url = conextPath+"/outLineManager/outLineAction!deleteNode.action?dto.isAjax=true";
		url = url+"&dto.currNodeId="+$("delNodeId").value +"&dto.parentNodeId="+pId+"&dto.taskId="+$("taskId").value;;
		var ajaxResut = postSub(url,"");
		if(ajaxResut=="sucess"){
			var broNodes = tree.getSubItems(pId);
			var broArr = broNodes.split(",");
			var index = broArr.length -1;
			//查找一下个选择兄弟节点
			if(broArr.length>=2){
				for(var i=0; i<broArr.length ;i++){
					if(broArr[i]==$("delNodeId").value){
						if(i>=1){
							index= i-1;
							break;
						}else{
							index= i+1;
							break;
						}
					}
				}
			}
			tree.deleteItem($("delNodeId").value,false);
			//如有兄弟节点，光标移到兄弟上，否则移到父节点
			if(index>=0){
				tree.selectItem(broArr[index],true);
				parent.$("currNodeId").value= broArr[index];
				parent.$("currLevel").value=tree.getLevel(broArr[index]);
				parent.$("parentNodeId").value=tree.getParentId(broArr[index]);
			}else{
				tree.selectItem(pId,true);
				parent.$("currNodeId").value= pId;
				parent.$("currLevel").value=tree.getLevel(pId);
				parent.$("parentNodeId").value=tree.getParentId(pId);
			}
			tree.stopEdit();
			return true;		
		}else if(ajaxResut=="canNotDel"){
			parent.$("mText").innerHTML ="需求有用例或BUG不能删除";
			return false;
		}else{
			parent.$("mText").innerHTML ="删除失败";
			return false;
		}
	}