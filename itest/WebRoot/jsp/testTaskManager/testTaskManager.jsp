<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
		<TITLE>测试任务设置</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel='STYLESHEET' type='text/css'href='<%=request.getContextPath()%>/css/page.css'>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
	</HEAD>
	<style type="text/css">
	a.bluebtn {
		background: transparent url('../images/button/btn-blue-left.gif ')
			no-repeat top left;
		display: block;
		float: right;
		font: normal 13px Tahoma;
		line-height: 16px;
		height: 24px;
		padding-left: 11px;
		text-decoration: none;
	}
	
	a:link .bluebtn,a:visited .bluebtn,a:active .bluebtn {
		color: #494949;
	}
	
	a.bluebtn span {
		background: transparent url('../images/button/btn-blue-right.gif')
			no-repeat top right;
		display: block;
		padding: 4px 11px 4px 0;
	}
	
	a.bluebtn:hover {
		background-position: bottom left;
		text-decoration: none
	}
	
	a.bluebtn:hover span {
		background-position: bottom right;
		color: black;
	}
	
	a.graybtn {
		background: transparent url('../images/button/btn-gray-left.gif')
			no-repeat top left;
		display: block;
		float: left;
		font: normal 13px Tahoma;
		line-height: 16px;
		height: 24px;
		padding-left: 11px;
		text-decoration: none;
	}
	
	a:link .graybtn,a:visited .graybtn,a:active .graybtn {
		color: #494949;
	}
	
	a.graybtn span {
		background: transparent url('../images/button/btn-gray-right.gif')
			no-repeat top right;
		display: block;
		padding: 4px 11px 4px 0;
	}
	
	a.graybtn:hover {
		background-position: bottom left;
	}
	
	a.graybtn:hover span {
		background-position: bottom right;
		color: black;
	}
	
	.buttonwrapper {
		overflow: hidden;
		width: 100%;
	}
	
	a {
		color: #0066aa;
		text-decoration: none;
	}
	
	a:link {
		color: #0066aa;
		text-decoration: none;
	}
	
	a:hover {
		text-decoration: none
	}
</style>
	<script type="text/javascript">
	function styleChange(num){
	 	 	switch(num){
				case 1:
					$("flowInfoTab").className="jihuo";
					$("verInfoTab").className="nojihuo";
					loadFlwInfo();
					break;
				case 2:
					$("flowInfoTab").className="nojihuo";
					$("verInfoTab").className="jihuo";
					 loadVerInfo();
					break;
	 		}
	 } 
	 var haveLoadVerInfo = 0;
	 function loadFlwInfo(){
	 	$("flwInfoDiv").style.display="";
	 	$("verInfoDiv").style.display="none";
	 }

	 function loadVerInfo(){
	 	if($("listStr").value==""){
			var url = conextPath+"/testTaskManager/testTaskManagerAction!softVerList.action";
			var ajaxRest = postSub(url,"");	 
			if(ajaxRest=="failed"){
				hintMsg('加载版本信息失败');
				return;
			}
			$("toolbarObj").style.width = $("gridbox").style.width;
			$("listStr").value=ajaxRest.replace(/[\r\n]/g, "");
		    loadGrid();
		    haveLoadVerInfo=1;
		}
		$("flwInfoDiv").style.display="none";	 
	 	$("verInfoDiv").style.display="";
	}	


	</script>
	<BODY bgcolor="#ffffff" style="overflow-y:hidden;">
		<input type="hidden" id="listStr" name="listStr" value="" />
		<table class="obj row20px" cellspacing="0" cellpadding="0"
			id="baseTable" align="center" width="500">
			<tr>
				<td>
					<div align="center">
						<table class="obj row20px" cellspacing="0" align="center"
							cellpadding="0" border="0" width="500">
							<tr>
								<td class="jihuo" id="flowInfoTab" width="60" class="tdtxt"
									align="right">
									<strong><a onclick="styleChange(1)"
										href="javascript:void(0);"><font color="black">流程设置2</font>
									</a>
									</strong>
								</td>
								<td class="nojihuo" id="verInfoTab" width="60" class="tdtxt"
									align="left">
									<strong><a onclick="styleChange(2)"
										href="javascript:void(0);"><font color="black">版本维护</font>
									</a>
									</strong>
								</td>
								<td colspan="2" width="250">
									&nbsp;
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div id="flwInfoDiv" class="cycleTask gridbox_light"
						style="border: 0px;" align="center">
						<div class="objbox" style="overflow: auto; width: 100%;">
							<ww:hidden id="initFlowStr" name="dto.testFlowStr"></ww:hidden>
							<ww:hidden id="testFlowStr" name="dto.initFlowStr"></ww:hidden>
							<ww:form theme="simple" method="post" id="createForm"
								name="createForm" namespace="" action="">
								<ww:hidden id="taskType" name="dto.taskType"></ww:hidden>
								<ww:hidden id="taskId" name="dto.detail.taskId"></ww:hidden>
								<ww:hidden id="testPhase" name="dto.detail.testPhase"></ww:hidden>
								<ww:hidden id="relaTaskId" name="dto.detail.relaTaskId"></ww:hidden>
								<ww:hidden id="outlineState" name="dto.detail.outlineState"></ww:hidden>
								<ww:hidden id="taskState" name="dto.detail.taskState"></ww:hidden>
								<ww:hidden id="customCase" name="dto.detail.customCase"></ww:hidden>
								<ww:hidden id="customBug" name="dto.detail.customBug"></ww:hidden>
								<ww:hidden id="testerId" name="dto.testerId"></ww:hidden>
								<ww:hidden id="testerConfId" name="dto.testerConfId"></ww:hidden>
								<ww:hidden id="analyserId" name="dto.analyserId"></ww:hidden>
								<ww:hidden id="assignationId" name="dto.assignationId"></ww:hidden>
								<ww:hidden id="programmerId" name="dto.programmerId"></ww:hidden>
								<ww:hidden id="empolderAffirmantId" name="dto.empolderAffirmantId"></ww:hidden>
								<ww:hidden id="empolderLeaderId" name="dto.empolderLeaderId"></ww:hidden>
								<ww:hidden id="testLeadId" name="dto.testLeadId"></ww:hidden>
								<ww:hidden id="testTaskState" name="dto.detail.testTaskState"></ww:hidden>
								<ww:hidden id="projectId" name="dto.detail.projectId"></ww:hidden>
								<ww:hidden id="caseReviewId" name="dto.caseReviewId"></ww:hidden>
								<ww:hidden id="testSeq" name="dto.detail.testSeq"></ww:hidden>
								<table class="obj row20px" cellspacing="0" cellpadding="0"
									id="flwTable" border="1" width="500">
									<tr class="odd_mypm">
										<td width="90" class="tdtxtb">
											<font color="blue" size="3"><b>任务名称:</b>
											</font>
										</td>
										<td colspan="3" class="tdtxtb" style="border-right: 0">
											${dto.taskName}
										</td>
									</tr>
									<tr class="ev_mypm">
										<td class="tdtxt" width="90">
											<font color="blue">负责人设置:</font>
										</td>
										<td class="dataM_center" nowrap style="color: red;">
											测试负责人:
										</td>
										<td colspan="2" class="tdtxt" style="border-right: 0">
											<ww:textfield id="testLead" name="dto.testLead"
												cssClass="text_c" cssStyle="width:310;padding:2 0 0 4;"
												readonly="true"
												onclick="loadGrid();priviChk('testLeadId','testLead');"></ww:textfield>
										</td>
									</tr>
									<tr class="odd_mypm">
										<td colspan="4" class="tdtxt" style="border-right: 0">
											<font color="blue">流程设置</font>
										</td>
									</tr>
									<tr class="ev_mypm">
										<td class="tdtxt" nowrap>
											<ww:checkbox name="dto.caseReview" id="caseReview"
												onclick="rewtFlowStr()" value="false" fieldValue="9" />
											用例Review
										</td>
										<td class="dataM_center">
											Review人:
										</td>
										<td colspan="2" class="tdtxtb" style="border-right: 0">
											<ww:textfield id="caseReviewer" cssClass="text_c"
												name="dto.caseReviewer"
												cssStyle="width:310;padding:2 0 0 4;"
												onfocus="loadGrid();priviChk('caseReviewId','caseReviewer');"></ww:textfield>
										</td>
									</tr>
									<tr class="odd_mypm">
										<td class="tdtxt">
											<ww:checkbox name="dto.reportBug" id="reportBug" value="true"
												fieldValue="1" disabled="true" />
											提交问题
										</td>
										<td class="dataM_center" style="color: red;">
											测试人员:
										</td>
										<td colspan="2" class="tdtxtb" style="border-right: 0">
											<ww:textfield id="tester" cssClass="text_c" name="dto.tester"
												cssStyle="width:310;padding:2 0 0 4;" readonly="true"
												onclick="loadGrid();priviChk('testerId','tester');"></ww:textfield>
										</td>
									</tr>
									<tr class="ev_mypm">
										<td class="tdtxt">
											<ww:checkbox name="dto.testerBugConfirm"
												onclick="rewtFlowStr()" id="testerBugConfirm" value="false"
												fieldValue="2" />
											测试互验
										</td>
										<td class="dataM_center">
											互验人员:
										</td>
										<td colspan="2" class="tdtxtb" style="border-right: 0">
											<ww:textfield id="testerConf" cssClass="text_c"
												name="dto.testerConf" readonly="true"
												onclick="loadGrid();priviChk('testerConfId','testerConf');"
												cssStyle="width:310;padding:2 0 0 4;"></ww:textfield>
										</td> 
									</tr>
									<tr class="odd_mypm">
										<td class="tdtxt">
											<ww:checkbox name="dto.analyse" id="analyse"
												onclick="rewtFlowStr()" value="false" fieldValue="3" />
											分析问题
										</td>
										<td class="dataM_center">
											分析人:
										</td>
										<td colspan="2" class="tdtxtb" style="border-right: 0">
											<ww:textfield id="analyser" cssClass="text_c"
												name="dto.analyser" readonly="true"
												onclick="loadGrid();priviChk('analyserId','analyser');"
												cssStyle="width:310;padding:2 0 0 4;"></ww:textfield>
										</td>
									</tr>
									<tr class="ev_mypm">
										<td class="tdtxt">
											<ww:checkbox name="dto.assignation" id="assignation"
												onclick="rewtFlowStr()" value="false" fieldValue="4" />
											分配问题
										</td>
										<td class="dataM_center">
											分配人:
										</td>
										<td colspan="2" class="tdtxtb" style="border-right: 0">
											<ww:if test="${dto.assigner}==null||${dto.assigner}==''">
												<ww:textfield id="assigner" cssClass="text_c"
													name="dto.assigner" readonly="true"
													onclick="loadGrid();priviChk('assignationId','assigner');"
													cssStyle="width:310;padding:2 0 0 4;"></ww:textfield>
											</ww:if>
											<ww:else>
												<ww:textfield id="assigner" cssClass="text_c"
													name="dto.assigner" readonly="true"
													onclick="loadGrid();priviChk('assignationId','assigner');"
													cssStyle="width:310;padding:2 0 0 4;"></ww:textfield>
											</ww:else>
										</td>
									</tr>
									<tr class="odd_mypm">
										<td class="tdtxt">
											<ww:checkbox name="dto.devFixBug" id="devFixBug" value="true"
												fieldValue="5" disabled="true" />
											修改问题
										</td>
										<td class="dataM_center" style="color: red;">
											修改人:
										</td>
										<td colspan="2" class="tdtxtb" style="border-right: 0">
											<ww:textfield id="programmer" cssClass="text_c"
												name="dto.programmer" readonly="true"
												onclick="loadGrid();priviChk('programmerId','programmer');"
												cssStyle="width:310;padding:2 0 0 4;"></ww:textfield>
										</td>
									</tr>
									<tr class="ev_mypm">
										<td class="tdtxt">
											<ww:checkbox name="dto.devConfirmFix" onclick="rewtFlowStr()"
												id="devConfirmFix" value="false" fieldValue="6" />
											开发互检
										</td>
										<td class="dataM_center">
											互检人:
										</td>
										<td colspan="2" class="tdtxtb" style="border-right: 0">
											<ww:textfield id="empolderAffirmant" cssClass="text_c"
												readonly="true"
												onclick="loadGrid();priviChk('empolderAffirmantId','empolderAffirmant');"
												name="dto.empolderAffirmant"
												cssStyle="width:310;padding:2 0 0 4;"></ww:textfield>
										</td>
									</tr>
									<tr class="odd_mypm">
										<td class="tdtxt">
											<ww:checkbox name="dto.arbitrateBug" onclick="rewtFlowStr()"
												id="arbitrateBug" value="true" fieldValue="1"
												disabled="true" fieldValue="7" />
											分歧仲裁
										</td>
										<td class="dataM_center" style="color: red;">
											仲裁人:
										</td>
										<td colspan="2" class="tdtxtb" style="border-right: 0">
											<ww:textfield id="empolderLeader" cssClass="text_c"
												readonly="true"
												onclick="loadGrid();priviChk('empolderLeaderId','empolderLeader');"
												name="dto.empolderLeader"
												cssStyle="width:310;padding:2 0 0 4;"></ww:textfield>
										</td>
									</tr>
									<tr class="ev_mypm">
										<td class="tdtxt">
											<ww:checkbox name="dto.testerVerifyFix" id="testerVerifyFix"
												value="true" fieldValue="8" disabled="true" />
											测试确认
										</td>
										<td class="tdtxt" colspan="3" style="border-right: 0">
											&nbsp;
										</td>
									</tr>
									<tr class="odd_mypm">
										<td colspan="4" class="tdtxt">
											<font color="blue" style="border-right: 0">消息设置</font>
										</td>
									</tr>
									<tr class="ev_mypm">
										<td class="tdtxt" colspan="4" style="border-right: 0">
											<ww:if test="${dto.detail.mailOverdueBug}==1">
												<ww:checkbox name="dto.detail.mailOverdueBug"
													id="mailOverdueBug" value="true" fieldValue="1" />
											</ww:if>
											<ww:else>
												<ww:checkbox name="dto.detail.mailOverdueBug"
													id="mailOverdueBug" value="false" fieldValue="1" disabled="true" />
											</ww:else>
											Bug逾期不改通知修改人
										</td>
									</tr>
									<tr class="odd_mypm">
										<td class="tdtxt" colspan="4" style="border-right: 0">
											<ww:if test="${dto.detail.mailDevFix}==1">
												<ww:checkbox name="dto.detail.mailDevFix" id="mailDevFix"
													value="true" fieldValue="1" />
											</ww:if>
											<ww:else>
												<ww:checkbox name="dto.detail.mailDevFix" id="mailDevFix"
													value="false" fieldValue="1" />
											</ww:else>
											Bug指派时通知修改人
										</td>
									</tr>
									<tr class="ev_mypm">
										<td class="tdtxt" colspan="4" style="border-right: 0">
											<ww:if test="${dto.detail.mailVerdict}==1">
												<ww:checkbox name="dto.detail.mailVerdict" id="mailVerdict"
													value="true" fieldValue="1" />
											</ww:if>
											<ww:else>
												<ww:checkbox name="dto.detail.mailVerdict" id="mailVerdict"
													value="false" fieldValue="1" />
											</ww:else>
											需仲裁时通知仲裁人
										</td>
									</tr>
									<tr class="odd_mypm">
										<td class="tdtxt" colspan="4" style="border-right: 0">
											<ww:if test="${dto.detail.mailReport}==1">
												<ww:checkbox name="dto.detail.mailReport" id="mailReport"
													value="true" fieldValue="1" />
											</ww:if>
											<ww:else>
												<ww:checkbox name="dto.detail.mailReport" id="mailReport"
													value="false" fieldValue="1" disabled="true"/>
											</ww:else>
											每日通知开发负责人、测试负责人测试报告
										</td>
									</tr>
									<tr class="ev_mypm">
										<td colspan="4" class="dataM_center" align="center"
											style="border-right: 0">
											<div id="cUMTxt" align="center"
												style="color: Blue; padding: 2px"></div>
										</td>
									</tr>
									<tr class="odd_mypm">
										<td colspan="4" class="tdtxt" align="center"
											style="border-right: 0;">
										   	<a class="bluebtn" href="javascript:void(0);" id="ret_b"
												onclick=" closeMe();" style="margin-left: 6px;"><span>
													返回</span> </a>
											<a class="bluebtn" href="javascript:void(0);" id="save_b"
												onclick="speUpSumbit(addUpCB());" style="margin-left: 6px;"><span>
													确定</span> </a>&nbsp;&nbsp;
										</td>
									</tr>
								</table>
							</ww:form>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div id="verInfoDiv"
						style="background-color: #ffffff; display: none; width: 500;"
						align="center">
						<table width="500" cellspacing="0" cellpadding="0" align="center"
							border="0">
							<tr>
								<td align="center">
									<div id="toolbarObj"></div>
								</td>
							</tr>
							<tr>
								<td align="center">
									<div id="gridbox"></div>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
		<div id="createDiv" align="center" class="cycleTask gridbox_light" style="border:0px;display:none">
		  <div class="objbox" style="overflow:auto;width:350;">
		    <ww:form theme="simple" method="post" id="verCForm" name="verCForm" namespace="" action="">
		     <table  border="0" id="baseTable" class="obj row20px" cellspacing="0" align="center" cellpadding="0" border="0" width="340">
		       <tr class="ev_mypm">
		        <td colspan="4" style="border-right:0">
					<div id="cUMTxt2" align="center" style="color: Blue; padding: 2px"></div>	        
		        </td>
		       </tr>
		       <tr class="odd_mypm">
		         <td width="80"  class="rightM_center" style="border-right:0;color:red"> 名称:
		         </td>
		         <td class="tdtxt" width="140" style="border-right:0">
		          	<input type="text" name="dto.softVer.versionNum" id="versionNum" style="width: 140; padding: 2 0 0 4;" class="text_c" maxlength="40"/>
		         </td>
		         <td width="60" class="rightM_center" style="border-right:0;color:red" >顺序号:
		         </td>
		         <td width="60">
		            <input type="text" name="dto.softVer.seq" id="verSeq" style="width: 80; padding: 2 0 0 4;" class="text_c" onkeypress="javascript:return numChk(event);" maxlength="4"/>
		         </td>
		       </tr>
		       <tr class="ev_mypm">
		         <td width="60" class="rightM_center" style="border-right:0">备注:
		         </td>
		         <td class="tdtxt" width="280" style="border-right:0" colspan="3">
		            <input type="text" name="dto.softVer.remark" id="remark" style="width: 280; padding: 2 0 0 4;" class="text_c" maxlength="100"/>
		         </td>
		       </tr>
		       <tr class="odd_mypm">
		         <td colspan="4" style="border-right:0;width:340">
					<a class="bluebtn" href="javascript:void(0);" id="saveVc_b" style="display:none;margin-left: 6px"
						onclick="addUpSub('conF');"><span>
						确定并继续</span> </a>	
					<a class="bluebtn" href="javascript:void(0);" id="saveV_b" style="margin-left: 6px"
						onclick="addUpSub();"><span>
						确定</span> </a>		         
		         </td>
		       </tr>
		     </table>
		     <ww:hidden id="versionId" name="dto.softVer.versionId" ></ww:hidden>
		      <ww:hidden id="verStatus" name="dto.softVer.verStatus" ></ww:hidden>
		    </ww:form>
		  </div>
		</div>
	 	<ww:include value="/jsp/common/selTemPerson.jsp"></ww:include>
		<ww:include value="/jsp/common/dialog.jsp"></ww:include>
	</BODY>
	<script type="text/javascript"src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
	<script type="text/javascript"src="<%=request.getContextPath()%>/jsp/testTaskManager/testTask.js"></script>
	<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
	<script type="text/javascript">
	var myId = "${session.currentUser.userInfo.id}";
	 <pmTag:priviChk urls="testTaskManagerAction!update" varNames="canSet"/>
	 if($("testLeadId").value==""){
	 	canSet = true;
	 }else if($("testLeadId").value!=""&&$("testLeadId").value.indexOf(myId)>=0){
	 	canSet = true;
	 }else if($("testLeadId").value!=""&&$("testLeadId").value.indexOf(myId)<0){
	 	canSet = false;
	 }
	 if(!canSet){
	 	$("save_b").style.display="none";
	 	eval("$('save_b').onclick  =''");
	 }
	 function priviChk(passValId,passNameId){
	 	if(!canSet){
	 		hintMsg("您不是测试负责人,无流程设置权限");
	 		return;
	 	}
	 	if($("testTaskState").value=="1"||$("testTaskState").value=="2"){
	 		hintMsg("己完成或结束不能设置流程");
	 		return;	 	
	 	}
	 	temUserSelWin(passValId,passNameId);
	 	
	 }
	 function closeMe(){
		parent.testW_ch.setModal(false);
		parent.testW_ch.hide();	 
	 }
	</script>
</HTML>
