
package cn.com.mypm.framework.hibernate;

import org.hibernate.criterion.Criterion;

/**
 * @author liuyg
 *
 */
public interface HibernateExpression {
	public Criterion createCriteria();
}
