package cn.com.mypm.framework.security;

import java.util.HashSet;
import java.util.Set;

/**
 * @author liuyg
 * 
 */
public  class Visit {

	private VisitUser userInfo;
	private Set<String> errors ;
	private String taskId =null;
	private String upDirectory;
	private boolean menuLoad = false;

	public void clearErrors() {
		if(errors==null){
			return;
		}
		errors.clear();
	}

	public Set<String> getErrors() {
		return errors;
	}

	public void addError(String err) {
		if(errors==null){
			errors = new HashSet<String>();
		}
		errors.add(err);
	}

	public void setErrors(Set<String> errors) {
		this.errors = errors;
	}

	public VisitUser getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(VisitUser userInfo) {
		this.userInfo = userInfo;
	}

	public  <T> T getUserInfo(Class<T>  clasz){
		return (T)this.userInfo;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getUpDirectory() {
		return upDirectory;
	}

	public void setUpDirectory(String upDirectory) {
		this.upDirectory = upDirectory;
	}

	public boolean isMenuLoad() {
		return menuLoad;
	}

	public void setMenuLoad(boolean menuLoad) {
		this.menuLoad = menuLoad;
	}


	
}