package cn.com.mypm.licenseMgr.blh;

import java.util.Date;

import org.apache.log4j.Logger;

import cn.com.mypm.framework.app.blh.BusinessBlh;
import cn.com.mypm.framework.app.view.View;
import cn.com.mypm.framework.hibernate.HibernateGenericController;
import cn.com.mypm.framework.transmission.events.BusiRequestEvent;
import cn.com.mypm.licenseMgr.dto.LicesneMgrDto;

public class LicesneMgrBlh extends BusinessBlh {

	private static LicesneMgrBlh instance = new LicesneMgrBlh();
	private static Date sendDate = null;
	private LicesneMgrBlh(){
		
	} 
	private static Logger logger = Logger.getLogger(HibernateGenericController.class);
	public synchronized View reptLicenseApp(BusiRequestEvent req){

		return super.getView("sendCodeApp");
	}
	private String buildMailBody(LicesneMgrDto dto){
		StringBuffer sb = new StringBuffer();

		return sb.toString();
	}
	public synchronized View sendReptLicenseInfo(BusiRequestEvent req){

		return super.globalAjax();

	}
	
	public synchronized View regLicense(BusiRequestEvent req){

		return super.globalAjax();       
		
	}



	public static LicesneMgrBlh getInstance() {
		return instance;
	}

}
